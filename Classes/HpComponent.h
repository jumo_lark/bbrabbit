//
//  HpComponent.h
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/12.
//
//

#ifndef __bbrabbit__HpComponent__
#define __bbrabbit__HpComponent__

#include "cocos2d.h"
USING_NS_CC;

using namespace std;

class HpComponent : public CCNode
{
public:
    
    static HpComponent* create(float maxHp);
    
    void refresh(float curHp);
    void getProcessed(float process);
    
    void resetMaxHp(float max);
    void resetCurHp(float cur);
    
private:
    
    HpComponent();
    virtual ~HpComponent();
    virtual bool init(float maxHp);
    
    void createHp();
    
protected:
    
    CCProgressTimer* _hp;
    
    float _max;
    float _curHp;
};

#endif /* defined(__bbrabbit__HpComponent__) */
