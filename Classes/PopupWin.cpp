//
//  PopupWin.cpp
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/11.
//
//

#include "PopupWin.h"
#include "Button.h"
#include "FormSprite.h"
#include "GlobalConstant.h"

enum KPopupWin_Zorder
{
    KPopupWin_Zorder_Base = 0,
    KPopupWin_Zorder_Img,
    KPopupWin_Zorder_Info,
    KPopupWin_Zorder_Button,
    KPopupWin_Zorder_Star,
};

PopupWin* PopupWin::create(cocos2d::CCScene *scene, int starCount)
{
    PopupWin* popup = new PopupWin();
    if(popup && popup->init(scene, starCount))
    {
        popup->autorelease();
        return popup;
    }
    CC_SAFE_DELETE(popup);
    return NULL;
}

PopupWin::PopupWin()
{
    
}

PopupWin::~PopupWin()
{
    
}

bool PopupWin::init(cocos2d::CCScene *scene, int starCount)
{
    if(PopupLayer::init(scene))
    {
        _id = PopupId_Win;
        _starCount = starCount;
        
        this->createTitleBg();
        this->createStars();
        this->createImg();
        this->createRewardInfo();
        this->createButtonReplay();
        this->createButtonNextLevel();
        
        return true;
    }
    return false;
}

void PopupWin::createBackground()
{
    _size = ccp(381, 428);
    
    _bg = FormSprite::createWithCorner(CCSprite::createWithSpriteFrameName("img_popup_bg_corner"), CCSprite::createWithSpriteFrameName("img_popup_bg_content"), _size.width, _size.height);
    _bg->setPosition(ccp(_screenSize.width * 0.5, _screenSize.height * 0.45));
    this->addChild(_bg);
}

void PopupWin::createTitleBg()
{
    CCSprite* sprite_title_left = CCSprite::createWithSpriteFrameName("img_popup_title_red");
    sprite_title_left->setAnchorPoint(ccp(0, 1));
    sprite_title_left->setPosition(ccp(-_size.width * 0.5, _size.height * 0.625));
    _bg->addChild(sprite_title_left);
    
    CCSprite* sprite_title_right = CCSprite::createWithSpriteFrameName("img_popup_title_red");
    sprite_title_right->setFlipX(true);
    sprite_title_right->setAnchorPoint(ccp(1, 1));
    sprite_title_right->setPosition(ccp(_size.width * 0.5, _size.height * 0.625));
    _bg->addChild(sprite_title_right);
    
    CCSprite* sprite_light = CCSprite::createWithSpriteFrameName("img_popup_light_point");
    sprite_light->setPosition(ccp(sprite_title_left->getContentSize().width * 0.105, sprite_title_left->getContentSize().height * 0.82));
    sprite_title_left->addChild(sprite_light);
}

void PopupWin::createStars()
{
    vector<string> vec_names;
    vector<CCPoint> vec_poss;
    vector<float> vec_scales;
    for(int i = 0; i < GlobalConstant_Max_starCount; ++i)
    {
        string str_name = i < _starCount ? "popupwin_star" : "popupwin_star_empty";
//        CCPoint pos = ccp(_size.width * 0.28 * (i - 1), _size.width * (i == 1 ? 0.5 : 0.465));
        CCPoint pos = ccp(_size.width * 0.28 * (i - 1), _size.width * (i == 1 ? 0.555 : 0.515));
        float scale = i == 1 ? 1 : 0.9;
        float rotation = 15 * (i - 1);
        
        CCSprite* sprite_star = CCSprite::createWithSpriteFrameName(str_name.c_str());
        sprite_star->setPosition(pos);
        sprite_star->setScale(scale);
        sprite_star->setRotation(rotation);
        _bg->addChild(sprite_star, KPopupWin_Zorder_Star);
    }
}

void PopupWin::createImg()
{
    CCSprite* img = CCSprite::createWithSpriteFrameName("popup_win_img");
    img->setPosition(ccp(0, _size.height * 0.115));
    _bg->addChild(img, KPopupWin_Zorder_Img);
}

void PopupWin::createRewardInfo()
{
    float scale_font = 0.75;
    
    CCSprite* img_gamepower = CCSprite::createWithSpriteFrameName("img_label_gamepower");
    img_gamepower->setPosition(ccp(-_size.width * 0.25, -_size.height * 0.155));
    _bg->addChild(img_gamepower, KPopupWin_Zorder_Info);
    
    CCLabelBMFont* font_gamepower = CCLabelBMFont::create("12345", "Fonts/font_num.fnt");
    font_gamepower->setAnchorPoint(ccp(0, 0.5));
    font_gamepower->setPosition(ccp(img_gamepower->getPosition().x + img_gamepower->getContentSize().width * 0.7, img_gamepower->getPosition().y));
    font_gamepower->setColor(ccc3(210, 122, 42));
    font_gamepower->setScale(scale_font);
    _bg->addChild(font_gamepower, KPopupWin_Zorder_Info);
    
    CCSprite* img_font_mmb = CCSprite::createWithSpriteFrameName("img_label_gold");
    img_font_mmb->setPosition(ccp(-_size.width * 0.25, -_size.height * 0.235));
    _bg->addChild(img_font_mmb, KPopupWin_Zorder_Info);
    
    CCSprite* img_mmb = CCSprite::createWithSpriteFrameName("img_mmb");
    img_mmb->setScale(0.55);
    img_mmb->setPosition(ccp(img_font_mmb->getPosition().x + img_font_mmb->getContentSize().width * 0.7 + img_mmb->boundingBox().size.width * 0.5, img_font_mmb->getPosition().y));
    _bg->addChild(img_mmb, KPopupWin_Zorder_Info);
    
    CCLabelBMFont* font_mmb = CCLabelBMFont::create("+100", "Fonts/font_num.fnt");
    font_mmb->setAnchorPoint(ccp(0, 0.5));
    font_mmb->setPosition(ccp(img_mmb->getPosition().x + img_mmb->boundingBox().size.width * 0.55, img_mmb->getPosition().y));
    font_mmb->setColor(ccc3(240, 178, 16));
    font_mmb->setScale(scale_font);
    _bg->addChild(font_mmb, KPopupWin_Zorder_Info);
}

void PopupWin::createButtonReplay()
{
    CCSprite* content = CCSprite::createWithSpriteFrameName("button_orange");
    CCLabelBMFont* font_info = CCLabelBMFont::create("重 玩", "Fonts/font_popup.fnt");
//    font_info->setScale(1.2);
    Button* button = Button::create(ButtonId_Game_Replay, content, NULL, font_info);
    button->setPosition(ccp(-_size.width * 0.225, -_size.height * 0.375));
    _bg->addChild(button);
    _buttonManager->addButton(button);
}

void PopupWin::createButtonNextLevel()
{
    CCSprite* content = CCSprite::createWithSpriteFrameName("button_blue");
    CCLabelBMFont* font_info = CCLabelBMFont::create("下一关", "Fonts/font_popup.fnt");
//    font_info->setScale(1.2);
    Button* button = Button::create(ButtonId_Game_NextLevel, content, NULL, font_info);
    button->setPosition(ccp(_size.width * 0.225, -_size.height * 0.375));
    _bg->addChild(button);
    _buttonManager->addButton(button);
}

void PopupWin::onButtonClick(cocos2d::CCObject *obj)
{
    Button* button = (Button*)obj;
    ButtonId bid = button->getId();
    
    if(bid == ButtonId_Game_Replay)
    {
        // Nothing Todo
    }
    else if(bid == ButtonId_Game_NextLevel)
    {
        // Nothing Todo
    }
}