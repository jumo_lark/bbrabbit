//
//  AStarPathHelper.h
//  rabbit
//
//  Created by 李崧榕 on 15/2/5.
//
//

#ifndef __rabbit__AStarPathHelper__
#define __rabbit__AStarPathHelper__

#include "cocos2d.h"
USING_NS_CC;

using namespace std;

class PathUnit;

enum PathUnitType
{
    PathUnitType_UnPass = 0,
    PathUnitType_Pass,
};

enum PathDirection
{
    PathDirection_Up    = 0,
    PathDirection_Down  = 1,
    PathDirection_Left  = 2,
    PathDirection_Right = 3,
};

class PathHelper : public CCNode
{
public:
    
    static PathHelper* create(CCTMXTiledMap* tileMap);
    
    CCArray* getPath(CCPoint point_start, CCPoint point_end);
    
protected:
    
    PathHelper();
    virtual ~PathHelper();
    virtual bool init(CCTMXTiledMap* tileMap);
    
    void initPathUnits(CCTMXTiledMap* tileMap);
    
    void addPathUnit(int locX, int locY, PathUnit* unit);
    PathUnit* getPathUnit(int locX, int locY);
    string getPathKey(int locX, int locY);
    
private:
    
    void getAStarSearchPath(CCArray* path, CCPoint start, CCPoint end);
    
    void getAroundsUnit(CCArray* aroundUnits, PathUnit* unit);
    
    // 以F值从小到大的顺序插入
    void insertOrderUnit(CCArray* units, PathUnit* unit);
    
    // data
    CCDictionary* _mapDic;
    CCSize _size;
    CCTMXTiledMap* _tileMap;
};

/*
 * class PathUnit 路径单元数据
 */

class PathUnit : public CCObject
{
public:
    
    CREATE_FUNC(PathUnit);
    
    int getF(){return _g + _h;}; // 从起点经过Unit到终点的预期总消耗值
    void setH(CCPoint end){_h = this->getH(end);};
    int getH(CCPoint end){return ccpDistance(_loc, end);};
    
    void setDirection(PathDirection direction, bool value){_direction[direction] = value;};
    
protected:
    
    PathUnit():_loc(ccp(0, 0)), _g(0), _h(0), _type(PathUnitType_Pass), _frontUnit(NULL){};
    virtual ~PathUnit(){};
    virtual bool init()
    {
        for(int i = PathDirection_Up; i <= PathDirection_Right; ++i)
        {
            _direction.push_back(false);
        };
        return true;
    };
    
private:
    
    CC_SYNTHESIZE(CCPoint, _loc, Location);          // Unit的位置
    CC_SYNTHESIZE(PathUnitType, _type, Type);        // Unit的类型
    CC_SYNTHESIZE(int, _g, G);                       // 从起点到Unit的已消耗值
    CC_SYNTHESIZE(int, _h, H);                       // 从Unit到终点的预期消耗值 此处采用欧几里得距离：d = sqrt((x1-x2)^2+(y1-y2)^2)
    CC_SYNTHESIZE(PathUnit*, _frontUnit, FrontUnit); // 上一个路径单元
    
    CC_SYNTHESIZE(vector<bool>, _direction, Direction);
};

#endif /* defined(__rabbit__AStarPathHelper__) */
