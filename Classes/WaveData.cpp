//
//  WaveData.cpp
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/10.
//
//

#include "WaveData.h"
#include "StringHelper.h"
#include "StaticDataManager.h"
#include "EnemyData.h"

WaveData* WaveData::create()
{
    WaveData* data = new WaveData();
    if(data && data->init())
    {
        data->autorelease();
        return data;
    }
    CC_SAFE_DELETE(data);
    return NULL;
}

WaveData::WaveData()
: _enemyDatas(NULL)
{
    
}

WaveData::~WaveData()
{
    CC_SAFE_RELEASE_NULL(_enemyDatas);
}

bool WaveData::init()
{
    _enemyDatas = CCArray::create();
    _enemyDatas->retain();
    
    return true;
}

void WaveData::deserialize(Value &value)
{
    int index = 0;
    _enemyGapTime = StringHelper::stringToInt(value[index].GetString());
    
    for(int i = 1; i < value.Size(); ++i)
    {
        string str_value = value[i].GetString();
        
        vector<string> vec_values = StringHelper::componentsSeparatedByString(str_value, KSign_Separator_StaticData);
        int enemyId = StringHelper::stringToInt(vec_values[0]);
        int enemyCount = StringHelper::stringToInt(vec_values[1]);
        
        EnemyData* enemyData = StaticData->getEnemyData(enemyId);
        for(int j = 0; j < enemyCount; ++j)
        {
            _enemyDatas->addObject(enemyData);
        }
    }
}