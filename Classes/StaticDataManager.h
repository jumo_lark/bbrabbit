//
//  StaticDataManager.h
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/10.
//
//

#ifndef __bbrabbit__StaticDataManager__
#define __bbrabbit__StaticDataManager__

#include "cocos2d.h"
USING_NS_CC;

#include "FileHelper.h"

class EnemyData;
class WaveData;
class TowerData;
class ItemData;

#define StaticData StaticDataManager::getInstance()

const string KSign_Separator_StaticData = ":";

class StaticDataManager : public CCObject
{
public:
    
    static StaticDataManager* getInstance();
    
    TowerData* getTowerData(int id, int level);
    WaveData* getWaveData(int id);
    EnemyData* getEnemyData(int id);
    ItemData* getItemData(int id);
    
protected:
    
    StaticDataManager();
    virtual ~StaticDataManager();
    virtual bool init();
    
    static StaticDataManager* _instance;
    
    void initTowerDatas();
    void initEnemyDatas();
    void initWaveDatas();
    void initItemDatas();
    
private:
    
    CCDictionary* _towersData;
    CCDictionary* _enemyDatas;
    CCDictionary* _waveDatas;
    CCDictionary* _itemDatas;
};

#endif /* defined(__bbrabbit__StaticDataManager__) */
