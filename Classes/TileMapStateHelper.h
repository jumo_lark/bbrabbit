//
//  TileMapStateHelper.h
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/9.
//
//

#ifndef __bbrabbit__TileMapStateHelper__
#define __bbrabbit__TileMapStateHelper__

#include "cocos2d.h"
USING_NS_CC;
using namespace std;

enum TileMapState
{
    TileMapState_NotInMap,
    
    TileMapState_Empty,
    TileMapState_Path,
    TileMapState_Item,
    TileMapState_Tower,
};

class TileMapStateHelper : public CCNode
{
public:
    
    static TileMapStateHelper* create(CCTMXTiledMap* tileMap);
    
    TileMapState getTileMapState(CCPoint tilePoint);
    void setTileMapState(CCPoint tilePoint, TileMapState state);
    
protected:
    
    TileMapStateHelper();
    virtual ~TileMapStateHelper();
    virtual bool init(CCTMXTiledMap* tileMap);
    
    void initMapState();
    
    bool isInMap(CCPoint tilePoint);
    
private:
    
    CCTMXTiledMap* _tileMap;
    CCSize _size;
    vector<vector<TileMapState> > _map_state;
};

#endif /* defined(__bbrabbit__TileMapStateHelper__) */
