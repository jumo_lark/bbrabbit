//
//  MoveComponent.cpp
//  rabbit
//
//  Created by 李崧榕 on 15/2/6.
//
//

#include "MoveComponent.h"

MoveComponent* MoveComponent::create(cocos2d::CCNode *target)
{
    MoveComponent* component = new MoveComponent();
    if(component && component->init(target))
    {
        component->autorelease();
        return component;
    }
    CC_SAFE_DELETE(component);
    return NULL;
}

MoveComponent::MoveComponent()
: _target(NULL)
, _delegate(NULL)
, _speed(100.f)
, _speedUnitVector(ccp(1, 0))
, _inPause(false)
{
    
}

MoveComponent::~MoveComponent()
{
    
}

bool MoveComponent::init(cocos2d::CCNode *target)
{
    if(CCNode::init())
    {
        _target = target;
        
        return true;
    }
    return false;
}

void MoveComponent::startMove(cocos2d::CCPoint pos)
{
    _targetPos = pos;
    _startPos = _target->getPosition();
    _speedUnitVector = ccpNormalize(_targetPos - _startPos);
    this->scheduleUpdate();
}

void MoveComponent::stopMove()
{
    this->unscheduleUpdate();
}

void MoveComponent::updateTargetPosition(cocos2d::CCPoint pos)
{
    if(_targetPos.equals(pos))
        return;
    
    _targetPos = pos;
    _startPos = _target->getPosition();
    _speedUnitVector = ccpNormalize(_targetPos - _startPos);
}

void MoveComponent::pauseMove()
{
    _inPause = true;
}

void MoveComponent::resumeMove()
{
    _inPause = false;
}

void MoveComponent::update(float dt)
{
    if(!_inPause)
    {
        CCPoint curPos = _target->getPosition();
        
        if(ccpDistance(curPos, _targetPos) <= _speed * dt)
        {
            _target->setPosition(_targetPos);
            this->unscheduleUpdate();
            
            if(_delegate)
            {
                _delegate->onArrived();
            }
        }
        else
        {
            CCPoint velocity = _speedUnitVector * _speed * dt;
            _target->setPosition(curPos + velocity);
        }
    }
}

void MoveComponent::setDelegate(MoveDelegate *delegate)
{
    _delegate = delegate;
}

void MoveComponent::removeDelegate()
{
    _delegate = NULL;
}