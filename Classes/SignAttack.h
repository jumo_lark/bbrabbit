//
//  SignAttack.h
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/12.
//
//

#ifndef __bbrabbit__SignAttack__
#define __bbrabbit__SignAttack__

#include "SignNode.h"
class DestroyableEntity;

class SignAttack : public SignNode
{
public:
    
    static SignAttack* create();
    
    void setTarget(DestroyableEntity* target);
    void removeTarget();
    
protected:
    
    SignAttack();
    virtual ~SignAttack();
    virtual bool init();
    
    virtual void update(float dt);
    
    virtual void addObserver();
    
    void onRemoveTarget(CCObject* obj);
    
private:
    
    CC_SYNTHESIZE_READONLY(DestroyableEntity*, _target, Target);
    
    CCSprite* _sprite_sign;
};

#endif /* defined(__bbrabbit__SignAttack__) */
