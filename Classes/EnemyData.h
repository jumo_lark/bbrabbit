//
//  EnemyData.h
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/6.
//
//

#ifndef __bbrabbit__EnemyData__
#define __bbrabbit__EnemyData__

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;
USING_NS_CC_EXT;
using namespace std;

using namespace rapidjson;

enum EnemyType
{
    EnemyType_Normal = 0,
    EnemyType_Boss,
    EnemyType_Super_Boss,
};

class EnemyData : public CCObject
{
public:
    
    static EnemyData* create();
    
    void deserialize(Value& value);
    
protected:
    
    EnemyData();
    virtual ~EnemyData();
    virtual bool init();
    
private:
    
    CC_SYNTHESIZE(int, _id, Id);
    CC_SYNTHESIZE_READONLY(EnemyType, _type, Type);
    CC_SYNTHESIZE_READONLY(string, _name, Name);
    CC_SYNTHESIZE_READONLY(int, _hp, Hp);
    CC_SYNTHESIZE_READONLY(float, _speed, Speed);
    CC_SYNTHESIZE_READONLY(int, _gold, Gold);
};

#endif /* defined(__bbrabbit__EnemyData__) */
