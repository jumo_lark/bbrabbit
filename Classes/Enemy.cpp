//
//  Enemy.cpp
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/6.
//
//

#include "Enemy.h"
#include "EntityManager.h"
#include "EnemyData.h"
#include "NotifyManager.h"
#include "HpComponent.h"

Enemy* Enemy::create(EntityManager *entityManager, EnemyData *data)
{
    Enemy* enemy = new Enemy();
    if(enemy && enemy->init(entityManager, data))
    {
        enemy->autorelease();
        return enemy;
    }
    CC_SAFE_DELETE(enemy);
    return NULL;
}

Enemy::Enemy()
: _entityManager(NULL)
, _data(NULL)
, _speed(100)
, _beKilled(false)
{
    
}

Enemy::~Enemy()
{
    
}

bool Enemy::init(EntityManager *entityManager, EnemyData *data)
{
    if(DestroyableEntity::init())
    {
        _entityManager = entityManager;
        _data = data;
        
        this->initProperty();
        this->addAnimation();
        this->addPathMoveComponent();
        this->addHpComponent();
        this->addShadow();
        
        return true;
    }
    return false;
}

void Enemy::initProperty()
{
    _maxHp = _data->getHp();
    _curHp = _maxHp;
    _speed = _data->getSpeed();
}

void Enemy::addAnimation()
{
    _anim = CCArmature::create(_data->getName().c_str());
    
    if(_data->getType() == EnemyType_Normal)
        _anim->setScale(0.75);
    else if(_data->getType() == EnemyType_Super_Boss)
        _anim->setScale(1.35);
    
    _anim->setPosition(ccp(0, _anim->boundingBox().size.height * 0.5));
    this->addChild(_anim);
    _anim->getAnimation()->setMovementEventCallFunc(this, movementEvent_selector(Enemy::onMovementEvent));
}

void Enemy::addPathMoveComponent()
{
    _pathMoveComponent = PathMoveComponent::create(_entityManager->getGame(), this);
    this->addChild(_pathMoveComponent);
    _pathMoveComponent->setDelegate(this);
    _pathMoveComponent->setSpeed(_speed);
}

void Enemy::addHpComponent()
{
    _hpComponent = HpComponent::create(_maxHp);
    this->addChild(_hpComponent);
    _hpComponent->setPosition(ccp(0, _anim->boundingBox().size.height * 1.0 + 10));
    this->hideHpComponent();
}

void Enemy::showHpComponent()
{
    _hpComponent->setVisible(true);
    this->unschedule(schedule_selector(Enemy::hideHpComponent));
    this->scheduleOnce(schedule_selector(Enemy::hideHpComponent), 2);
}

void Enemy::hideHpComponent()
{
    _hpComponent->setVisible(false);
}

void Enemy::addShadow()
{
    CCSprite* shadow = CCSprite::createWithSpriteFrameName("tower_shadow.png");
    shadow->setScale(1.5);
    this->addChild(shadow, -1);
    
//    shadow->setPosition(ccp(0, 0));
}

void Enemy::addObserver()
{
    Notify->addObserver(this, callfuncO_selector(Enemy::pauseMove), NotifyMsg_game_pause, NULL);
    Notify->addObserver(this, callfuncO_selector(Enemy::resumeMove), NotifyMsg_game_resume, NULL);
}

void Enemy::onMovementEvent(cocos2d::extension::CCArmature *, cocos2d::extension::MovementEventType type, const char *)
{
    if(type == COMPLETE)
    {
        _anim->getAnimation()->play("walk");
    }
}

void Enemy::onEnter()
{
    DestroyableEntity::onEnter();
    
    this->addObserver();
}

void Enemy::onExit()
{
    DestroyableEntity::onExit();
    
    Notify->removeAllObservers(this);
}

void Enemy::startMove(cocos2d::CCArray *path)
{
    _anim->getAnimation()->play("walk");
    _pathMoveComponent->startMove(path);
}

void Enemy::stopMove()
{
    _anim->getAnimation()->stop();
    _pathMoveComponent->stopMove();
}

void Enemy::pauseMove()
{
    _pathMoveComponent->pauseMove();
}

void Enemy::resumeMove()
{
    _pathMoveComponent->resumeMove();
}

void Enemy::onChangeDirection(PathDirection dir)
{
    float preAbsScaleX = _anim->getScaleX() > 0 ? _anim->getScaleX() : -_anim->getScaleX();
    if(dir == PathDirection_Left)
        _anim->setScaleX(-preAbsScaleX);
    else if(dir == PathDirection_Right)
        _anim->setScaleX(preAbsScaleX);
}

void Enemy::onArrived()
{
//    CCLOG("enemy::arrived");
    DestroyableEntity::killed();
    _beKilled = false;
    _entityManager->removeEnemy(this, _beKilled);
}

void Enemy::recover(int hp)
{
    DestroyableEntity::recover(hp);
    _hpComponent->refresh(_curHp);
}

void Enemy::damage(int hp)
{
    if(strcmp(_anim->getAnimation()->getCurrentMovementID().c_str(), "walk") == 0)
    {
        _anim->getAnimation()->play("damage");
    }
    
    _hpComponent->refresh(_curHp - hp);
    this->showHpComponent();
    DestroyableEntity::damage(hp);
}

void Enemy::killed()
{
    DestroyableEntity::killed();
    _beKilled = true;
    _entityManager->removeEnemy(this, _beKilled);
}

CCRect Enemy::getRect()
{
    CCRect rect = _anim->boundingBox();
    CCPoint worldZero = this->convertToWorldSpaceAR(CCPointZero);
    return CCRectMake(rect.origin.x + worldZero.x, rect.origin.y + worldZero.y, rect.size.width, rect.size.height);
}

int Enemy::getDistanceToEnd()
{
    return _pathMoveComponent->getLastDistance();
}

void Enemy::setSpeed(float speed)
{
    _speed = speed;
    _pathMoveComponent->setSpeed(_speed);
}