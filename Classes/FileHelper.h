//
//  FileHelper.h
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/10.
//
//

#ifndef __bbrabbit__FileHelper__
#define __bbrabbit__FileHelper__

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;
USING_NS_CC_EXT;
using namespace std;

using namespace rapidjson;

class FileHelper
{
public:
    
    static Document getJsonFile(const string& fileName);
};

#endif /* defined(__bbrabbit__FileHelper__) */
