//
//  Bullet.cpp
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/10.
//
//

#include "Bullet.h"
#include "NotifyManager.h"
#include "EntityManager.h"
#include "GameScene.h"
#include "DestroyableEntity.h"

Bullet* Bullet::create(EntityManager* entityManager, DestroyableEntity* target, float attackPower)
{
    Bullet* bullet = new Bullet();
    if(bullet && bullet->init(entityManager, target, attackPower))
    {
        bullet->autorelease();
        return bullet;
    }
    CC_SAFE_DELETE(bullet);
    return NULL;
}

Bullet::Bullet()
: _inPause(false)
, _speed(360)
, _moveComponent(NULL)
{
    
}

Bullet::~Bullet()
{
    
}

bool Bullet::init(EntityManager* entityManager, DestroyableEntity* target, float attackPower)
{
    if(CCNode::init())
    {
        _entityManager = entityManager;
        _target = target;
        _targetPosition = _target->getPosition();
        _attackPower = attackPower;
        
        this->initProperty();
        this->addMoveComponent();
        
        this->scheduleUpdate();
        
        return true;
    }
    return false;
}

void Bullet::initProperty()
{
    if(_entityManager->getGame()->getGameState() != GameState_Play)
        _inPause = true;
}

void Bullet::addMoveComponent()
{
    _moveComponent = MoveComponent::create(this);
    this->addChild(_moveComponent);
    _moveComponent->setDelegate(this);
    _moveComponent->setSpeed(_speed);
}

void Bullet::update(float dt)
{
    if(!_inPause)
    {
        this->updateTargetPosition();
        _moveComponent->updateTargetPosition(_targetPosition);
        
//        CCLOG("_moveComponent->getspeed = %f ,pos = %f ,%f", _moveComponent->getSpeed(), _moveComponent->getTargetPos().x, _moveComponent->getTargetPos().y);
    }
}

void Bullet::updateTargetPosition()
{
    if(_target)
        _targetPosition = ccp(_target->getRect().getMidX(), _target->getRect().getMidY());
}

void Bullet::startShoot()
{
    this->updateTargetPosition();
    _moveComponent->startMove(_targetPosition);
    if(_inPause)
        this->onPause();
}

void Bullet::onEnter()
{
    CCNode::onEnter();
    
    this->addObserver();
}

void Bullet::onExit()
{
    CCNode::onExit();
    
    Notify->removeAllObservers(this);
}

void Bullet::addObserver()
{
    Notify->addObserver(this, callfuncO_selector(Bullet::onPause), NotifyMsg_game_pause, NULL);
    Notify->addObserver(this, callfuncO_selector(Bullet::onResume), NotifyMsg_game_resume, NULL);
    Notify->addObserver(this, callfuncO_selector(Bullet::onTargetKilled), NotifyMsg_killed_destroyable_entity, NULL);
}

void Bullet::onPause()
{
    _inPause = true;
    _moveComponent->pauseMove();
}

void Bullet::onResume()
{
    _inPause = false;
    _moveComponent->resumeMove();
}

void Bullet::onTargetKilled(cocos2d::CCObject *obj)
{
    if(_target == obj)
        _target = NULL;
}

void Bullet::onArrived()
{
    if(_target)
        _target->damage(_attackPower);
    
    _entityManager->addTowerHitEffect(TowerId_Rabbit, _targetPosition);
    _entityManager->removeBullet(this);
}