//
//  GameUI.cpp
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/9.
//
//

#include "GameUI.h"
#include "Button.h"
#include "NotifyManager.h"
#include "GameScene.h"
#include "LabelImgNum.h"
#include "LabelWaves.h"
#include "PanelConstructor.h"
#include "PanelLevelup.h"
#include "EntityManager.h"
#include "SignAttack.h"
#include "SignWaveEntry.h"
#include "PopupMenu.h"

enum KGameUITag
{
    KGameUITag_Base,
    KGameUITag_ButtonSign,
};

enum KGameUIZorder
{
    KGameUIZorder_Base,
    KGameUIZorder_Sign,
    KGameUIZorder_Panel,
};

GameUI* GameUI::create(CCScene* scene, GameScene* game)
{
    GameUI* ui = new GameUI();
    if(ui && ui->init(scene, game))
    {
        ui->autorelease();
        return ui;
    }
    CC_SAFE_DELETE(ui);
    return NULL;
}

GameUI::GameUI()
{
    
}

GameUI::~GameUI()
{
    
}

bool GameUI::init(CCScene* scene, GameScene* game)
{
    if(CCLayer::init())
    {
        _scene = scene;
        _game = game;
        _screenSize = CCDirector::sharedDirector()->getWinSize();
        
        this->createButtonManager();
        this->createUIContent();
        
        return true;
    }
    return false;
}

void GameUI::createButtonManager()
{
    _buttonManager = ButtonManager::create();
    this->addChild(_buttonManager);
}

void GameUI::createUIContent()
{
    this->createLabelGold();
    this->createLabelWaves();
    this->createPanelConstructor();
    this->createPanelLevelup();
    this->createSignAttack();
    this->createSignEntry();
    
    this->createButtonAcc();
    this->createButtonPause();
    this->createButtonMenu();
}

void GameUI::createLabelGold()
{
    CCSprite* img = CCSprite::createWithSpriteFrameName("gameui_img_gold");
    CCSprite* img_bg = CCSprite::createWithSpriteFrameName("gameui_img_label_bg");
    img_bg->setFlipX(true);
    img_bg->setPosition(ccp(img->getContentSize().width * 0.5 + img_bg->getContentSize().width * 0.575, img->getContentSize().height * 0.5));
    img->addChild(img_bg, -1);
    _label_gold = LabelImgNum::create(LabelImgNumType_GameGold, img, _game->getGold());
    _label_gold->setPosition(ccp(_screenSize.width * 0.145, _screenSize.height * 0.9375));
    this->addChild(_label_gold, KGameUIZorder_Base);
}

void GameUI::createLabelWaves()
{
    _label_waves = LabelWaves::create(LabelWavesType_Normal, _game->getEntityManager()->getMaxWave());
    _label_waves->setPosition(ccp(_screenSize.width * 0.5, _screenSize.height * 0.9375));
    this->addChild(_label_waves, KGameUIZorder_Base);
}

void GameUI::createPanelConstructor()
{
    _panel_constructor = PanelConstructor::create(_game);
    this->addChild(_panel_constructor, KGameUIZorder_Panel);
}

void GameUI::createPanelLevelup()
{
    _panel_levelup = PanelLevelup::create(_game);
    this->addChild(_panel_levelup, KGameUIZorder_Panel);
}

void GameUI::createSignAttack()
{
    _sign_attack = SignAttack::create();
    this->addChild(_sign_attack, KGameUIZorder_Sign);
}

void GameUI::createSignEntry()
{
    SignWaveEntry* sign = SignWaveEntry::create();
    sign->setDuration(KTime_WaveReady - 0.1);
    sign->setPosition(_game->convertToWorldPositionInTileCenter(_game->getEntry()) + ccp(0, _game->getTileSize().height * 0.2));
    this->addChild(sign, KGameUIZorder_Sign);
}

void GameUI::createButtonAcc()
{
    CCSprite* content = CCSprite::createWithSpriteFrameName("button_gameui_bg");
    CCSprite* sign = CCSprite::createWithSpriteFrameName("button_acc_1");
    sign->setPosition(content->getContentSize() * 0.5);
    sign->setTag(KGameUITag_ButtonSign);
    content->addChild(sign);
    Button* button = Button::create(ButtonId_Game_Acc, content);
    button->setPosition(ccp(_screenSize.width * 0.71, _screenSize.height * 0.9375));
    this->addChild(button, KGameUIZorder_Base);
    _buttonManager->addButton(button);
}

void GameUI::createButtonPause()
{
    CCSprite* content = CCSprite::createWithSpriteFrameName("button_gameui_bg");
    CCSprite* sign = CCSprite::createWithSpriteFrameName("button_pause_1");
    sign->setPosition(content->getContentSize() * 0.5);
    sign->setTag(KGameUITag_ButtonSign);
    content->addChild(sign);
    Button* button = Button::create(ButtonId_Game_Pause, content);
    button->setPosition(ccp(_screenSize.width * 0.79, _screenSize.height * 0.9375));
    this->addChild(button, KGameUIZorder_Base);
    _buttonManager->addButton(button);
}

void GameUI::createButtonMenu()
{
    CCSprite* content = CCSprite::createWithSpriteFrameName("button_gameui_bg");
    CCSprite* sign = CCSprite::createWithSpriteFrameName("button_menu");
    sign->setPosition(content->getContentSize() * 0.5);
    sign->setTag(KGameUITag_ButtonSign);
    content->addChild(sign);
    Button* button = Button::create(ButtonId_Game_Menu, content);
    button->setPosition(ccp(_screenSize.width * 0.87, _screenSize.height * 0.9375));
    this->addChild(button, KGameUIZorder_Base);
    _buttonManager->addButton(button);
}

// util interface
void GameUI::showPanelConstructor(cocos2d::CCPoint tilePoint)
{
    if(_panel_constructor->getInShow())
        _panel_constructor->hide();
    else
        _panel_constructor->show(tilePoint);
}

void GameUI::showPanelLevelup(Tower *tower)
{
    if(_panel_levelup->getInShow())
        _panel_levelup->hide();
    else
        _panel_levelup->show(tower);
}

void GameUI::setAttackSignTarget(DestroyableEntity *target)
{
    _sign_attack->setTarget(target);
}

DestroyableEntity* GameUI::getAttackSignTarget()
{
    return _sign_attack->getTarget();
}

// override
void GameUI::onEnter()
{
    CCLayer::onEnter();
    CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this, -GameSceneTouchPriority_UI, true);
    
    this->addObserver();
}

void GameUI::onExit()
{
    CCLayer::onExit();
    CCDirector::sharedDirector()->getTouchDispatcher()->removeDelegate(this);
    
    Notify->removeAllObservers(this);
}

bool GameUI::ccTouchBegan(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent)
{
    bool isTrigger = false;
    
    if(_buttonManager->addEventTouchBegan(pTouch))
    {
        isTrigger = true;
    }
    
    if(!isTrigger && _panel_constructor->touchBegan(pTouch))
    {
        isTrigger = true;
    }
    else if(!isTrigger && _panel_levelup->touchBegan(pTouch))
    {
        isTrigger = true;
    }
    
    return isTrigger;
}

void GameUI::ccTouchMoved(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent)
{
    
}

void GameUI::ccTouchEnded(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent)
{
    bool isTrigger = false;
    
    if(_buttonManager->addEventTouchEnd(pTouch))
    {
        isTrigger = true;
    }
}

// observer
void GameUI::addObserver()
{
    Notify->addObserver(this, callfuncO_selector(GameUI::onButtonClick), NotifyMsg_button_click, NULL);
    Notify->addObserver(this, callfuncO_selector(GameUI::onEnterBackground), NotifyMsg_enter_background, NULL);
}

void GameUI::onButtonClick(cocos2d::CCObject *obj)
{
    Button* button = (Button*)obj;
    ButtonId bid = button->getId();
    
    if(bid == ButtonId_Game_Acc)
    {
        if(_game->getGameSpeed() == GameSpeed_Acc)
        {
            CCSprite* sprite = CCSprite::createWithSpriteFrameName("button_acc_1");
            ((CCSprite*)(button->getSpriteNormal()->getChildByTag(KGameUITag_ButtonSign)))->setDisplayFrame(CCSpriteFrame::createWithTexture(sprite->getTexture(), sprite->getTextureRect()));
            _game->updateGameSpeed(GameSpeed_Normal);
        }
        else if(_game->getGameSpeed() == GameSpeed_Normal)
        {
            CCSprite* sprite = CCSprite::createWithSpriteFrameName("button_acc_2");
            ((CCSprite*)(button->getSpriteNormal()->getChildByTag(KGameUITag_ButtonSign)))->setDisplayFrame(CCSpriteFrame::createWithTexture(sprite->getTexture(), sprite->getTextureRect()));
            _game->updateGameSpeed(GameSpeed_Acc);
        }
    }
    else if(bid == ButtonId_Game_Pause)
    {
        if(_game->getGameState() == GameState_Pause)
        {
            CCSprite* sprite = CCSprite::createWithSpriteFrameName("button_pause_1");
            ((CCSprite*)(button->getSpriteNormal()->getChildByTag(KGameUITag_ButtonSign)))->setDisplayFrame(CCSpriteFrame::createWithTexture(sprite->getTexture(), sprite->getTextureRect()));
            _game->updateGameState(GameState_Play);
        }
        else if(_game->getGameState() == GameState_Play)
        {
            CCSprite* sprite = CCSprite::createWithSpriteFrameName("button_pause_2");
            ((CCSprite*)(button->getSpriteNormal()->getChildByTag(KGameUITag_ButtonSign)))->setDisplayFrame(CCSpriteFrame::createWithTexture(sprite->getTexture(), sprite->getTextureRect()));
            _game->updateGameState(GameState_Pause);
        }
    }
    else if(bid == ButtonId_Game_Menu)
    {
        this->onEnterBackground();
    }
}

void GameUI::onEnterBackground()
{
    if(_game->getGameState() != GameState_Win &&
       _game->getGameState() != GameState_Over &&
       !_game->getInPopup())
        _scene->addChild(PopupMenu::create(_scene));
}

void GameUI::keyBackClicked()
{
    this->onEnterBackground();
}