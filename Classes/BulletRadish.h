//
//  BulletRadish.h
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/13.
//
//

#ifndef __bbrabbit__BulletRadish__
#define __bbrabbit__BulletRadish__

#include "Bullet.h"

class BulletRadish : public Bullet
{
public:
    
    static BulletRadish* create(EntityManager* entityManager, DestroyableEntity* target, float attackPower, int level);
    
protected:
    
    BulletRadish();
    virtual ~BulletRadish();
    virtual bool init(EntityManager* entityManager, DestroyableEntity* target, float attackPower, int level);
    
    virtual void update(float dt);
    
private:
    
    CC_SYNTHESIZE_READONLY(int, _level, Level);
    
    CCArmature* _anim;
};

#endif /* defined(__bbrabbit__BulletRadish__) */
