//
//  Tower.cpp
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/9.
//
//

#include "Tower.h"
#include "TowerData.h"
#include "EntityManager.h"
#include "Enemy.h"
#include "DestroyableEntity.h"
#include "GameScene.h"
#include "Util.h"
#include "NotifyManager.h"
#include "StaticDataManager.h"
#include "SignLevelup.h"
#include "StringHelper.h"
#include "AudioController.h"

Tower* Tower::create(EntityManager *entityManager, TowerData *data, CCPoint tilePoint)
{
    Tower* tower = new Tower();
    if(tower && tower->init(entityManager, data, tilePoint))
    {
        tower->autorelease();
        return tower;
    }
    CC_SAFE_DELETE(tower);
    return NULL;
}

Tower::Tower()
: _level(1)
, _fireRate(1.0f)
, _attackPower(1.0f)
, _attackRange(100)
, _freeTime(_fireRate)
, _inPause(false)
, _anim(NULL)
, _target(NULL)
, _inAttack(false)
, _sign_levelup(NULL)
, _shadow(NULL)
, _preScaleX(-1)
{
    
}

Tower::~Tower()
{
    
}

bool Tower::init(EntityManager *entityManager, TowerData *data, CCPoint tilePoint)
{
    if(CCNode::init())
    {
        _entityManager = entityManager;
        _data = data;
        _tilePoint = tilePoint;
        
        this->initTowerProperty();
        this->createAnimation();
        
        this->scheduleUpdate();
        
        return true;
    }
    return false;
}

void Tower::initTowerProperty()
{
    _fireRate = _data->getFireRate();
    _attackPower = _data->getAttackPower();
    _attackRange = _data->getAttackRange() * _entityManager->getGame()->getTileSize().width;
    
    _freeTime = 0.0f;
    
    _inPause = _entityManager->getGame()->getGameState() != GameState_Play;
    _inAttack = false;
}

void Tower::createAnimation()
{
    string str_anim = TowerData::getTowerName(_data->getId()) + "_" + StringHelper::intToString(_level);
    _anim = CCArmature::create(str_anim.c_str());
    this->addChild(_anim);
    _anim->getAnimation()->setMovementEventCallFunc(this, movementEvent_selector(Tower::onMovementEvent));
    _anim->getAnimation()->setFrameEventCallFunc(this, frameEvent_selector(Tower::onFrameEvent));
    _anim->getAnimation()->play("idle");
    _anim->setScaleX(_preScaleX);
}

void Tower::createLevelupSign()
{
    _sign_levelup = SignLevelup::create(this);
    this->addChild(_sign_levelup);
    this->resetSignPosition();
}

void Tower::resetSignPosition()
{
    _sign_levelup->setPosition(ccp(0, _anim->boundingBox().size.height * 0.5 + 20));
}

void Tower::createShadow()
{
    _shadow = CCSprite::createWithSpriteFrameName("tower_shadow.png");
    this->addChild(_shadow, -1);
    this->resetShadow();
}

void Tower::resetShadow()
{
    float scale = 1;
    float factor_height = -0.5;
    if(_level == 1)
    {
        scale = 0.9;
        factor_height = 0.315;
    }
    else if(_level == 2)
    {
        scale = 1.0;
        factor_height = 0.41;
    }
    else if(_level == 3)
    {
        scale = 1.35;
        factor_height = 0.535;
    }
    
    _shadow->setScale(scale);
    _shadow->setPosition(ccp(0, -_entityManager->getGame()->getTileSize().height * factor_height));
}

void Tower::start()
{
    _anim->getAnimation()->play("start");
    this->createLevelupSign();
    this->createShadow();
}

void Tower::click()
{
    if(!_inAttack)
        _anim->getAnimation()->play("click");
}

void Tower::levelup()
{
    Audio->playSfx("audio_tower_levelup");
    
    ++ _level;
    _data = StaticData->getTowerData(_data->getId(), _level);
    this->initTowerProperty();
    
    _anim->removeFromParent();
    this->createAnimation();
    this->resetSignPosition();
    this->resetShadow();
    
    _entityManager->addTowerLevelupEffect(this->getPosition(), _level);
}

int Tower::getLevelupCost()
{
    int cost = 10000;
    TowerData* nextData = StaticData->getTowerData(_data->getId(), _level + 1);
    if(nextData)
        cost = nextData->getCost();
    return cost;
}

bool Tower::enableLevelup()
{
    bool value = false;
    
    if(_level < 3)
    {
        if(this->getLevelupCost() <= _entityManager->getGame()->getGold())
            value = true;
    }
    return value;
}

int Tower::getDestroyPrice()
{
    int price_destroy = 0;
    float factor_destroy = 0.7;
    for(int i = _level; i > 0; --i)
    {
        TowerData* data = StaticData->getTowerData(_data->getId(), i);
        price_destroy += data->getCost() * factor_destroy;
    }
    return price_destroy;
}

void Tower::addObserver()
{
    Notify->addObserver(this, callfuncO_selector(Tower::onPause), NotifyMsg_game_pause, NULL);
    Notify->addObserver(this, callfuncO_selector(Tower::onResume), NotifyMsg_game_resume, NULL);
    Notify->addObserver(this, callfuncO_selector(Tower::onTargetKilled), NotifyMsg_killed_destroyable_entity, NULL);
}

void Tower::onEnter()
{
    CCNode::onEnter();
    
    this->addObserver();
}

void Tower::onExit()
{
    CCNode::onExit();
    
    Notify->removeAllObservers(this);
}

void Tower::update(float dt)
{
    if(!_inPause)
    {
        _freeTime += dt;
        
        if(_freeTime >= _fireRate)
        {
            if(this->selectAttackTarget())
            {
                this->attack(_target);
                _freeTime = 0.0f;
            }
        }
    }
}

bool Tower::selectAttackTarget()
{
    this->selectSelectedTarget();
    
    if(!_target)
        this->selectNearestTarget();
    
    return _target != NULL;
}

void Tower::selectSelectedTarget()
{
    bool isSelected = false;
    
    _target = _entityManager->getGame()->getSelectedTarget();
    
    if(_target && Util::isInHitRect(this->getPosition(), _attackRange, _target->getRect()))
        isSelected = true;
    else
        _target = NULL;
}

void Tower::selectNearestTarget()
{
    CCArray* enemys = _entityManager->getEnemyArr();
    CCArray* enemysInRange = CCArray::create();
    
    for(int i = 0; i < enemys->count(); ++i)
    {
        Enemy* enemy = (Enemy*)enemys->objectAtIndex(i);
        
        if(Util::isInHitRect(this->getPosition(), _attackRange, enemy->getRect()))
        {
            enemysInRange->addObject(enemy);
        }
    }
    
    Enemy* nearestEnemy = NULL;
    for(int i = enemysInRange->count() - 1; i >= 0; --i)
    {
        Enemy* enemy = (Enemy*)enemysInRange->objectAtIndex(i);
        if(nearestEnemy)
        {
            if(nearestEnemy->getDistanceToEnd() > enemy->getDistanceToEnd())
                nearestEnemy = enemy;
        }
        else
        {
            nearestEnemy = enemy;
        }
    }
    
    _target = nearestEnemy;
}

void Tower::attack(DestroyableEntity* target)
{
//    this->runAction(CCSequence::create(CCScaleTo::create(0.3, 0.5), CCScaleTo::create(0.3, 1.0), NULL));
    _name_attack = target->getPosition().x <= this->getPosition().x ? "attack_1" : "attack_2";
    _preScaleX = 1;
    _anim->setScaleX(_preScaleX);
    _anim->getAnimation()->play(_name_attack.c_str());
    _target = target;
    _inAttack = true;
}

void Tower::onPause()
{
    _inPause = true;
}

void Tower::onResume()
{
    _inPause = false;
}

void Tower::onTargetKilled(CCObject* obj)
{
    if(_target == obj)
        _target = NULL;
}

void Tower::onMovementEvent(cocos2d::extension::CCArmature *anim, cocos2d::extension::MovementEventType type, const char *name)
{
    if(type == COMPLETE)
    {
        _anim->getAnimation()->play("idle");
        
        if(strcmp(_name_attack.c_str(), "attack_1") == 0)
            _preScaleX = -1;
        else if(strcmp(_name_attack.c_str(), "attack_2") == 0)
            _preScaleX = 1;
        
        _anim->setScaleX(_preScaleX);
    }
}

void Tower::onFrameEvent(cocos2d::extension::CCBone *bone, const char *name, int, int)
{
    if(strcmp(name, "start") == 0)
    {
        _entityManager->addTowerStartEffect(this->getPosition() + ccp(0, -_entityManager->getGame()->getTileSize().height * 0.35));
    }
    else if(strcmp(name, "attack") == 0)
    {
        if(_target)
        {
            Audio->playSfx("audio_rabbit_attck");
            _entityManager->addBullet(this);
            _inAttack = false;
        }
    }
}