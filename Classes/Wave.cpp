//
//  Wave.cpp
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/10.
//
//

#include "Wave.h"
#include "EntityManager.h"
#include "GameScene.h"
#include "Enemy.h"
#include "NotifyManager.h"

Wave* Wave::create(EntityManager *entityManager, WaveData *data)
{
    Wave* wave = new Wave();
    if(wave && wave->init(entityManager, data))
    {
        wave->autorelease();
        return wave;
    }
    CC_SAFE_DELETE(wave);
    return wave;
}

Wave::Wave()
: _curEnemyIndex(0)
{
    
}

Wave::~Wave()
{
    
}

bool Wave::init(EntityManager *entityManager, WaveData *data)
{
    if(CCNode::init())
    {
        _entityManager = entityManager;
        _data = data;
        
        return true;
    }
    return false;
}

void Wave::onEnter()
{
    CCNode::onEnter();
    
    Notify->addObserver(this, callfuncO_selector(Wave::onPause), NotifyMsg_game_pause, NULL);
    Notify->addObserver(this, callfuncO_selector(Wave::onResume), NotifyMsg_game_resume, NULL);
}

void Wave::onExit()
{
    CCNode::onExit();
    
    Notify->removeAllObservers(this);
}

void Wave::onPause()
{
    this->pauseSchedulerAndActions();
}

void Wave::onResume()
{
    this->resumeSchedulerAndActions();
}

void Wave::startWave()
{
    this->addEnemy();
    this->schedule(schedule_selector(Wave::addEnemy), _data->getEnemyGapTime());
}

void Wave::endWave()
{
    this->unschedule(schedule_selector(Wave::addEnemy));
    _entityManager->removeWave(this);
}

void Wave::addEnemy()
{
    if(_curEnemyIndex < _data->getEnemyDatas()->count())
    {
        EnemyData* data = (EnemyData*)_data->getEnemyDatas()->objectAtIndex(_curEnemyIndex);
        Enemy* enemy = _entityManager->addEnemy(data);
        
        CCPoint entry = _entityManager->getGame()->getEntry();
        enemy->setPosition(_entityManager->getGame()->convertToWorldPositionInTileCenter(entry) + ccp(0, -_entityManager->getGame()->getTileSize().height * 0.5));
        enemy->startMove(_entityManager->getGame()->getPath());
        enemy->setScale(0);
        enemy->runAction(CCEaseBackOut::create(CCScaleTo::create(0.45, 1)));
        
        ++ _curEnemyIndex;
        
        if(_curEnemyIndex >= _data->getEnemyDatas()->count())
        {
            this->endWave();
        }
    }
    else
    {
        this->endWave();
    }
}