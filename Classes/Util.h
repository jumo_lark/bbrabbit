//
//  Util.h
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/9.
//
//

#ifndef __bbrabbit__Util__
#define __bbrabbit__Util__

#include "cocos2d.h"
USING_NS_CC;
using namespace std;

enum ScreenProfile
{
    ScreenProfile_1280_720,  // 1.78      iphone 5s/iphone 6/iphone 6plus 960 * 540
    ScreenProfile_800_480,   // 1.6666666
    ScreenProfile_960_640,   // 1.5       iphone 4
    ScreenProfile_1024_768,  // 1.3333333 ipad
};

class Util
{
public:
    
    // ScreenProfile
    static void registScreenProfile(CCSize screenSize);
    static ScreenProfile getScreenProfile();
    
    // encryp Pvr
    static void setPvrEncryptKeys();
    
    static CCArray* getRandomObjectArray(CCArray* objects, int randomCount);
    
    static int randomInRange(int min, int max);
    
    static CCArray* getCopyArray(CCArray* objects);  //浅拷贝
    
    static bool isInHitRect(const CCPoint& point1, float radius1, const CCPoint& point2, float radius2);
    static bool isInHitRect(const CCPoint& point1, float radius1, CCRect hitRect);
    static bool isInHitRect(CCRect hitRect1, CCRect hitRect2);
    
    static float getTargetRotation(CCPoint targetPos, CCPoint centerPos);
    
    static CCActionInterval* getPopupAction();
    static CCActionInterval* getPopupAction(float scaleFactor);
    
private:
    
    static ScreenProfile _screenProfile;
};

#endif /* defined(__bbrabbit__Util__) */
