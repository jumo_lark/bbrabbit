//
//  PhoenixEgg.h
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/10.
//
//

#ifndef __bbrabbit__PhoenixEgg__
#define __bbrabbit__PhoenixEgg__

#include "DestroyableEntity.h"
#include "cocos-ext.h"
USING_NS_CC_EXT;
using namespace std;

class EntityManager;

class PhoenixEgg : public DestroyableEntity
{
public:
    
    static PhoenixEgg* create(EntityManager* entityManager, CCPoint tilePoint, CCPoint hpPoint);
    
    virtual void recover(int hp);
    virtual void damage(int hp);
    virtual void killed();
    virtual CCRect getRect();
protected:
    
    PhoenixEgg();
    virtual ~PhoenixEgg();
    virtual bool init(EntityManager* entityManager, CCPoint tilePoint, CCPoint hpPoint);
    
    void createAnimation();
    void createHp();
    
    void refreshHp(int hp);
    
private:
    
    CC_SYNTHESIZE_READONLY(EntityManager*, _entityManager, EntityManager);
    CC_SYNTHESIZE_READONLY(CCPoint, _tilePoint, TilePoint);
    CC_SYNTHESIZE_READONLY(CCPoint, _hpPoint, HpPoint);
    
    CCArmature* _anim;
    CCSprite* _sprite;
    CCLabelBMFont* _label_hp;
};

#endif /* defined(__bbrabbit__PhoenixEgg__) */
