//
//  SignWaveEntry.h
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/12.
//
//

#ifndef __bbrabbit__SignWaveEntry__
#define __bbrabbit__SignWaveEntry__

#include "SignNode.h"

class SignWaveEntry : public SignNode
{
public:
    
    static SignWaveEntry* create();
    
protected:
    
    SignWaveEntry();
    virtual ~SignWaveEntry();
    virtual bool init();
    
    virtual void addObserver();
    void startAnimation();
    virtual void update(float dt);
    
    void onPause();
    void onResume();
    
private:
    
    CCSprite* _bg;
    CCProgressTimer* _sign;
    
    CC_SYNTHESIZE(float, _duration, Duration);
    float _freeTime;
    bool _inPause;
};


#endif /* defined(__bbrabbit__SignWaveEntry__) */
