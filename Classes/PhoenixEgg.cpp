//
//  PhoenixEgg.cpp
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/10.
//
//

#include "PhoenixEgg.h"
#include "EntityManager.h"
#include "StringHelper.h"
#include "GameScene.h"

PhoenixEgg* PhoenixEgg::create(EntityManager* entityManager, CCPoint tilePoint, CCPoint hpPoint)
{
    PhoenixEgg* egg = new PhoenixEgg();
    if(egg && egg->init(entityManager, tilePoint, hpPoint))
    {
        egg->autorelease();
        return egg;
    }
    CC_SAFE_DELETE(egg);
    return NULL;
}

PhoenixEgg::PhoenixEgg()
{
    
}

PhoenixEgg::~PhoenixEgg()
{
    
}

bool PhoenixEgg::init(EntityManager* entityManager, CCPoint tilePoint, CCPoint hpPoint)
{
    if(DestroyableEntity::init())
    {
        _entityManager = entityManager;
        _tilePoint = tilePoint;
        _hpPoint = hpPoint;
        
        _maxHp = 10;
        _curHp = _maxHp;
        
        this->createAnimation();
        this->createHp();
        
        return true;
    }
    return false;
}

void PhoenixEgg::createAnimation()
{
//    _anim = CCArmature::create("king_3");
//    this->addChild(_anim);
//    _anim->getAnimation()->playByIndex(0);
    _sprite = CCSprite::createWithSpriteFrameName("phoenix_egg_1.png");
    this->addChild(_sprite);
}

void PhoenixEgg::createHp()
{
    CCSize tileSize = _entityManager->getGame()->getTileSize();
    CCPoint diff = _hpPoint - _tilePoint;
    CCSprite* sprite_label_bg = CCSprite::createWithSpriteFrameName("gameui_egg_hp_bg");
    sprite_label_bg->setPosition(ccp(diff.x * tileSize.width, -diff.y * tileSize.height));
    this->addChild(sprite_label_bg);
    
    if(diff.y > 0)
        sprite_label_bg->setPosition(sprite_label_bg->getPosition() + ccp(0, -tileSize.height * 0.2));
    
    _label_hp = CCLabelBMFont::create(StringHelper::intToString(_curHp).c_str(), "Fonts/font_gamescene.fnt");
    _label_hp->setScale(0.5);
    _label_hp->setPosition(ccp(sprite_label_bg->getContentSize().width * 0.725, sprite_label_bg->getContentSize().height * 0.5));
    sprite_label_bg->addChild(_label_hp);
}

void PhoenixEgg::refreshHp(int hp)
{
    _label_hp->setString(StringHelper::intToString(hp).c_str());
    
    if(hp == 9)
        _sprite->setDisplayFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("phoenix_egg_2.png"));
    else if(hp == 5)
        _sprite->setDisplayFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("phoenix_egg_3.png"));
    else if(hp == 1)
        _sprite->setDisplayFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("phoenix_egg_4.png"));
}

void PhoenixEgg::recover(int hp)
{
    DestroyableEntity::recover(hp);
    this->refreshHp(_curHp);
}

void PhoenixEgg::damage(int hp)
{
    int curHp = _curHp - hp;
    if(curHp < 0)
        curHp = 0;
    this->refreshHp(curHp);
    DestroyableEntity::damage(hp);
}

void PhoenixEgg::killed()
{
    DestroyableEntity::killed();
    _entityManager->removeEgg();
}

CCRect PhoenixEgg::getRect()
{
    CCRect rect = _anim->boundingBox();
    CCPoint worldZero = this->convertToWorldSpaceAR(CCPointZero);
    return CCRectMake(rect.origin.x + worldZero.x, rect.origin.y + worldZero.y, rect.size.width, rect.size.height);
}