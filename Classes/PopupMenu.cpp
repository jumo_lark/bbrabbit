//
//  PopupMenu.cpp
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/11.
//
//

#include "PopupMenu.h"
#include "Button.h"
#include "FormSprite.h"

PopupMenu* PopupMenu::create(cocos2d::CCScene *scene)
{
    PopupMenu* popup = new PopupMenu();
    if(popup && popup->init(scene))
    {
        popup->autorelease();
        return popup;
    }
    CC_SAFE_DELETE(popup);
    return NULL;
}

PopupMenu::PopupMenu()
{
    
}

PopupMenu::~PopupMenu()
{
    
}

bool PopupMenu::init(cocos2d::CCScene *scene)
{
    if(PopupLayer::init(scene))
    {
        _id = PopupId_Menu;
        
        this->createTitleBg();
        this->createButtonContinue();
        this->createButtonReplay();
        this->createButtonReturnMap();
        
        return true;
    }
    return false;
}

void PopupMenu::createBackground()
{
    _size = ccp(281, 279);
    
    _bg = FormSprite::createWithCorner(CCSprite::createWithSpriteFrameName("img_popup_bg_corner"), CCSprite::createWithSpriteFrameName("img_popup_bg_content"), _size.width, _size.height, CornerState::createWithData(false, false, true, true));
    _bg->setPosition(ccp(_screenSize.width * 0.5, _screenSize.height * 0.45));
    this->addChild(_bg);
}

void PopupMenu::createTitleBg()
{
    CCSprite* sprite_title_left = CCSprite::createWithSpriteFrameName("img_popup_title_blue");
    sprite_title_left->setAnchorPoint(ccp(0, 1));
    sprite_title_left->setPosition(ccp(-_size.width * 0.5, _size.height * 0.65));
    sprite_title_left->setScale(0.7375);
    _bg->addChild(sprite_title_left, -1);
    
    CCSprite* sprite_title_right = CCSprite::createWithSpriteFrameName("img_popup_title_blue");
    sprite_title_right->setFlipX(true);
    sprite_title_right->setAnchorPoint(ccp(1, 1));
    sprite_title_right->setPosition(ccp(_size.width * 0.5, _size.height * 0.65));
    sprite_title_right->setScale(0.7375);
    _bg->addChild(sprite_title_right, -1);
    
    CCSprite* sprite_light = CCSprite::createWithSpriteFrameName("img_popup_light_point");
    sprite_light->setPosition(ccp(sprite_title_left->getContentSize().width * 0.105, sprite_title_left->getContentSize().height * 0.82));
    sprite_title_left->addChild(sprite_light);
}

void PopupMenu::createButtonContinue()
{
    CCSprite* content = CCSprite::createWithSpriteFrameName("button_blue_long");
    CCLabelBMFont* font_info = CCLabelBMFont::create("继续游戏", "Fonts/font_popup.fnt");
//    font_info->setScale(1.2);
    Button* button = Button::create(ButtonId_Game_Continue, content, NULL, font_info);
    button->setPosition(ccp(0, _size.height * 0.3));
    _bg->addChild(button);
    _buttonManager->addButton(button);
}

void PopupMenu::createButtonReplay()
{
    CCSprite* content = CCSprite::createWithSpriteFrameName("button_orange_long");
    CCLabelBMFont* font_info = CCLabelBMFont::create("重新开始", "Fonts/font_popup.fnt");
//    font_info->setScale(1.2);
    Button* button = Button::create(ButtonId_Game_Replay, content, NULL, font_info);
    button->setPosition(ccp(0, -_size.height * 0));
    _bg->addChild(button);
    _buttonManager->addButton(button);
}

void PopupMenu::createButtonReturnMap()
{
    CCSprite* content = CCSprite::createWithSpriteFrameName("button_red_long");
    CCLabelBMFont* font_info = CCLabelBMFont::create("返回地图", "Fonts/font_popup.fnt");
//    font_info->setScale(1.2);
    Button* button = Button::create(ButtonId_Game_ReturnMap, content, NULL, font_info);
    button->setPosition(ccp(0, -_size.height * 0.3));
    _bg->addChild(button);
    _buttonManager->addButton(button);
}

void PopupMenu::onButtonClick(cocos2d::CCObject *obj)
{
    Button* button = (Button*)obj;
    ButtonId bid = button->getId();
    
    if(bid == ButtonId_Game_Continue)
    {
        this->removeFromParent();
    }
    else if(bid == ButtonId_Game_Replay)
    {
        // Nothing Todo
    }
    else if(bid == ButtonId_Game_ReturnMap)
    {
        // Nothing Todo
    }
}