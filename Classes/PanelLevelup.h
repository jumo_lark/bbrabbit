//
//  PanelLevelup.h
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/10.
//
//

#ifndef __bbrabbit__PanelLevelup__
#define __bbrabbit__PanelLevelup__

#include "cocos2d.h"
USING_NS_CC;

class GameScene;
class ButtonManager;
class Tower;
class Button;

class PanelLevelup : public CCNode
{
public:
    
    static PanelLevelup* create(GameScene* game);
    
    virtual void onEnter();
    virtual void onExit();
    
    bool touchBegan(CCTouch* pTouch);
    
    void show(Tower* tower);
    void hide();
    
protected:
    
    PanelLevelup();
    virtual ~PanelLevelup();
    virtual bool init(GameScene* game);
    
    void createButtonManager();
    void createBackground();
    void createButtonLevelup();
    void createButtonDestroy();
    
    void addObserver();
    void onButtonTouch(CCObject* obj);
    void onUpdateGold(CCObject* obj);
    
    void refresh(Tower* tower);
    
private:
    
    GameScene* _game;
    
    ButtonManager* _buttonManager;
    CCSprite* _bg;
    CCSize _size;
    CCSize _tileSize;
    Button* _button_levelup;
    CCLabelBMFont* _font_price_levelup;
    Button* _button_destroy;
    CCLabelBMFont* _font_price_destroy;
    
    Tower* _tower;
    
    CC_SYNTHESIZE_READONLY(bool, _inShow, InShow);
};

#endif /* defined(__bbrabbit__PanelLevelup__) */
