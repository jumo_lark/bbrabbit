//
//  SignNode.cpp
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/12.
//
//

#include "SignNode.h"

void SignNode::onEnter()
{
    CCNode::onEnter();
    
    this->addObserver();
}

void SignNode::onExit()
{
    CCNode::onExit();
    
    Notify->removeAllObservers(this);
}