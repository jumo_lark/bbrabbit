//
//  SignWaveEntry.cpp
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/12.
//
//

#include "SignWaveEntry.h"
#include "NotifyManager.h"

SignWaveEntry* SignWaveEntry::create()
{
    SignWaveEntry* sign = new SignWaveEntry();
    if(sign && sign->init())
    {
        sign->autorelease();
        return sign;
    }
    CC_SAFE_DELETE(sign);
    return NULL;
}

SignWaveEntry::SignWaveEntry()
: _freeTime(0)
, _duration(3)
, _inPause(false)
{
    
}

SignWaveEntry::~SignWaveEntry()
{
    
}

bool SignWaveEntry::init()
{
    if(SignNode::init())
    {
        _bg = CCSprite::createWithSpriteFrameName("gameui_img_sign_entry");
        this->addChild(_bg);
        
        _sign = CCProgressTimer::create(CCSprite::createWithSpriteFrameName("gameui_img_sign_entry_center"));
        _sign->setType(kCCProgressTimerTypeRadial);
//        _sign->setMidpoint(ccp(0, 0.5));
//        _sign->setBarChangeRate(ccp(1, 0.5));
        _sign->setPercentage(0);
        _sign->setPosition(ccp(_bg->getContentSize().width * 0.5, _bg->getContentSize().height * 0.575));
        _bg->addChild(_sign);
        
        this->setVisible(false);
        
        return true;
    }
    return false;
}

void SignWaveEntry::addObserver()
{
    Notify->addObserver(this, callfuncO_selector(SignWaveEntry::startAnimation), NotifyMsg_update_wave, NULL);
    Notify->addObserver(this, callfuncO_selector(SignWaveEntry::startAnimation), NotifyMsg_update_wave, NULL);
    Notify->addObserver(this, callfuncO_selector(SignWaveEntry::startAnimation), NotifyMsg_update_wave, NULL);
}

void SignWaveEntry::startAnimation()
{
    _freeTime = 0;
    this->setVisible(true);
    this->scheduleUpdate();
    this->runAction(CCRepeat::create(CCSequence::create(CCScaleTo::create(0.35, 0.8), CCScaleTo::create(0.35, 1), NULL), (_duration / 0.7) + 1));
}

void SignWaveEntry::update(float dt)
{
    if(!_inPause)
    {
        if(_freeTime >= _duration)
        {
            this->unscheduleUpdate();
            this->setVisible(false);
        }
        else
        {
            _freeTime += dt;
            if(_freeTime > _duration)
                _freeTime = _duration;
            
            _sign->setPercentage((_freeTime / _duration) * 100);
        }
    }
}

void SignWaveEntry::onPause()
{
    _inPause = true;
}

void SignWaveEntry::onResume()
{
    _inPause = false;
}