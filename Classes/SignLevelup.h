//
//  SignLevelup.h
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/12.
//
//

#ifndef __bbrabbit__SignLevelup__
#define __bbrabbit__SignLevelup__

#include "SignNode.h"

class Tower;

class SignLevelup : public SignNode
{
public:
    
    static SignLevelup* create(Tower* tower);
    
protected:
    
    SignLevelup();
    virtual ~SignLevelup();
    virtual bool init(Tower* tower);
    
    virtual void addObserver();
    
    void updateState();
    
private:
    
    Tower* _tower;
    CCSprite* _sign;
};

#endif /* defined(__bbrabbit__SignLevelup__) */
