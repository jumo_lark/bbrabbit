//
//  AudioController.cpp
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/13.
//
//

#include "AudioController.h"

AudioController* AudioController::_intance = NULL;

AudioController* AudioController::getInstance()
{
    if(!_intance)
    {
        _intance = new AudioController();
        _intance->init();
        _intance->onEnter();
    }
    return _intance;
}

AudioController::AudioController()
{
    
}

AudioController::~AudioController()
{
    
}

bool AudioController::init()
{
    if(CCNode::init())
    {
        _audioEngine = CocosDenshion::SimpleAudioEngine::sharedEngine();
        
        return true;
    }
    return false;
}

unsigned int AudioController::playSfx(string name)
{
    int id = 0;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
    string str_path_name = "Audio/Effects/" + name + ".ogg";
    id = _audioEngine->playEffect(str_path_name.c_str());
#endif
    return id;
}

void AudioController::stopAllEffects()
{
    _audioEngine->stopAllEffects();
}

void AudioController::playBgMusic(string name, bool loop)
{
    string str_path_name = "Audio/BgMusic/" + name + ".mp3";
    _audioEngine->playBackgroundMusic(str_path_name.c_str(), loop);
}

void AudioController::playBgMusic(string name)
{
    playBgMusic(name, true);
}

void AudioController::stopBgMusic()
{
    _audioEngine->stopBackgroundMusic();
}

void AudioController::pauseBgMusic()
{
    _audioEngine->pauseBackgroundMusic();
}

void AudioController::resumeBgMusic()
{
    _audioEngine->resumeBackgroundMusic();
}