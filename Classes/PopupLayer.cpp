//
//  PopupLayer.cpp
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/11.
//
//

#include "PopupLayer.h"
#include "NotifyManager.h"
#include "Button.h"
#include "GlobalConstant.h"
#include "Util.h"
#include "ResourceHelper.h"

PopupLayer* PopupLayer::create(cocos2d::CCScene *scene)
{
    PopupLayer* popup = new PopupLayer();
    if(popup && popup->init(scene))
    {
        popup->autorelease();
        return popup;
    }
    CC_SAFE_DELETE(popup);
    return NULL;
}

PopupLayer::PopupLayer()
: _id(PopupId_Default)
{
    
}

PopupLayer::~PopupLayer()
{
    ResourceHelper::removePopupResource();
}

bool PopupLayer::init(cocos2d::CCScene *scene)
{
    if(CCLayer::init())
    {
        _scene = scene;
        _screenSize = CCDirector::sharedDirector()->getWinSize();
        
        ResourceHelper::addPopupResource();
        
        CCLayerColor* darkLayer = CCLayerColor::create(ccc4(0, 0, 0, 150));
        this->addChild(darkLayer);
        
        this->createButtonManager();
        this->createBackground();
        
        _bg->setScale(0);
        _bg->runAction(Util::getPopupAction());
        
        return true;
    }
    return false;
}

void PopupLayer::createButtonManager()
{
    _buttonManager = ButtonManager::create();
    this->addChild(_buttonManager);
}

void PopupLayer::createBackground()
{
    _bg = CCNode::create();
    this->addChild(_bg);
    _size = CCSizeZero;
}

void PopupLayer::onEnter()
{
    CCLayer::onEnter();
    CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this, -GlobalZorder_Popup, true);
    
    this->addObserver();
    Notify->postNotification(NotifyMsg_popup_open, this);
}

void PopupLayer::onExit()
{
    CCLayer::onExit();
    CCDirector::sharedDirector()->getTouchDispatcher()->removeDelegate(this);
    Notify->removeAllObservers(this);
    Notify->postNotification(NotifyMsg_popup_close, this);
}

bool PopupLayer::ccTouchBegan(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent)
{
    bool isTrigger = false;
    
    if(_buttonManager->addEventTouchBegan(pTouch))
    {
        isTrigger = true;
    }
    
    return true;
}

void PopupLayer::ccTouchMoved(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent)
{
    
}

void PopupLayer::ccTouchEnded(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent)
{
    bool isTrigger = false;
    
    if(_buttonManager->addEventTouchEnd(pTouch))
    {
        isTrigger = true;
    }
}

void PopupLayer::addObserver()
{
    Notify->addObserver(this, callfuncO_selector(PopupLayer::onButtonClick), NotifyMsg_button_click, NULL);
}

void PopupLayer::onButtonClick(cocos2d::CCObject *obj)
{
    
}
