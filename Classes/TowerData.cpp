//
//  TowerData.cpp
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/9.
//
//

#include "TowerData.h"

TowerData* TowerData::create()
{
    TowerData* data = new TowerData();
    if(data && data->init())
    {
        data->autorelease();
        return data;
    }
    CC_SAFE_DELETE(data);
    return NULL;
}

TowerData::TowerData()
: _id(TowerId_None)
, _fireRate(1.0f)
, _attackPower(1.0f)
, _attackRange(1.0f)
, _cost(120)
{
    
}

TowerData::~TowerData()
{
    
}

bool TowerData::init()
{
    return true;
}

void TowerData::deserialize(Value &value)
{
    _fireRate = value["rate"].GetDouble();
    _attackPower = value["attack"].GetDouble();
    _attackRange = value["range"].GetDouble();
    _cost = value["cost"].GetInt();
}

string TowerData::getTowerName(TowerId tid)
{
    string name = "";
    
    if(tid == TowerId_None)
        name = "common";
    else if(tid == TowerId_Rabbit)
        name = "rabbit";
    else if(tid == TowerId_Turtle)
        name = "turtle";
    else if(tid == TowerId_Cat)
        name = "cat";
    else if(tid == TowerId_Pig)
        name = "pig";
    else if(tid == TowerId_Leopard)
        name = "leopard";
    else if(tid == TowerId_Eagle_small)
        name = "eagle_small";
    else if(tid == TowerId_Eagle_big)
        name = "eagle_big";
    else if(tid == TowerId_Dragon)
        name = "dragon";
    else if(tid == TowerId_Bee)
        name = "bee";
    else if(tid == TowerId_Crocodile)
        name = "crocodile";
    else if(tid == TowerId_Lobster)
        name = "lobster";
    
    return name;
}