//
//  StaticDataManager.cpp
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/10.
//
//

#include "StaticDataManager.h"
#include "TowerData.h"
#include "EnemyData.h"
#include "WaveData.h"
#include "ItemData.h"
#include "StringHelper.h"

StaticDataManager* StaticDataManager::_instance = NULL;

StaticDataManager* StaticDataManager::getInstance()
{
    if(!_instance)
    {
        _instance = new StaticDataManager();
        _instance->init();
    }
    return _instance;
}

StaticDataManager::StaticDataManager()
: _towersData(NULL)
, _enemyDatas(NULL)
, _waveDatas(NULL)
, _itemDatas(NULL)
{
    
}

StaticDataManager::~StaticDataManager()
{
    CC_SAFE_RELEASE_NULL(_towersData);
    CC_SAFE_RELEASE_NULL(_enemyDatas);
    CC_SAFE_RELEASE_NULL(_waveDatas);
    CC_SAFE_RELEASE_NULL(_itemDatas);
}

bool StaticDataManager::init()
{
    _towersData = CCDictionary::create();
    _towersData->retain();
    
    _enemyDatas = CCDictionary::create();
    _enemyDatas->retain();
    
    _waveDatas = CCDictionary::create();
    _waveDatas->retain();
    
    _itemDatas = CCDictionary::create();
    _itemDatas->retain();
    
    this->initTowerDatas();
    this->initEnemyDatas();
    this->initWaveDatas();
    this->initItemDatas();
    
    return true;
}

void StaticDataManager::initTowerDatas()
{
    Document doc = FileHelper::getJsonFile("Data/tower.json");
    Value& root = doc;
    
    for(Value::MemberIterator it = root.MemberonBegin(); it != root.MemberonEnd(); ++it)
    {
        int towerId = StringHelper::stringToInt(it->name.GetString());
        
        CCDictionary* towerDatas = CCDictionary::create();
        _towersData->setObject(towerDatas, towerId);
        
        for(Value::MemberIterator jt = it->value.MemberonBegin(); jt != it->value.MemberonEnd(); ++jt)
        {
            int level = StringHelper::stringToInt(jt->name.GetString());
            TowerData* data = TowerData::create();
            data->deserialize(jt->value);
            data->setId((TowerId)towerId);
            towerDatas->setObject(data, level);
        }
    }
}

void StaticDataManager::initEnemyDatas()
{
    Document doc = FileHelper::getJsonFile("Data/enemy.json");
    Value& root = doc;
    
    for(Value::MemberIterator it = root.MemberonBegin(); it != root.MemberonEnd(); ++it)
    {
        string str_key = it->name.GetString();
        EnemyData* data = EnemyData::create();
        data->deserialize(it->value);
        data->setId(StringHelper::stringToInt(str_key));
        _enemyDatas->setObject(data, str_key);
    }
}

void StaticDataManager::initWaveDatas()
{
    Document doc = FileHelper::getJsonFile("Data/wave.json");
    Value& root = doc;
    
    for(Value::MemberIterator it = root.MemberonBegin(); it != root.MemberonEnd(); ++it)
    {
        WaveData* data = WaveData::create();
        data->deserialize(it->value);
        
        _waveDatas->setObject(data, it->name.GetString());
    }
}

void StaticDataManager::initItemDatas()
{
    Document doc = FileHelper::getJsonFile("Data/item.json");
    Value& root = doc;
    
    for(Value::MemberIterator it = root.MemberonBegin(); it != root.MemberonEnd(); ++it)
    {
        string str_key = it->name.GetString();
        ItemData* data = ItemData::create();
        data->deserialize(it->value);
        data->setId(StringHelper::stringToInt(str_key));
        _itemDatas->setObject(data, str_key);
    }
}

TowerData* StaticDataManager::getTowerData(int id, int level)
{
    return (TowerData*)((CCDictionary*)_towersData->objectForKey(id))->objectForKey(level);
}

WaveData* StaticDataManager::getWaveData(int id)
{
    return (WaveData*)_waveDatas->objectForKey(StringHelper::intToString(id));
}

EnemyData* StaticDataManager::getEnemyData(int id)
{
    return (EnemyData*)_enemyDatas->objectForKey(StringHelper::intToString(id));
}

ItemData* StaticDataManager::getItemData(int id)
{
    return (ItemData*)_itemDatas->objectForKey(StringHelper::intToString(id));
}