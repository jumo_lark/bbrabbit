//
//  EntityManager.cpp
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/6.
//
//

#include "EntityManager.h"
#include "GameScene.h"
#include "Enemy.h"
#include "EnemyData.h"
#include "Tower.h"
#include "Wave.h"
#include "WaveData.h"
#include "StaticDataManager.h"
#include "PhoenixEgg.h"
#include "NotifyManager.h"
#include "Bullet.h"
#include "Item.h"
#include "ItemData.h"
#include "StringHelper.h"
#include "BulletRadish.h"
#include "AudioController.h"

enum KEntityZorder
{
    KEntityZorder_Egg,
    KEntityZorder_Item,
    KEntityZorder_Effect_Tower_Back,
    KEntityZorder_Tower,
    KEntityZorder_Enemy,
    KEntityZorder_Bullet,
    KEntityZorder_Effect
};

EntityManager* EntityManager::create(GameScene *game)
{
    EntityManager* manager = new EntityManager();
    if(manager && manager->init(game))
    {
        manager->autorelease();
        return manager;
    }
    CC_SAFE_DELETE(manager);
    return NULL;
}

EntityManager::EntityManager()
: _game(NULL)
, _enemyArr(NULL)
, _itemArr(NULL)
, _towerArr(NULL)
, _waveDataArr(NULL)
, _curWaveDataIndex(0)
, _curWave(NULL)
{
    
}

EntityManager::~EntityManager()
{
    CC_SAFE_RELEASE_NULL(_enemyArr);
    CC_SAFE_RELEASE_NULL(_itemArr);
    CC_SAFE_RELEASE_NULL(_towerArr);
    CC_SAFE_RELEASE_NULL(_waveDataArr);
}

bool EntityManager::init(GameScene *game)
{
    if(CCNode::init())
    {
        _game = game;
        
        _enemyArr = CCArray::create();
        _enemyArr->retain();
        
        _itemArr = CCArray::create();
        _itemArr->retain();
        
        _towerArr = CCArray::create();
        _towerArr->retain();
        
        _waveDataArr = CCArray::create();
        _waveDataArr->retain();
        
        this->initItems();
        this->initWaves();
        
        return true;
    }
    return false;
}

void EntityManager::initItems()
{
    CCSize mapRealSize = _game->getMapRealSize();
    CCSize tileSize = _game->getTileSize();
    
    CCTMXObjectGroup* mapObjectGroup = _game->getTileMap()->objectGroupNamed("Objects");
    CCArray* objects = mapObjectGroup->getObjects();
    
    for(int i = 0; i < objects->count(); ++i)
    {
        CCDictionary* objectDic = (CCDictionary*)objects->objectAtIndex(i);
        CCString* cstr_name = (CCString*)objectDic->objectForKey("name");
     
        if(StringHelper::hasPrefix(string(cstr_name->getCString()), "Item"))
        {
            int itemId = StringHelper::stringToInt(((CCString *)objectDic->objectForKey("type"))->getCString());
            float width = ((CCString *)objectDic->objectForKey("width"))->floatValue() / tileSize.width;
            float height = ((CCString *)objectDic->objectForKey("height"))->floatValue() / tileSize.height;
            float x = ((CCString*)objectDic->objectForKey("x"))->floatValue() / tileSize.width;
            float y = (mapRealSize.height - ((CCString*)objectDic->objectForKey("y"))->floatValue()) / tileSize.height - height;
            
            ItemData* data = StaticData->getItemData(itemId);
            this->addItem(data, ccp(x, y), ccp(width, height));
        }
    }
}

void EntityManager::initWaves()
{
    // demo
    for(int i = 1; i <= 5; ++i)
    {
        _waveDataArr->addObject(StaticData->getWaveData(i));
    }
}

void EntityManager::onEnter()
{
    CCNode::onEnter();
    
    this->addObserver();
}

void EntityManager::onExit()
{
    CCNode::onExit();
    
    Notify->removeAllObservers(this);
}

void EntityManager::addObserver()
{
    Notify->addObserver(this, callfuncO_selector(EntityManager::onPause), NotifyMsg_game_pause, NULL);
    Notify->addObserver(this, callfuncO_selector(EntityManager::onResume), NotifyMsg_game_resume, NULL);
}

void EntityManager::onPause()
{
    this->pauseSchedulerAndActions();
}

void EntityManager::onResume()
{
    this->resumeSchedulerAndActions();
}

// public

/*
 * Enemy
 */
Enemy* EntityManager::addEnemy(EnemyData *data)
{
    Enemy* enemy = Enemy::create(this, data);
    this->addChild(enemy, KEntityZorder_Enemy);
    _enemyArr->addObject(enemy);
    return enemy;
}

void EntityManager::removeEnemy(Enemy *enemy, bool beKilled)
{
    Audio->playSfx("audio_enemy_1");
    Notify->postNotification(NotifyMsg_killed_enemy, enemy);
    
    if(beKilled)
    {
        _game->addGold(enemy->getData()->getGold(), enemy->getPosition());
    }
    else
    {
        int damagePower = 1;
        _egg->damage(damagePower);
    }
    
    CCRect rect = enemy->getRect();
    CCPoint pos_target_center = ccp(rect.getMidX(), rect.getMidY());
    this->addDeathEffect(pos_target_center);
    
    enemy->removeFromParent();
    _enemyArr->removeObject(enemy);

    if(_enemyArr->count() == 0 && _curWave == NULL)
    {
        if(_curWaveDataIndex < _waveDataArr->count())
        {
            this->readyAddWave();
        }
        else
        {
            this->endWave();
        }
    }
}

Enemy* EntityManager::getEnemy(cocos2d::CCPoint worldPosition)
{
    Enemy* enemy = NULL;
    for(int i = 0; i < _enemyArr->count(); ++i)
    {
        Enemy* enemy_i = (Enemy*)_enemyArr->objectAtIndex(i);
        if(enemy_i->getRect().containsPoint(worldPosition))
        {
            enemy = enemy_i;
            break;
        }
    }
    return enemy;
}

/*
 * Item
 */
Item* EntityManager::addItem(ItemData *data, cocos2d::CCPoint tilePoint, cocos2d::CCSize tileSize)
{
    Item* item = Item::create(this, data, tilePoint, tileSize);
    item->setPosition(item->getCenterPosition());
    this->addChild(item, KEntityZorder_Item);
    _itemArr->addObject(item);
    
    this->updateGameMapState(item, TileMapState_Item);
    
    return item;
}

void EntityManager::removeItem(Item *item, bool beKilled)
{
    _game->addGold(item->getData()->getGold(), item->getPosition());
    this->updateGameMapState(item, TileMapState_Empty);
    
    Notify->postNotification(NotifyMsg_killed_enemy, item);
    
    this->addDeathEffect(item->getPosition());
    item->removeFromParent();
    _itemArr->removeObject(item);
    
    if(_itemArr->count() == 0)
    {
        // itemClear
    }
}

Item* EntityManager::getItem(cocos2d::CCPoint tilePoint)
{
    Item* item = NULL;
    for(int i = 0; i < _itemArr->count(); ++i)
    {
        Item* item_i = (Item*)_itemArr->objectAtIndex(i);
        if(item_i->containsTilePoint(tilePoint))
        {
            item = item_i;
            break;
        }
    }
    return item;
}

void EntityManager::updateGameMapState(Item *item, TileMapState state)
{
    CCPoint tilePoint = item->getTilePoint();
    CCSize tileSize = item->getTileSize();
    for(int x = 0; x < tileSize.width; ++x)
    {
        for(int y = 0; y < tileSize.height; ++y)
        {
            CCPoint point = ccp(tilePoint.x + x, tilePoint.y + y);
            _game->getTileMapStateHelper()->setTileMapState(point, state);
        }
    }
}

/*
 * Tower
 */
Tower* EntityManager::addTower(TowerData *data, CCPoint tilePoint)
{
    _game->getTileMapStateHelper()->setTileMapState(tilePoint, TileMapState_Tower);
    
    Tower* tower = Tower::create(this, data, tilePoint);
    tower->setPosition(_game->convertToWorldPositionInTileCenter(tilePoint));
    this->addChild(tower, KEntityZorder_Tower);
    _towerArr->addObject(tower);
    tower->start();
    return tower;
}

void EntityManager::removeTower(Tower *tower)
{
    _game->addGold(tower->getDestroyPrice(), tower->getPosition());
    _game->getTileMapStateHelper()->setTileMapState(tower->getTilePoint(), TileMapState_Empty);
    
    this->addDeathEffect(tower->getPosition());
    tower->removeFromParent();
    _towerArr->removeObject(tower);
}

Tower* EntityManager::getTower(cocos2d::CCPoint tilePoint)
{
    Tower* tower = NULL;
    for(int i = 0; i < _towerArr->count(); ++i)
    {
        Tower* tower_i = (Tower*)_towerArr->objectAtIndex(i);
        if(tower_i->getTilePoint().equals(tilePoint))
        {
            tower = tower_i;
            break;
        }
    }
    return tower;
}

/*
 * Bullet
 */
Bullet* EntityManager::addBullet(Tower *tower)
{
    Bullet* bullet = NULL;
    
    if(tower->getData()->getId() == TowerId_Rabbit)
        bullet = BulletRadish::create(this, tower->getTarget(), tower->getAttackPower(), tower->getLevel());
    
    bullet->setPosition(tower->getPosition());
    this->addChild(bullet, KEntityZorder_Bullet);
    bullet->startShoot();
    
    return bullet;
}

void EntityManager::removeBullet(Bullet *bullet)
{
    bullet->removeFromParent();
    Audio->playSfx("audio_rabbit_hit");
}

/*
 * Egg
 */
void EntityManager::addEgg(cocos2d::CCPoint tilePoint, CCPoint hpPoint)
{
    _egg = PhoenixEgg::create(this, tilePoint, hpPoint);
    _egg->setPosition(_game->convertToWorldPositionInTileCenter(tilePoint));
    this->addChild(_egg, KEntityZorder_Egg);
}

void EntityManager::removeEgg()
{
    Notify->postNotification(NotifyMsg_killed_egg, _egg);
    
    this->addDeathEffect(_egg->getPosition());
    _egg->removeFromParent();
    _egg = NULL;
    
    _game->updateGameState(GameState_Over);
}

/*
 * Wave
 */
void EntityManager::readyAddWave()
{
    this->scheduleOnce(schedule_selector(EntityManager::addWave), KTime_WaveReady);
    Notify->postNotification(NotifyMsg_update_wave, CCInteger::create(_curWaveDataIndex + 1));
}

void EntityManager::addWave()
{
    if(_curWaveDataIndex < _waveDataArr->count())
    {
        WaveData* data = (WaveData*)_waveDataArr->objectAtIndex(_curWaveDataIndex);
        _curWave = Wave::create(this, data);
        this->addChild(_curWave);
        _curWave->startWave();
        
        ++_curWaveDataIndex;
    }
    else
    {
        this->endWave();
    }
}

void EntityManager::removeWave(Wave *wave)
{
    wave->removeFromParent();
    _curWave = NULL;
}

void EntityManager::endWave()
{
    _game->updateGameState(GameState_Win);
}

int EntityManager::getMaxWave()
{
    return _waveDataArr->count();
}

/*
 * Effect
 */
void EntityManager::addGoldEffect(int gold, const cocos2d::CCPoint &pos)
{
    CCArmature* anim = CCArmature::create("goldeffect");
    this->addAutoRemoveAnimation(anim, pos, KEntityZorder_Effect);
    
    CCBone* coinBone = anim->getBone("Layer2");
    CCBone* fontBone = anim->getBone("Layer3");
    
    CCSprite* coin = CCSprite::createWithSpriteFrameName("gameui_img_gold");
    coin->setPosition(ccp(-coin->getContentSize().width * 0.3, 0));
    coin->setScale(0.5);
    coinBone->addDisplay(coin, 0);
    coinBone->setVisible(true);
    
    CCLabelBMFont* font = CCLabelBMFont::create(StringHelper::intToString(gold).c_str(), "Fonts/font_num_outline.fnt");
    font->setScale(0.75);
    font->setColor(ccYELLOW);
    font->setAnchorPoint(ccp(0, 0.5));
    font->setPosition(ccp(0, - coin->getContentSize().height * 0.05));
    fontBone->addDisplay(font, 0);
    fontBone->setVisible(true);
}

void EntityManager::addDeathEffect(const CCPoint &pos)
{
    this->addAutoRemoveAnimation(CCArmature::create("death"), pos, KEntityZorder_Effect);
}

void EntityManager::addTowerStartEffect(const cocos2d::CCPoint &pos)
{
    this->addAutoRemoveAnimation(CCArmature::create("tower_start_light"), pos, KEntityZorder_Effect_Tower_Back);
}

void EntityManager::addTowerLevelupEffect(const cocos2d::CCPoint &pos, int level)
{
    CCArmature* anim = CCArmature::create("upgrade_heroInfo_showHero_rotate");
    anim->setScale(level == 2 ? 0.7 : 0.8);
    this->addAutoRemoveAnimation(anim, pos, KEntityZorder_Effect);
}

void EntityManager::addTowerHitEffect(TowerId towerId, const cocos2d::CCPoint &pos)
{
    string str_name = this->getTowerHitEffectName(towerId);
    this->addAutoRemoveAnimation(CCArmature::create(str_name.c_str()), pos, KEntityZorder_Effect);
}

void EntityManager::addAutoRemoveAnimation(CCArmature* anim, CCPoint pos, int zorder)
{
    anim->setPosition(pos);
    anim->getAnimation()->setMovementEventCallFunc(this, movementEvent_selector(EntityManager::onRemoveAnimation));
    this->addChild(anim, zorder);
    anim->getAnimation()->playByIndex(0);
}

void EntityManager::onRemoveAnimation(cocos2d::extension::CCArmature * anim, cocos2d::extension::MovementEventType type, const char * name)
{
    if(type == COMPLETE)
    {
        anim->removeFromParent();
    }
}

string EntityManager::getTowerHitEffectName(TowerId towerId)
{
    string str_name = "";
    if(towerId == TowerId_Rabbit)
    {
        str_name = "shield_hit_effect";
    }
    return str_name;
}