//
//  GlobalConstant.h
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/11.
//
//

#ifndef bbrabbit_GlobalConstant_h
#define bbrabbit_GlobalConstant_h

#include "cocos2d.h"
USING_NS_CC;

enum GlobalZorder
{
    GlobalZorder_Popup = 100,
};

const int GlobalConstant_Max_starCount = 3;

#endif
