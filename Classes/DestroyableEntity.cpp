//
//  DestroyableEntity.cpp
//  rabbit
//
//  Created by 李崧榕 on 15/2/5.
//
//

#include "DestroyableEntity.h"
#include "NotifyManager.h"

DestroyableEntity::DestroyableEntity()
: _curHp(1)
, _maxHp(1)
, _isKilled(false)
{
    
}

DestroyableEntity::~DestroyableEntity()
{
    
}

bool DestroyableEntity::init()
{
    if(CCNode::init())
    {
        return true;
    }
    return false;
}

void DestroyableEntity::recover(int hp)
{
    if(_curHp > 0)
    {
        _curHp += hp;
        if(_curHp > _maxHp)
            _curHp = _maxHp;
    }
}

void DestroyableEntity::damage(int hp)
{
    _curHp -= hp;
    if(_curHp <= 0)
    {
        _curHp = 0;
        this->killed();
    }
}

void DestroyableEntity::killed()
{
    _isKilled = true;
    Notify->postNotification(NotifyMsg_killed_destroyable_entity, this);
}