//
//  NotifyManager.h
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/9.
//
//

#ifndef __bbrabbit__NotifyManager__
#define __bbrabbit__NotifyManager__

#include "cocos2d.h"
USING_NS_CC;
using namespace std;

#include "NotifyMsg.h"

#define Notify NotifyManager::getInstance()

class NotifyManager : public CCObject
{
public:
    
    static NotifyManager* getInstance();
    
    void addObserver(CCObject *target, SEL_CallFuncO selector, NotifyMsg msg, CCObject *sender);
    
    void removeObserver(CCObject *target, NotifyMsg msg);
    void removeAllObservers(CCObject *target);
    
    void postNotification(NotifyMsg msg);
    void postNotification(NotifyMsg msg, CCObject *sender);
    
protected:
    
    NotifyManager();
    virtual ~NotifyManager();
    virtual bool init();
    
    static NotifyManager* _instance;
    
    string getStrMsg(NotifyMsg msg);
    
private:
    
    CCNotificationCenter* _notify;
};

#endif /* defined(__bbrabbit__NotifyManager__) */
