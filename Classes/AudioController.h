//
//  AudioController.h
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/13.
//
//

#ifndef __bbrabbit__AudioController__
#define __bbrabbit__AudioController__

#include "cocos2d.h"
USING_NS_CC;
#include "SimpleAudioEngine.h"
using namespace std;

#define Audio AudioController::getInstance()

class AudioController : public CCNode
{
public:
    
    static AudioController* getInstance();
    
    unsigned int playSfx(string name);
    void stopAllEffects();
    
    void playBgMusic(string name, bool loop);
    void playBgMusic(string name);
    void stopBgMusic();
    void pauseBgMusic();
    void resumeBgMusic();
    
protected:
    
    AudioController();
    virtual ~AudioController();
    virtual bool init();
    
    static AudioController* _intance;
    
private:
    
    CocosDenshion::SimpleAudioEngine* _audioEngine;
};

#endif /* defined(__bbrabbit__AudioController__) */
