//
//  EnemyData.cpp
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/6.
//
//

#include "EnemyData.h"
#include "StringHelper.h"
#include "StaticDataManager.h"

EnemyData* EnemyData::create()
{
    EnemyData* data = new EnemyData();
    if(data && data->init())
    {
        data->autorelease();
        return data;
    }
    CC_SAFE_DELETE(data);
    return NULL;
}

EnemyData::EnemyData()
: _id(0)
, _type(EnemyType_Normal)
, _name("enemy_1")
, _hp(1)
, _speed(100)
{
    
}

EnemyData::~EnemyData()
{
    
}

bool EnemyData::init()
{
    return true;
}

void EnemyData::deserialize(Value &value)
{
    string str_value = value.GetString();
    vector<string> vec_values = StringHelper::componentsSeparatedByString(str_value, KSign_Separator_StaticData);
    
    _type = (EnemyType)StringHelper::stringToInt(vec_values[0]);
    _name = vec_values[1];
    _hp = StringHelper::stringToInt(vec_values[2]);
    _speed = StringHelper::stringToFloat(vec_values[3]);
    _gold = StringHelper::stringToInt(vec_values[4]);
}