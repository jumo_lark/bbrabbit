//
//  PopupWin.h
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/11.
//
//

#ifndef __bbrabbit__PopupWin__
#define __bbrabbit__PopupWin__

#include "PopupLayer.h"

class PopupWin : public PopupLayer
{
public:
    
    static PopupWin* create(CCScene* scene, int starCount);
    
protected:
    
    PopupWin();
    virtual ~PopupWin();
    virtual bool init(CCScene* scene, int starCount);
    
    virtual void createBackground();
    void createTitleBg();
    void createStars();
    void createImg();
    void createRewardInfo();
    void createButtonReplay();
    void createButtonNextLevel();
    
private:
    
    // observer
    virtual void onButtonClick(CCObject* obj);
    
    int _starCount;
};

#endif /* defined(__bbrabbit__PopupWin__) */
