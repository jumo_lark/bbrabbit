//
//  PanelConstructor.cpp
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/9.
//
//

#include "PanelConstructor.h"
#include "GameScene.h"
#include "Button.h"
#include "NotifyManager.h"
#include "TowerData.h"
#include "StaticDataManager.h"
#include "StringHelper.h"

enum KConstructorTag
{
    KConstructorTag_Base,
    KConstructorTag_Price_Bg,
    KConstructorTag_Price,
};

const ccColor3B KColor_Enable = ccc3(255, 255, 255);
const ccColor3B KColor_Unable = ccc3(100, 100, 100);

PanelConstructor* PanelConstructor::create(GameScene *game)
{
    PanelConstructor* panel = new PanelConstructor();
    if(panel && panel->init(game))
    {
        panel->autorelease();
        return panel;
    }
    CC_SAFE_DELETE(panel);
    return NULL;
}

PanelConstructor::PanelConstructor()
: _inShow(false)
{
    
}

PanelConstructor::~PanelConstructor()
{
    
}

bool PanelConstructor::init(GameScene *game)
{
    if(CCNode::init())
    {
        _game = game;
        
        this->createButtonManager();
        this->createBackground();
        this->createButtonConstructorTowers();
        
        this->hide();
        
        return true;
    }
    return false;
}

void PanelConstructor::createButtonManager()
{
    _buttonManager = ButtonManager::create();
    this->addChild(_buttonManager);
}

void PanelConstructor::createBackground()
{
    CCSprite* sprite_center = CCSprite::createWithSpriteFrameName("gameui_img_sign_constructor");
    sprite_center->runAction(CCRepeatForever::create(CCSequence::create(CCScaleTo::create(0.2, 0.9), CCScaleTo::create(0.2, 1), NULL)));
    this->addChild(sprite_center);
    
    _bg = CCSprite::createWithSpriteFrameName("gameui_img_panel_constructor_bg");
    _bg->setPosition(ccp(0, _game->getTileSize().height * 0.625 + _bg->getContentSize().height * 0.5));
    this->addChild(_bg);
    _size = _bg->getContentSize();
}

void PanelConstructor::createButtonConstructorTowers()
{
    TowerData* data = StaticData->getTowerData(TowerId_Rabbit, 1);
    
    CCSprite* content = CCSprite::createWithSpriteFrameName("img_icon_rabbit");
    content->setPosition(ccp(0, content->getContentSize().height * 0.1));
    Button* button = Button::create(ButtonId_GameUI_Constructor, content);
    button->setPosition(_size * 0.5);
    _bg->addChild(button);
    _buttonManager->addButton(button);
    // price
    CCSprite* sprite_price_bg = CCSprite::createWithSpriteFrameName("gameui_img_price_bg");
    sprite_price_bg->setPosition(ccp(content->getContentSize().width * 0.5, -content->getContentSize().height * 0));
    sprite_price_bg->setTag(KConstructorTag_Price_Bg);
    content->addChild(sprite_price_bg);
    
    CCLabelBMFont* font_price = CCLabelBMFont::create(StringHelper::intToString(data->getCost()).c_str(), "Fonts/font_num.fnt");
    font_price->setPosition(ccp(sprite_price_bg->getContentSize().width * 0.6, sprite_price_bg->getContentSize().height * 0.5));
    font_price->setScale(0.5);
    font_price->setTag(KConstructorTag_Price);
    sprite_price_bg->addChild(font_price);
    
    // demo tower is rabbit
    button->setExtraData(data);
}

void PanelConstructor::onEnter()
{
    CCNode::onEnter();
    
    this->addObserver();
}

void PanelConstructor::onExit()
{
    CCNode::onExit();
    
    Notify->removeAllObservers(this);
}

void PanelConstructor::addObserver()
{
    Notify->addObserver(this, callfuncO_selector(PanelConstructor::onButtonTouch), NotifyMsg_button_touch, NULL);
    Notify->addObserver(this, callfuncO_selector(PanelConstructor::onUpdateGold), NotifyMsg_update_gold, NULL);
}

void PanelConstructor::onButtonTouch(cocos2d::CCObject *obj)
{
    Button* button = (Button*)obj;
    ButtonId bid = button->getId();
    if(bid == ButtonId_GameUI_Constructor)
    {
        button->resume();
        
        TowerData* data = (TowerData*)button->getExtraData();
        
        if(data->getCost() <= _game->getGold())
        {
            _game->costGold(data->getCost());
            _game->addTower(data, _tilePoint);
            this->hide();
        }
        else
        {
            
        }
    }
}

void PanelConstructor::onUpdateGold(cocos2d::CCObject *obj)
{
    this->refresh();
}

bool PanelConstructor::touchBegan(cocos2d::CCTouch *pTouch)
{
    bool isTrigger = false;
    
    if(_inShow)
    {
        isTrigger = true;
        
        if(_buttonManager->addEventTouchBegan(pTouch))
        {
            
        }
        else
        {
            this->hide();
        }
    }
    
    return isTrigger;
}

void PanelConstructor::show(cocos2d::CCPoint tilePoint)
{
    _tilePoint = tilePoint;
    this->setVisible(true);
    this->setPosition(_game->convertToWorldPositionInTileCenter(_tilePoint));
    _inShow = true;
    this->refresh();
}

void PanelConstructor::hide()
{
    _inShow = false;
    this->setVisible(false);
}

void PanelConstructor::refresh()
{
    int gold = _game->getGold();
    
    for(int i = 0; i < _buttonManager->getButtons()->count(); ++i)
    {
        Button* button = (Button*)_buttonManager->getButtons()->objectAtIndex(i);
        TowerData* data = (TowerData*)button->getExtraData();
        if(data->getCost() <= gold)
        {
            button->getSpriteNormal()->setColor(KColor_Enable);
        }
        else
        {
            button->getSpriteNormal()->setColor(KColor_Unable);
        }
    }
    
    if(_tilePoint.y == 1)
    {
        _bg->setPosition(ccp(0, -(_game->getTileSize().height * 0.625 + _bg->getContentSize().height * 0.5)));
    }
    else
    {
        _bg->setPosition(ccp(0, _game->getTileSize().height * 0.625 + _bg->getContentSize().height * 0.5));
    }
}