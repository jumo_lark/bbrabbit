//
//  TowerData.h
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/9.
//
//

#ifndef __bbrabbit__TowerData__
#define __bbrabbit__TowerData__

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;
USING_NS_CC_EXT;
using namespace std;

using namespace rapidjson;

enum TowerId
{
    TowerId_None        = 10000, // 未初始化 Common
    TowerId_Rabbit      = 10001, // 蹦蹦兔
    TowerId_Turtle      = 10002, // 智智龟
    TowerId_Cat         = 10003, // 猫小椰
    TowerId_Pig         = 10004, // 猪哼哼
    TowerId_Leopard     = 10005, // 富贵豹
    TowerId_Eagle_small = 10006, // 鹰小宝
    TowerId_Eagle_big   = 10007, // 鹰大宝
    TowerId_Dragon      = 10008, // 小逗龙
    TowerId_Bee         = 10009, // 蜂小浪
    TowerId_Crocodile   = 10010, // 鳄斯拉
    TowerId_Lobster     = 10011, // 龙虾强
};

class TowerData : public CCObject
{
public:
    
    static TowerData* create();
    
    void deserialize(Value& value);
    
    static string getTowerName(TowerId tid);
    
protected:
    
    TowerData();
    virtual ~TowerData();
    virtual bool init();
    
private:
    
    CC_SYNTHESIZE(TowerId, _id, Id);
    CC_SYNTHESIZE_READONLY(float, _fireRate, FireRate);
    CC_SYNTHESIZE_READONLY(float, _attackPower, AttackPower);
    CC_SYNTHESIZE_READONLY(float, _attackRange, AttackRange);
    CC_SYNTHESIZE_READONLY(int, _cost, Cost);
};

#endif /* defined(__bbrabbit__TowerData__) */
