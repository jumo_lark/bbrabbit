//
//  Wave.h
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/10.
//
//

#ifndef __bbrabbit__Wave__
#define __bbrabbit__Wave__

#include "cocos2d.h"
USING_NS_CC;

#include "WaveData.h"
class EntityManager;

class Wave : public CCNode
{
public:
    
    static Wave* create(EntityManager* entityManager, WaveData* data);
    
    virtual void onEnter();
    virtual void onExit();
    
    void startWave();
    
protected:
    
    Wave();
    virtual ~Wave();
    virtual bool init(EntityManager* entityManager, WaveData* data);
    
    void endWave();
    
    void addEnemy();
    
    void onPause();
    void onResume();
    
private:
    
    EntityManager* _entityManager;
    WaveData* _data;
    
    int _curEnemyIndex;
};

#endif /* defined(__bbrabbit__Wave__) */
