//
//  Tower.h
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/9.
//
//

#ifndef __bbrabbit__Tower__
#define __bbrabbit__Tower__

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;
USING_NS_CC_EXT;
using namespace std;

class TowerData;
class EntityManager;
class DestroyableEntity;
class SignLevelup;

class Tower : public CCNode
{
public:
    
    static Tower* create(EntityManager* entityManager, TowerData* data, CCPoint tilePoint);
    
    virtual void onEnter();
    virtual void onExit();
    
    void start();
    void click();
    virtual void levelup();
    
    int getLevelupCost();
    bool enableLevelup();
    int getDestroyPrice();
    
protected:
    
    Tower();
    virtual ~Tower();
    virtual bool init(EntityManager* entityManager, TowerData* data, CCPoint tilePoint);
    
    void initTowerProperty();
    void createAnimation();
    void createLevelupSign();
    void resetSignPosition();
    void createShadow();
    void resetShadow();
    
    void addObserver();
    
    virtual void update(float dt);
    virtual bool selectAttackTarget();
    void selectSelectedTarget();
    void selectNearestTarget();
    virtual void attack(DestroyableEntity* target);
    
    void onPause();
    void onResume();
    void onTargetKilled(CCObject* obj);
    
    void onMovementEvent(CCArmature *, MovementEventType, const char *);
    void onFrameEvent(CCBone *, const char *, int, int);
    
private:
    
    CC_SYNTHESIZE_READONLY(EntityManager*, _entityManager, EntityManager);
    CC_SYNTHESIZE_READONLY(TowerData*, _data, Data);
    CC_SYNTHESIZE_READONLY(CCPoint, _tilePoint, TilePoint);
    
    CC_SYNTHESIZE_READONLY(DestroyableEntity*, _target, Target);
    CCArmature* _anim;
    SignLevelup* _sign_levelup;
    CCSprite* _shadow;
    
    // property
    CC_SYNTHESIZE_READONLY(int, _level, Level);
    CC_SYNTHESIZE_READONLY(float, _fireRate, FireRate);
    CC_SYNTHESIZE_READONLY(float, _attackPower, AttackPower);
    CC_SYNTHESIZE_READONLY(float, _attackRange, AttackRange);
    float _freeTime;
    
    // state
    bool _inPause;
    bool _inAttack;
    string _name_attack;
    float _preScaleX;
};

#endif /* defined(__bbrabbit__Tower__) */
