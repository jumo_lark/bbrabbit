//
//  PopupFailed.h
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/11.
//
//

#ifndef __bbrabbit__PopupFailed__
#define __bbrabbit__PopupFailed__

#include "PopupLayer.h"

class PopupFailed : public PopupLayer
{
public:
    
    static PopupFailed* create(CCScene* scene);
    
protected:
    
    PopupFailed();
    virtual ~PopupFailed();
    virtual bool init(CCScene* scene);
    
    virtual void createBackground();
    void createTitleBg();
    void createImg();
    void createButtonReturnMap();
    void createButtonReplay();
    
private:
    
    // observer
    virtual void onButtonClick(CCObject* obj);
};

#endif /* defined(__bbrabbit__PopupFailed__) */
