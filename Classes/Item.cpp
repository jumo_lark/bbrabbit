//
//  Item.cpp
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/12.
//
//

#include "Item.h"
#include "EntityManager.h"
#include "ItemData.h"
#include "NotifyManager.h"
#include "GameScene.h"
#include "HpComponent.h"

Item* Item::create(EntityManager *entityManager, ItemData *data, CCPoint tilePoint, CCSize tileSize)
{
    Item* item = new Item();
    if(item && item->init(entityManager, data, tilePoint, tileSize))
    {
        item->autorelease();
        return item;
    }
    CC_SAFE_DELETE(item);
    return NULL;
}

Item::Item()
: _entityManager(NULL)
, _data(NULL)
, _sprite_img(NULL)
, _beKilled(false)
{
    
}

Item::~Item()
{
    
}

bool Item::init(EntityManager *entityManager, ItemData *data, CCPoint tilePoint, CCSize tileSize)
{
    if(DestroyableEntity::init())
    {
        _entityManager = entityManager;
        _data = data;
        _tilePoint = tilePoint;
        _tileSize = tileSize;
        
        this->initProperty();
        this->addSpriteImg();
        this->addHpComponent();
        
        return true;
    }
    return false;
}

void Item::initProperty()
{
    _maxHp = _data->getHp();
    _curHp = _maxHp;
}

void Item::addSpriteImg()
{
    _sprite_img = CCSprite::createWithSpriteFrameName(_data->getName().c_str());
    this->addChild(_sprite_img);
}

void Item::addHpComponent()
{
    _hpComponent = HpComponent::create(_maxHp);
    _hpComponent->setPosition(ccp(0, _sprite_img->getContentSize().height * 0.5 + 10));
    this->addChild(_hpComponent);
    this->hideHpComponent();
}

void Item::showHpComponent()
{
    _hpComponent->setVisible(true);
    this->unschedule(schedule_selector(Item::hideHpComponent));
    this->scheduleOnce(schedule_selector(Item::hideHpComponent), 2);
}

void Item::hideHpComponent()
{
    _hpComponent->setVisible(false);
}

void Item::addObserver()
{
    
}

void Item::onEnter()
{
    DestroyableEntity::onEnter();
    
    this->addObserver();
}

void Item::onExit()
{
    DestroyableEntity::onExit();
    
    Notify->removeAllObservers(this);
}

CCPoint Item::getCenterPosition()
{
    CCPoint realPosition = _entityManager->getGame()->convertToWorldPosition(_tilePoint); // 左上角的坐标
    CCSize mapTileSize = _entityManager->getGame()->getTileSize();
    CCSize realSize = CCSizeMake(mapTileSize.width * _tileSize.width, mapTileSize.height * _tileSize.height);
    return ccp(realPosition.x + realSize.width * 0.5, realPosition.y - realSize.height * 0.5);
}

bool Item::containsTilePoint(cocos2d::CCPoint tilePoint)
{
    return (tilePoint.x >= _tilePoint.x &&
            tilePoint.x < _tilePoint.x + _tileSize.width &&
            tilePoint.y >= _tilePoint.y &&
            tilePoint.y < _tilePoint.y + _tileSize.height);
}

void Item::recover(int hp)
{
    DestroyableEntity::recover(hp);
    _hpComponent->refresh(_curHp);
}

void Item::damage(int hp)
{
    DestroyableEntity::damage(hp);
    if(!_isKilled)
    {
        _hpComponent->refresh(_curHp);
        this->showHpComponent();
    }
}

void Item::killed()
{
    DestroyableEntity::killed();
    _beKilled = true;
    _entityManager->removeItem(this, _beKilled);
}

CCRect Item::getRect()
{
    CCRect rect = _sprite_img->boundingBox();
    CCPoint worldZero = this->convertToWorldSpaceAR(CCPointZero);
    return CCRectMake(rect.origin.x + worldZero.x, rect.origin.y + worldZero.y, rect.size.width, rect.size.height);
}