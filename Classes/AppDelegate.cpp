#include "AppDelegate.h"
#include "GameScene.h"
#include "Util.h"
#include "StaticDataManager.h"
#include "ResourceHelper.h"
#include "NotifyManager.h"
#include "AudioController.h"

USING_NS_CC;

AppDelegate::AppDelegate() {

}

AppDelegate::~AppDelegate() 
{
}

bool AppDelegate::applicationDidFinishLaunching() {
    // initialize director
    CCDirector* pDirector = CCDirector::sharedDirector();
    CCEGLView* pEGLView = CCEGLView::sharedOpenGLView();

    pDirector->setOpenGLView(pEGLView);
	
    // turn on display FPS
//    pDirector->setDisplayStats(true);

    // set FPS. the default value is 1.0/60 if you don't call this
    pDirector->setAnimationInterval(1.0 / 60);

    // 适配
    CCSize screenSize = CCDirector::sharedDirector()->getWinSize();
    Util::registScreenProfile(screenSize);
//    CCEGLView::sharedOpenGLView()->setDesignResolutionSize(960, 540, kResolutionFixedHeight);
    CCEGLView::sharedOpenGLView()->setDesignResolutionSize(1136, 640, kResolutionFixedHeight);
    
    CCTexture2D::PVRImagesHavePremultipliedAlpha(true);
    Util::setPvrEncryptKeys();
    
    StaticData;
    ResourceHelper::addCommonResource();
    ResourceHelper::addGameSceneResource();
    
    // create a scene. it's an autorelease object
    CCScene *pScene = GameScene::scene();

    // run
    pDirector->runWithScene(pScene);

    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
    CCDirector::sharedDirector()->stopAnimation();

    // if you use SimpleAudioEngine, it must be pause
    // SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
    Notify->postNotification(NotifyMsg_enter_background);
    Audio->pauseBgMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    CCDirector::sharedDirector()->startAnimation();

    // if you use SimpleAudioEngine, it must resume here
    // SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
    Notify->postNotification(NotifyMsg_enter_foreground);
    Audio->resumeBgMusic();
}
