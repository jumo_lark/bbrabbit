//
//  FormSprite.cpp
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/11.
//
//

#include "FormSprite.h"

/*
 * class CornerState
 */

CornerState* CornerState::createWithData(bool lt, bool rt, bool lb, bool rb)
{
    CornerState* state = new CornerState();
    
    if(state != NULL && state->initWithData(lt, rt, lb, rb))
    {
        state->autorelease();
        return state;
    }
    
    CC_SAFE_DELETE(state);
    return NULL;
}

CornerState::CornerState()
{
    
}

CornerState::~CornerState()
{
    
}

bool CornerState::init()
{
    return this->initWithData(true, true, true, true);
}

bool CornerState::initWithData(bool lt, bool rt, bool lb, bool rb)
{
    _leftTop = lt;
    _rightTop = rt;
    _leftBottom = lb;
    _rightBottom = rb;
    
    return true;
}

/*
 * class FormSprite
 */

FormSprite* FormSprite::createWithCorner(cocos2d::CCSprite *corner, cocos2d::CCSprite *content, float width, float height)
{
    return createWithCorner(corner, content, width, height, CornerState::create());
}

FormSprite* FormSprite::createWithCorner(cocos2d::CCSprite *corner, cocos2d::CCSprite *content, float width, float height, CornerState *state)
{
    FormSprite* sprite = new FormSprite();
    
    if(sprite != NULL && sprite->initWithConrner(corner, content, width, height, state))
    {
        sprite->autorelease();
        return sprite;
    }
    
    CC_SAFE_DELETE(sprite);
    return NULL;
}

FormSprite* FormSprite::createWithLine(CCSprite* line, float length, FormSpriteLineType type)
{
    FormSprite* sprite = new FormSprite();
    
    if(sprite && sprite->initWithLine(line, length, type))
    {
        sprite->autorelease();
        return sprite;
    }
    
    CC_SAFE_DELETE(sprite);
    return NULL;
}

FormSprite* FormSprite::createWithLine(CCSprite* border, CCSprite* line, float length, FormSpriteLineType type)
{
    FormSprite* sprite = new FormSprite();
    
    if(sprite && sprite->initWithLine(border, line, length, type))
    {
        sprite->autorelease();
        return sprite;
    }
    
    CC_SAFE_DELETE(sprite);
    return NULL;
}

FormSprite::FormSprite()
: _corner(NULL)
, _content(NULL)
, _cornerState(NULL)
, _line(NULL)
, _lineBorder(NULL)
{
    
}

FormSprite::~FormSprite()
{
    CC_SAFE_RELEASE_NULL(_corner);
    CC_SAFE_RELEASE_NULL(_content);
    CC_SAFE_RELEASE_NULL(_cornerState);
    CC_SAFE_RELEASE_NULL(_line);
    CC_SAFE_RELEASE_NULL(_lineBorder);
}

bool FormSprite::initWithConrner(cocos2d::CCSprite *corner, cocos2d::CCSprite *content, float width, float height, CornerState* state)
{
    if(CCNode::init())
    {
        CCSize cornerSize = corner->boundingBox().size;
        CCSize contentSize = content->boundingBox().size;
        if(cornerSize.width * 2 > width || cornerSize.height * 2 > height)
        {
            CCLOG("宽度、宽度不能小于单元的两倍");
            return false;
        }
        
        if(cornerSize.width != contentSize.width || cornerSize.height != contentSize.height)
        {
            CCLOG("corner和content的宽高需要一致");
            return false;
        }
        _corner = corner;
        CC_SAFE_RETAIN(_corner);
        _content = content;
        CC_SAFE_RETAIN(_content);
        _cornerState = state;
        CC_SAFE_RETAIN(_cornerState);
        _width = width;
        _height = height;
        
        this->initSprite();
        
        this->setContentSize(ccp(_width, _height));
        
        return true;
    }
    
    return false;
}

bool FormSprite::initWithLine(CCSprite* line, float length, FormSpriteLineType type)
{
    if(CCNode::init())
    {
        _line = line;
        _length = length;
        _lineType = type;
        CC_SAFE_RETAIN(_line);
        
        _width = _lineType == FormSpriteLineType_Horizon ? _line->getContentSize().width : _length;
        _height = _lineType == FormSpriteLineType_Horizon ? _length : _line->getContentSize().height;
        
        this->initLineSprite(_length);
        
        this->setContentSize(ccp(_width, _height));
        
        return true;
    }
    
    return false;
}

bool FormSprite::initWithLine(cocos2d::CCSprite *border, cocos2d::CCSprite *line, float length, FormSpriteLineType type)
{
    if(CCNode::init())
    {
        _lineBorder = border;
        _line = line;
        _length = length;
        _lineType = type;
        CC_SAFE_RETAIN(_lineBorder);
        CC_SAFE_RETAIN(_line);
        
        _width = _lineType == FormSpriteLineType_Horizon ? _line->getContentSize().width : _length;
        _height = _lineType == FormSpriteLineType_Horizon ? _length : _line->getContentSize().height;
        
        this->initLineSpriteWithBorder();
        
        this->setContentSize(ccp(_width, _height));
        
        return true;
    }
    
    return false;
}

void FormSprite::initSprite()
{
    _sprite = CCSpriteBatchNode::createWithTexture(_content->getTexture());
    _sprite->setPosition(ccp(-_width * 0.5, -_height * 0.5));
    this->addChild(_sprite);
    
    CCSize unitSize = CCSizeMake(_corner->getTextureRect().size.width - 2, _corner->getTextureRect().size.height - 2);
    
    CCRect cornerRect = CCRectMake(_corner->getTextureRect().origin.x + 1, _corner->getTextureRect().origin.y + 1, _corner->getTextureRect().size.width - 2, _corner->getTextureRect().size.height - 2);
    CCRect contentRect = CCRectMake(_content->getTextureRect().origin.x + 1, _content->getTextureRect().origin.y + 1, _content->getTextureRect().size.width - 2, _content->getTextureRect().size.height - 2);
    
    //    CCLOG("unitSize.width = %f, unitSize.height = %f", unitSize.width, unitSize.height);
    
    float wFactor, hFactor;
    float wLeave, hLeave;
    int wCount, hCount;
    
    wFactor = _width / unitSize.width;
    hFactor = _height / unitSize.height;
    
    wCount = wFactor;
    hCount = hFactor;
    
    wLeave = wFactor - wCount;
    hLeave = hFactor - hCount;
    
    if(wLeave >= 0.001)
    {
        wCount += 1;
    }
    
    if(hLeave >= 0.001)
    {
        hCount += 1;
    }
    
    //    CCLOG("wFactor = %f, hFactor = %f", wFactor, hFactor);
    //    CCLOG("wCount = %d, hCount = %d", wCount, hCount);
    //    CCLOG("wLeave = %f, hLeave = %f", wLeave, hLeave);
    
    CCSprite* spriteUnit = NULL;
    
    for(int i = 0; i < wCount; ++i)
    {
        for(int j = 0; j < hCount; ++j)
        {
            if(i == 0 && j == 0) // 左上
            {
                if(_cornerState->getLeftTop())
                {
                    spriteUnit = CCSprite::createWithTexture(_corner->getTexture(), cornerRect);
                }
                else
                {
                    spriteUnit = CCSprite::createWithTexture(_content->getTexture(), contentRect);
                }
                
                spriteUnit->setPosition(ccp(unitSize.width * 0.5, _height - unitSize.height * 0.5));
            }
            else if(i == wCount - 1 && j == 0) // 右上
            {
                if(_cornerState->getRightTop())
                {
                    spriteUnit = CCSprite::createWithTexture(_corner->getTexture(), cornerRect);
                    spriteUnit->setFlipX(true);
                }
                else
                {
                    spriteUnit = CCSprite::createWithTexture(_content->getTexture(), contentRect);
                }
                
                spriteUnit->setPosition(ccp(_width - unitSize.width * 0.5, _height - unitSize.height * 0.5));
            }
            else if(i == 0 && j == hCount - 1) // 左下
            {
                if(_cornerState->getLeftBottom())
                {
                    spriteUnit = CCSprite::createWithTexture(_corner->getTexture(), cornerRect);
                    spriteUnit->setFlipY(true);
                }
                else
                {
                    spriteUnit = CCSprite::createWithTexture(_content->getTexture(), contentRect);
                }
                
                spriteUnit->setPosition(ccp(unitSize.width * 0.5, unitSize.height * 0.5));
            }
            else if(i == wCount - 1 && j == hCount - 1) // 右下
            {
                if(_cornerState->getRightBottom())
                {
                    spriteUnit = CCSprite::createWithTexture(_corner->getTexture(), cornerRect);
                    spriteUnit->setFlipX(true);
                    spriteUnit->setFlipY(true);
                }
                else
                {
                    spriteUnit = CCSprite::createWithTexture(_content->getTexture(), contentRect);
                }
                
                spriteUnit->setPosition(ccp(_width - unitSize.width * 0.5, unitSize.height * 0.5));
            }
            else if(wLeave >= 0.001 && i == wCount - 2) // 宽余
            {
                if(hLeave >= 0.001 && j == hCount - 2) // 宽高共余
                {
                    spriteUnit = CCSprite::createWithTexture(_content->getTexture(), CCRectMake(contentRect.origin.x, contentRect.origin.y, contentRect.size.width * wLeave, contentRect.size.height * hLeave));
                    spriteUnit->setPosition(ccp(unitSize.width * i + spriteUnit->boundingBox().size.width * 0.5, _height - unitSize.height * j - spriteUnit->boundingBox().size.height * 0.5));
                }
                else
                {
                    spriteUnit = CCSprite::createWithTexture(_content->getTexture(), CCRectMake(contentRect.origin.x, contentRect.origin.y, contentRect.size.width * wLeave, contentRect.size.height));
                    
                    if(j == hCount - 1)
                    {
                        spriteUnit->setPosition(ccp(unitSize.width * i + spriteUnit->boundingBox().size.width * 0.5, spriteUnit->boundingBox().size.height * 0.5));
                    }
                    else
                    {
                        spriteUnit->setPosition(ccp(unitSize.width * i + spriteUnit->boundingBox().size.width * 0.5, _height - unitSize.height * j - spriteUnit->boundingBox().size.height * 0.5));
                    }
                }
            }
            else if(hLeave >= 0.001 && j == hCount - 2) // 高余
            {
                spriteUnit = CCSprite::createWithTexture(_content->getTexture(), CCRectMake(contentRect.origin.x, contentRect.origin.y, contentRect.size.width, contentRect.size.height * hLeave));
                
                if(i == wCount - 1)
                {
                    spriteUnit->setPosition(ccp(_width - spriteUnit->boundingBox().size.width * 0.5, _height - unitSize.height * j - spriteUnit->boundingBox().size.height * 0.5));
                }
                else
                {
                    spriteUnit->setPosition(ccp(unitSize.width * i + spriteUnit->boundingBox().size.width * 0.5, _height - unitSize.height * j - spriteUnit->boundingBox().size.height * 0.5));
                }
            }
            else
            {
                spriteUnit = CCSprite::createWithTexture(_content->getTexture(), contentRect);
                
                if(i == wCount - 1)
                {
                    spriteUnit->setPosition(ccp(_width - spriteUnit->boundingBox().size.width * 0.5, _height - unitSize.height * j - spriteUnit->boundingBox().size.height * 0.5));
                }
                else if(j == hCount - 1)
                {
                    spriteUnit->setPosition(ccp(unitSize.width * i + spriteUnit->boundingBox().size.width * 0.5, spriteUnit->boundingBox().size.height * 0.5));
                }
                else
                {
                    spriteUnit->setPosition(ccp(unitSize.width * i + spriteUnit->boundingBox().size.width * 0.5, _height - unitSize.height * j - spriteUnit->boundingBox().size.height * 0.5));
                }
            }
            
            _sprite->addChild(spriteUnit);
        }
    }
}

void FormSprite::initLineSprite(float length)
{
    _sprite = CCSpriteBatchNode::createWithTexture(_line->getTexture());
    _sprite->setPosition(ccp(-_width * 0.5, -_height * 0.5));
    this->addChild(_sprite);
    
    CCSize unitSize;
    CCRect lineTextureRect = _line->getTextureRect();
    
    float factor;
    float leave;
    int count;
    
    if(_lineType == FormSpriteLineType_Horizon)
    {
        unitSize = CCSizeMake(_line->getTextureRect().size.width, _line->getTextureRect().size.height - 2);
        lineTextureRect = CCRectMake(lineTextureRect.origin.x, lineTextureRect.origin.y + 1, lineTextureRect.size.width, lineTextureRect.size.height - 2);
        
        factor = length / unitSize.height;
    }
    else if(_lineType == FormSpriteLineType_Vertical)
    {
        unitSize = CCSizeMake(_line->getTextureRect().size.width - 2, _line->getTextureRect().size.height);
        lineTextureRect = CCRectMake(lineTextureRect.origin.x + 1, lineTextureRect.origin.y, lineTextureRect.size.width - 2, lineTextureRect.size.height);
        
        factor = length / unitSize.width;
    }
    count = factor;
    leave = factor - count;
    CCLOG("count = %d", count);
    if(leave >= 0.001)
    {
        count += 1;
    }
    
    CCSprite* spriteUnit = NULL;
    
    for(int i = 0; i < count; ++i)
    {
        if(leave >= 0.001 && i == count - 1)
        {
            if(_lineType == FormSpriteLineType_Horizon)
            {
                CCRect leaveRect = CCRectMake(lineTextureRect.origin.x, lineTextureRect.origin.y, lineTextureRect.size.width, lineTextureRect.size.height * leave);;
                
                spriteUnit = CCSprite::createWithTexture(_line->getTexture(), leaveRect);
                spriteUnit->setPosition(ccp(unitSize.width * 0.5, unitSize.height * (i + 0.5 * leave)));
            }
            else if(_lineType == FormSpriteLineType_Vertical)
            {
                CCRect leaveRect = CCRectMake(lineTextureRect.origin.x, lineTextureRect.origin.y, lineTextureRect.size.width * leave, lineTextureRect.size.height);;
                
                spriteUnit = CCSprite::createWithTexture(_line->getTexture(), leaveRect);
                spriteUnit->setPosition(ccp(unitSize.width * (i + 0.5 * leave), unitSize.height * 0.5));
            }
            
            _sprite->addChild(spriteUnit);
        }
        else
        {
            spriteUnit = CCSprite::createWithTexture(_line->getTexture(), lineTextureRect);
            if(_lineType == FormSpriteLineType_Horizon)
            {
                spriteUnit->setPosition(ccp(unitSize.width * 0.5, unitSize.height * (i + 0.5)));
            }
            else if(_lineType == FormSpriteLineType_Vertical)
            {
                spriteUnit->setPosition(ccp(unitSize.width * (i + 0.5), unitSize.height * 0.5));
            }
            
            _sprite->addChild(spriteUnit);
        }
    }
}

void FormSprite::initLineSpriteWithBorder()
{
    float contentLength = 0;
    
    if(_lineType == FormSpriteLineType_Horizon)
    {
        float borderHeight = _lineBorder->getContentSize().height;
        contentLength = _length - borderHeight * 2;
    }
    else
    {
        float borderWidth = _lineBorder->getContentSize().width;
        contentLength = _length - borderWidth * 2;
    }
    
    contentLength += 2;
    
    this->initLineSprite(contentLength);
    
    if(_lineType == FormSpriteLineType_Horizon)
    {
        // upBorder
        {
            
        }
        
        // downBorder
        {
            
        }
    }
    else
    {
        CCRect rect = _lineBorder->getTextureRect();
        rect = CCRectMake(rect.origin.x, rect.origin.y, rect.size.width - 1, rect.size.height);
        
        // leftBorder
        CCSprite* sprite_leftBorder = CCSprite::createWithTexture(_lineBorder->getTexture(), rect);
        sprite_leftBorder->setPosition(ccp(-rect.size.width * 0.5, _height * 0.5));
        _sprite->addChild(sprite_leftBorder);
        
        // rightBorder
        CCSprite* sprite_rightBorder = CCSprite::createWithTexture(_lineBorder->getTexture(), rect);
        sprite_rightBorder->setFlipX(true);
        sprite_rightBorder->setPosition(ccp(_width - rect.size.width * 1.5, _height * 0.5));
        _sprite->addChild(sprite_rightBorder);
        
        _sprite->setPosition(_sprite->getPosition() + ccp(rect.size.width, 0));
    }
}

