//
//  Enemy.h
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/6.
//
//

#ifndef __bbrabbit__Enemy__
#define __bbrabbit__Enemy__

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;
USING_NS_CC_EXT;

#include "DestroyableEntity.h"
#include "PathMoveComponent.h"
class EntityManager;
class EnemyData;
class HpComponent;

class Enemy : public DestroyableEntity, public PathMoveDelegate
{
public:
    
    static Enemy* create(EntityManager* entityManager, EnemyData* data);
    
    virtual void onEnter();
    virtual void onExit();
    
    void startMove(CCArray* path);
    void stopMove();
    
    void pauseMove();
    void resumeMove();
    
    virtual void onChangeDirection(PathDirection dir);
    virtual void onArrived();
    virtual void recover(int hp);
    virtual void damage(int hp);
    virtual void killed();
    
    virtual CCRect getRect();
    int getDistanceToEnd();
    
    void setSpeed(float speed);
    
protected:
    
    Enemy();
    virtual ~Enemy();
    virtual bool init(EntityManager* entityManager, EnemyData* data);
    
    void initProperty();
    void addAnimation();
    void addPathMoveComponent();
    void addHpComponent();
    void showHpComponent();
    void hideHpComponent();
    void addShadow();
    
    void addObserver();
    
    void onMovementEvent(CCArmature *, MovementEventType, const char *);
    
private:
    
    CC_SYNTHESIZE_READONLY(EntityManager*, _entityManager, EntityManager);
    CC_SYNTHESIZE_READONLY(EnemyData*, _data, Data);
    CC_SYNTHESIZE_READONLY(float, _speed, Speed);
    
    CCArmature* _anim;
    PathMoveComponent* _pathMoveComponent;
    HpComponent* _hpComponent;
    
    CC_SYNTHESIZE_READONLY(bool, _beKilled, BeKilled);
};

#endif /* defined(__bbrabbit__Enemy__) */
