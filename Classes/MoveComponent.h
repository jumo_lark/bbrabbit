//
//  MoveComponent.h
//  rabbit
//
//  Created by 李崧榕 on 15/2/6.
//
//

#ifndef __rabbit__MoveComponent__
#define __rabbit__MoveComponent__

#include "cocos2d.h"
USING_NS_CC;

class MoveDelegate
{
public:
    virtual void onArrived() = 0;
};

class MoveComponent : public CCNode
{
public:
    
    static MoveComponent* create(CCNode* target);
    
    void startMove(CCPoint pos);
    void stopMove();
    
    void updateTargetPosition(CCPoint pos);
    
    void pauseMove();
    void resumeMove();
    
    void setDelegate(MoveDelegate* delegate);
    void removeDelegate();
    
protected:
    
    MoveComponent();
    virtual ~MoveComponent();
    virtual bool init(CCNode* target);
    
    virtual void update(float dt);
    
private:
    
    CC_SYNTHESIZE_READONLY(CCNode*, _target, Target);
    CC_SYNTHESIZE_READONLY(MoveDelegate*, _delegate, Delegate);
    
    CC_SYNTHESIZE(float, _speed, Speed);
    
    CC_SYNTHESIZE_READONLY(CCPoint, _speedUnitVector, SpeedUnitVector); // 速度的方向 单位向量
    CC_SYNTHESIZE_READONLY(CCPoint, _startPos, StartPos);
    CC_SYNTHESIZE_READONLY(CCPoint, _targetPos, TargetPos);
    
    CC_SYNTHESIZE_READONLY(bool, _inPause, InPause);
};

#endif /* defined(__rabbit__MoveComponent__) */
