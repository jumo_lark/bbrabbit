//
//  WaveData.h
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/10.
//
//

#ifndef __bbrabbit__WaveData__
#define __bbrabbit__WaveData__

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;
USING_NS_CC_EXT;
using namespace std;

using namespace rapidjson;

class WaveData : public CCObject
{
public:
    
    static WaveData* create();
    
    void deserialize(Value& value);
    
protected:
    
    WaveData();
    virtual ~WaveData();
    virtual bool init();
    
private:
    
    CC_SYNTHESIZE_READONLY(float, _enemyGapTime, EnemyGapTime);
    CC_SYNTHESIZE_READONLY(CCArray*, _enemyDatas, EnemyDatas);
};

#endif /* defined(__bbrabbit__WaveData__) */
