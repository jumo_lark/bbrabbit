//
//  Button.h
//  rabbit
//
//  Created by 李崧榕 on 15/2/5.
//
//

#ifndef __rabbit__Button__
#define __rabbit__Button__

#include "cocos2d.h"
USING_NS_CC;

#include "ButtonConfig.h"

class Button : public CCNode
{
public:
    
    static Button* create(ButtonId id, CCSprite* spriteNormal, CCSprite* spriteClick, CCNode* nodeInfo);
    static Button* create(ButtonId id, CCSprite* spriteNormal);
    static Button* create(ButtonId id, CCSprite* spriteNormal, CCSprite* spriteClick);
    
    virtual void touch();
    virtual void resume();
    virtual void click();
    
    void setExtraData(CCObject* extraData);
    
    CCRect getRect();
    
protected:
    
    Button();
    virtual ~Button();
    virtual bool init(ButtonId id, CCSprite* spriteNormal, CCSprite* spriteClick, CCNode* nodeInfo);
    
private:
    
    CC_SYNTHESIZE_READONLY(ButtonId, _id, Id);
    CC_SYNTHESIZE_READONLY(CCSprite*, _sprite_normal, SpriteNormal);
    CC_SYNTHESIZE_READONLY(CCSprite*, _sprite_click, SpriteClick);
    CC_SYNTHESIZE_READONLY(CCNode*, _node_info, NodeInfo);
    
    CC_SYNTHESIZE_READONLY(CCPoint, _scale_sprite_normal, ScaleSpriteNormal);
    CC_SYNTHESIZE_READONLY(CCPoint, _scale_node_info, ScaleNodeInfo);
    
    // data
    CC_SYNTHESIZE_READONLY(CCObject*, _extraData, ExtraData);
    
    // defult true
    CC_SYNTHESIZE(bool, _touchAble, TouchAble);
    CC_SYNTHESIZE(bool, _clickAble, ClickAble);
};

/*
 * class ButtonUtil
 */

class ButtonUtil
{
public:
    
    static int getHitButtonIndex(CCArray* buttons, CCPoint pos);
    static Button* getHitButton(CCArray* buttons, CCPoint pos);
    static int getHitButtonId(CCArray* buttons, CCPoint pos);
};

/*
 * class ButtonManager
 */

class ButtonManager : public CCNode
{
public:
    
    static ButtonManager* create();
    
    void addButton(Button* button);
    void removeButton(Button* button);
    void removeButton(int buttonId);
    
    bool addEventTouchBegan(CCTouch* pTouch);
    bool addEventTouchEnd(CCTouch* pTouch);
    
protected:
    
    ButtonManager();
    virtual ~ButtonManager();
    virtual bool init();
    
private:
    
    CC_SYNTHESIZE_READONLY(CCArray*, _buttons, Buttons);
    CC_SYNTHESIZE_READONLY(Button*, _pressedButton, PressedButton);
};

#endif /* defined(__rabbit__Button__) */
