//
//  SignNode.h
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/12.
//
//

#ifndef __bbrabbit__SignNode__
#define __bbrabbit__SignNode__

#include "cocos2d.h"
USING_NS_CC;
#include "NotifyManager.h"

class SignNode : public CCNode
{
public:
    
    virtual void onEnter();
    virtual void onExit();
    
protected:
    
    virtual void addObserver() = 0;
    
};

#endif /* defined(__bbrabbit__SignNode__) */
