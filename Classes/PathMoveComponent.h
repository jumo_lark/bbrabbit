//
//  PathMoveComponent.h
//  rabbit
//
//  Created by 李崧榕 on 15/2/6.
//
//

#ifndef __rabbit__PathMoveComponent__
#define __rabbit__PathMoveComponent__

#include "cocos2d.h"
USING_NS_CC;

#include "MoveComponent.h"
#include "AStarPathHelper.h"
class GameScene;

class PathMoveDelegate
{
public:
    virtual void onChangeDirection(PathDirection dir){};
    virtual void onArrived() = 0;
};

class PathMoveComponent : public CCNode, public MoveDelegate
{
public:
    
    static PathMoveComponent* create(GameScene* game, CCNode* target);
    
    void startMove(CCArray* path);
    void stopMove();
    
    void pauseMove();
    void resumeMove();
    
    void setDelegate(PathMoveDelegate* delegate);
    void removeDelegate();
    
    virtual void onArrived();
    
    int getLastDistance();
    
    void setSpeed(float speed);
    
protected:
    
    PathMoveComponent();
    virtual ~PathMoveComponent();
    virtual bool init(GameScene* game, CCNode* target);
    
    void movePathUnit();
    
private:
    
    CC_SYNTHESIZE_READONLY(GameScene*, _game, Game);
    CC_SYNTHESIZE_READONLY(CCNode*, _target, Target);
    CC_SYNTHESIZE_READONLY(PathMoveDelegate*, _delegate, Delegate);
    
    MoveComponent* _moveComponent;
    
    CC_SYNTHESIZE_READONLY(float, _speed, Speed);
    
    CC_SYNTHESIZE_READONLY(CCArray*, _path, Path);
    CC_SYNTHESIZE_READONLY(int, _curPathUnitIndex, CurPathUnitIndex);
};

#endif /* defined(__rabbit__PathMoveComponent__) */
