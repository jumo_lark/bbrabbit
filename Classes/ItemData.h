//
//  ItemData.h
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/12.
//
//

#ifndef __bbrabbit__ItemData__
#define __bbrabbit__ItemData__

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;
USING_NS_CC_EXT;
using namespace std;

using namespace rapidjson;

class ItemData : public CCObject
{
public:
    
    static ItemData* create();
    
    void deserialize(Value& value);
    
protected:
    
    ItemData();
    virtual ~ItemData();
    virtual bool init();
    
private:
    
    CC_SYNTHESIZE(int, _id, Id);
    CC_SYNTHESIZE_READONLY(string, _name, Name);
    CC_SYNTHESIZE_READONLY(int, _hp, Hp);
    CC_SYNTHESIZE_READONLY(int, _gold, Gold);
};

#endif /* defined(__bbrabbit__ItemData__) */
