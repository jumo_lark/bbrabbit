//
//  LabelWaves.h
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/9.
//
//

#ifndef __bbrabbit__LabelWaves__
#define __bbrabbit__LabelWaves__

#include "cocos2d.h"
USING_NS_CC;

using namespace std;

enum LabelWavesType
{
    LabelWavesType_Normal,
};

class LabelWaves : public CCNode
{
public:
    
    static LabelWaves* create(LabelWavesType type, int maxWave);
    
    virtual void onEnter();
    virtual void onExit();
    
protected:
    
    LabelWaves();
    virtual ~LabelWaves();
    virtual bool init(LabelWavesType type, int maxWave);
    
    void createLabelWaves();
    
    void onUpdateWaves(CCObject* obj);
    
private:
    
    CC_SYNTHESIZE_READONLY(LabelWavesType, _type, Type);
    CC_SYNTHESIZE_READONLY(int, _maxWave, MaxWave);
    
    CCLabelBMFont* _label_waves;
};

#endif /* defined(__bbrabbit__LabelWaves__) */
