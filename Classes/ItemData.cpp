//
//  ItemData.cpp
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/12.
//
//

#include "ItemData.h"
#include "StringHelper.h"
#include "StaticDataManager.h"

ItemData* ItemData::create()
{
    ItemData* data = new ItemData();
    if(data && data->init())
    {
        data->autorelease();
        return data;
    }
    CC_SAFE_DELETE(data);
    return NULL;
}

ItemData::ItemData()
: _id(0)
, _name("item_1_1_1")
, _hp(1)
{
    
}

ItemData::~ItemData()
{
    
}

bool ItemData::init()
{
    return true;
}

void ItemData::deserialize(Value &value)
{
    string str_value = value.GetString();
    vector<string> vec_values = StringHelper::componentsSeparatedByString(str_value, KSign_Separator_StaticData);
    
    _name = vec_values[0];
    _hp = StringHelper::stringToInt(vec_values[1]);
    _gold = StringHelper::stringToInt(vec_values[2]);
}