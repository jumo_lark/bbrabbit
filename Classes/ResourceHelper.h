//
//  ResourceHelper.h
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/11.
//
//

#ifndef __bbrabbit__ResourceHelper__
#define __bbrabbit__ResourceHelper__

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;
USING_NS_CC_EXT;
using namespace std;

#include "TowerData.h"

class ResourceHelper
{
public:
    
    // 公用资源  常驻内存
    static void addCommonResource();
    
    // Tower
    static void addTowerResource(TowerId tid);
    static void removeTowerResource(TowerId tid);
    static vector<string> getTowerArmatureNames(TowerId tid);
    
    // Popup
    static void addPopupResource();
    static void removePopupResource();
    
    // GameScene
    static void addGameSceneResource();
    static void removeGameSceneResource();
    static vector<string> getGameSceneArmatureNames();
    
    // public
    static void addBinaryArmatureInfos(vector<string> names, bool autoLoadResource);
    static void removeBinaryArmatureInfos(vector<string> names);
    
    static void addBinaryArmatureInfo(string name, bool autoLoadResource);
    static void removeBinaryArmatureInfo(string name);
};

#endif /* defined(__bbrabbit__ResourceHelper__) */
