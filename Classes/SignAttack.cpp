//
//  SignAttack.cpp
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/12.
//
//

#include "SignAttack.h"
#include "DestroyableEntity.h"

SignAttack* SignAttack::create()
{
    SignAttack* sign = new SignAttack();
    if(sign && sign->init())
    {
        sign->autorelease();
        return sign;
    }
    CC_SAFE_DELETE(sign);
    return NULL;
}

SignAttack::SignAttack()
: _target(NULL)
{
    
}

SignAttack::~SignAttack()
{
    
}

bool SignAttack::init()
{
    if(SignNode::init())
    {
        _sprite_sign = CCSprite::createWithSpriteFrameName("gameui_img_sign_attack");
        this->addChild(_sprite_sign);
        
        this->scheduleUpdate();
        
        return true;
    }
    return false;
}

void SignAttack::update(float dt)
{
    if(_target)
    {
        CCRect rect = _target->getRect();
        CCPoint pos_target_top = ccp(rect.getMidX(), rect.getMaxY());
        this->setPosition(pos_target_top + ccp(0, 20));
    }
}

void SignAttack::addObserver()
{
    Notify->addObserver(this, callfuncO_selector(SignAttack::onRemoveTarget), NotifyMsg_killed_destroyable_entity, NULL);
}

void SignAttack::onRemoveTarget(cocos2d::CCObject *obj)
{
    if(_target == obj)
        this->removeTarget();
}

void SignAttack::setTarget(DestroyableEntity *target)
{
    if(_target == target)
    {
        this->removeTarget();
    }
    else
    {
        this->setVisible(true);
        _target = target;
    }
}

void SignAttack::removeTarget()
{
    _target = NULL;
    this->setVisible(false);
}