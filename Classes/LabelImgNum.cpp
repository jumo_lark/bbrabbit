//
//  LabelImgNum.cpp
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/9.
//
//

#include "LabelImgNum.h"
#include "StringHelper.h"
#include "NotifyManager.h"

LabelImgNum* LabelImgNum::create(LabelImgNumType type, cocos2d::CCSprite *img, int num)
{
    LabelImgNum* label = new LabelImgNum();
    if(label && label->init(type, img, num))
    {
        label->autorelease();
        return label;
    }
    CC_SAFE_DELETE(label);
    return NULL;
}

LabelImgNum::LabelImgNum()
{
    
}

LabelImgNum::~LabelImgNum()
{
    
}

bool LabelImgNum::init(LabelImgNumType type, cocos2d::CCSprite *img, int num)
{
    if(CCNode::init())
    {
        _type = type;
        _sprite_img = img;
        _num = num;
        
        this->createImg();
        this->createLabelNum();
        
        return true;
    }
    return false;
}

void LabelImgNum::createImg()
{
    this->addChild(_sprite_img);
}

void LabelImgNum::createLabelNum()
{
    _label_num = CCLabelBMFont::create(StringHelper::intToString(_num).c_str(), "Fonts/font_num.fnt");
    _label_num->setScale(0.8);
    _label_num->setPosition(ccp(_sprite_img->getContentSize().width * 1.875, 0));
    this->addChild(_label_num);
}

void LabelImgNum::addObserver()
{
    if(_type == LabelImgNumType_GameGold)
        Notify->addObserver(this, callfuncO_selector(LabelImgNum::onGoldUpdate), NotifyMsg_update_gold, NULL);
}

void LabelImgNum::onGoldUpdate(cocos2d::CCObject *obj)
{
    int gold = ((CCInteger*)obj)->getValue();
    this->setNum(gold);
}

void LabelImgNum::onEnter()
{
    CCNode::onEnter();
    this->addObserver();
}

void LabelImgNum::onExit()
{
    CCNode::onExit();
    Notify->removeAllObservers(this);
}

void LabelImgNum::setNum(int num)
{
    _num = num;
    _label_num->setString(StringHelper::intToString(_num).c_str());
}
