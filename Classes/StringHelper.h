//
//  StringHelper.h
//  rabbit
//
//  Created by 李崧榕 on 15/2/5.
//
//

#ifndef __rabbit__StringHelper__
#define __rabbit__StringHelper__

#include "cocos2d.h"
USING_NS_CC;

using namespace std;

class StringHelper
{
public:
    
    static float randomFloatInRange(float minInclusive, float maxInclusive);
    static int randomIntInRange(int minInclusive, int maxInclusive);
    
    static string intToString(int num);
    static string floatToString(float num);
    static int stringToInt(const string& str);
    static float stringToFloat(const string& str);
    
    static bool hasPrefix(const string& source, const string& prefix);
    static vector<string> componentsSeparatedByString(const string& str, const string& delim);
};

#endif /* defined(__rabbit__StringHelper__) */
