//
//  GameUI.h
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/9.
//
//

#ifndef __bbrabbit__GameUI__
#define __bbrabbit__GameUI__

#include "cocos2d.h"
USING_NS_CC;

class GameScene;
class ButtonManager;
class LabelImgNum;
class LabelWaves;
class PanelConstructor;
class PanelLevelup;
class Tower;
class SignAttack;
class DestroyableEntity;

class GameUI : public CCLayer
{
public:
    
    static GameUI* create(CCScene* scene, GameScene* game);
    
    // util interface
    void showPanelConstructor(CCPoint tilePoint);
    void showPanelLevelup(Tower* tower);
    void setAttackSignTarget(DestroyableEntity* target);
    DestroyableEntity* getAttackSignTarget();
    
    // ovrride
    virtual void onEnter();
    virtual void onExit();
    virtual bool ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent);
    virtual void ccTouchMoved(CCTouch* pTouch, CCEvent* pEvent);
    virtual void ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent);
    
    virtual void keyBackClicked();
    
protected:
    
    GameUI();
    virtual ~GameUI();
    virtual bool init(CCScene* scene, GameScene* game);
    
    void createButtonManager();
    void createUIContent();
    
    void createLabelGold();
    void createLabelWaves();
    void createPanelConstructor();
    void createPanelLevelup();
    void createSignAttack();
    void createSignEntry();
    
    void createButtonAcc();
    void createButtonPause();
    void createButtonMenu();
    
private:
    
    CC_SYNTHESIZE_READONLY(CCScene*, _scene, Scene);
    CC_SYNTHESIZE_READONLY(GameScene*, _game, Game);
    CC_SYNTHESIZE_READONLY(CCSize, _screenSize, ScreenSize);
    
    CC_SYNTHESIZE_READONLY(ButtonManager*, _buttonManager, ButtonManager);
    
    // ui content
    LabelImgNum* _label_gold;
    LabelWaves* _label_waves;
    PanelConstructor* _panel_constructor;
    PanelLevelup* _panel_levelup;
    SignAttack* _sign_attack;
    
    // observer
    void addObserver();
    void onButtonClick(CCObject* obj);
    void onEnterBackground();
};

#endif /* defined(__bbrabbit__GameUI__) */
