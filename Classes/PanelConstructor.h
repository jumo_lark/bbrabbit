//
//  PanelConstructor.h
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/9.
//
//

#ifndef __bbrabbit__PanelConstructor__
#define __bbrabbit__PanelConstructor__

#include "cocos2d.h"
USING_NS_CC;

class GameScene;
class ButtonManager;

class PanelConstructor : public CCNode
{
public:
    
    static PanelConstructor* create(GameScene* game);
    
    virtual void onEnter();
    virtual void onExit();
    
    bool touchBegan(CCTouch* pTouch);
    
    void show(CCPoint tilePoint);
    void hide();
    
protected:
    
    PanelConstructor();
    virtual ~PanelConstructor();
    virtual bool init(GameScene* game);
    
    void createButtonManager();
    void createBackground();
    void createButtonConstructorTowers();
    
    void addObserver();
    void onButtonTouch(CCObject* obj);
    void onUpdateGold(CCObject* obj);
    
    void refresh();
    
private:
    
    GameScene* _game;
    
    ButtonManager* _buttonManager;
    CCSprite* _bg;
    CCSize _size;
    
    CCPoint _tilePoint;
    
    CC_SYNTHESIZE_READONLY(bool, _inShow, InShow);
};

#endif /* defined(__bbrabbit__PanelConstructor__) */
