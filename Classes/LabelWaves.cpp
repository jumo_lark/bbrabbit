//
//  LabelWaves.cpp
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/9.
//
//

#include "LabelWaves.h"
#include "StringHelper.h"
#include "NotifyManager.h"

LabelWaves* LabelWaves::create(LabelWavesType type, int maxWave)
{
    LabelWaves* label = new LabelWaves();
    if(label && label->init(type, maxWave))
    {
        label->autorelease();
        return label;
    }
    CC_SAFE_DELETE(label);
    return NULL;
}

LabelWaves::LabelWaves()
{
    
}

LabelWaves::~LabelWaves()
{
    
}

bool LabelWaves::init(LabelWavesType type, int maxWave)
{
    if(CCNode::init())
    {
        _type = type;
        _maxWave = maxWave;
        
        this->createLabelWaves();
        
        return true;
    }
    return false;
}

void LabelWaves::createLabelWaves()
{
    float width = 0.0f;
    
    CCSprite* sprite_num_bg = CCSprite::createWithSpriteFrameName("gameui_img_label_bg");
    this->addChild(sprite_num_bg);
    width += sprite_num_bg->getContentSize().width * 0.95;
    
    CCSprite* sprite_font = CCSprite::createWithSpriteFrameName("gameui_img_wave");
    this->addChild(sprite_font);
    width += sprite_font->getContentSize().width;
    
    sprite_num_bg->setPosition(ccp(-width * 0.5 + sprite_num_bg->getContentSize().width * 0.5, 0));
    sprite_font->setPosition(ccp(width * 0.5 - sprite_font->getContentSize().width * 0.5, 0));
    
    CCLabelBMFont* font_maxWave = CCLabelBMFont::create(("/" + StringHelper::intToString(_maxWave)).c_str(), "Fonts/font_num.fnt");
    font_maxWave->setAnchorPoint(ccp(0, 0.5));
    font_maxWave->setPosition(ccp(sprite_num_bg->getContentSize().width * 0.485, sprite_num_bg->getContentSize().height * 0.5));
    font_maxWave->setScale(0.8);
    sprite_num_bg->addChild(font_maxWave);
    
    string str_waves = StringHelper::intToString(1);
    _label_waves = CCLabelBMFont::create(str_waves.c_str(), "Fonts/font_num.fnt");
    _label_waves->setAnchorPoint(ccp(1, 0.5));
    _label_waves->setPosition(ccp(sprite_num_bg->getContentSize().width * 0.46, sprite_num_bg->getContentSize().height * 0.5));
    _label_waves->setScale(0.8);
    _label_waves->setColor(ccYELLOW);
    sprite_num_bg->addChild(_label_waves);
}

void LabelWaves::onEnter()
{
    CCNode::onEnter();
    
    Notify->addObserver(this, callfuncO_selector(LabelWaves::onUpdateWaves), NotifyMsg_update_wave, NULL);
}

void LabelWaves::onExit()
{
    CCNode::onExit();
    
    Notify->removeAllObservers(this);
}

void LabelWaves::onUpdateWaves(cocos2d::CCObject *obj)
{
    int curWave = ((CCInteger*)obj)->getValue();
    
    if(curWave <= _maxWave)
    {
        _label_waves->setString(StringHelper::intToString(curWave).c_str());
    }
    else
    {
        
    }
}