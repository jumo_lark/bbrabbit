//
//  Item.h
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/12.
//
//

#ifndef __bbrabbit__Item__
#define __bbrabbit__Item__

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;
USING_NS_CC_EXT;

#include "DestroyableEntity.h"
class EntityManager;
class ItemData;
class HpComponent;

class Item : public DestroyableEntity
{
public:
    
    static Item* create(EntityManager* entityManager, ItemData* data, CCPoint tilePoint, CCSize tileSize);
    
    virtual void onEnter();
    virtual void onExit();
    
    CCPoint getCenterPosition();
    bool containsTilePoint(CCPoint tilePoint);
    
    virtual void recover(int hp);
    virtual void damage(int hp);
    virtual void killed();
    
    virtual CCRect getRect();
    
protected:
    
    Item();
    virtual ~Item();
    virtual bool init(EntityManager* entityManager, ItemData* data, CCPoint tilePoint, CCSize tileSize);
    
    void initProperty();
    void addSpriteImg();
    void addHpComponent();
    void showHpComponent();
    void hideHpComponent();
    
    void addObserver();
    
private:
    CC_SYNTHESIZE_READONLY(EntityManager*, _entityManager, EntityManager);
    CC_SYNTHESIZE_READONLY(ItemData*, _data, Data);
    CC_SYNTHESIZE_READONLY(CCPoint, _tilePoint, TilePoint); // tilePoint 为左上角
    CC_SYNTHESIZE_READONLY(CCSize, _tileSize, TileSize);
    
    CCSprite* _sprite_img;
    HpComponent* _hpComponent;
    
    CC_SYNTHESIZE_READONLY(bool, _beKilled, BeKilled);
};

#endif /* defined(__bbrabbit__Item__) */
