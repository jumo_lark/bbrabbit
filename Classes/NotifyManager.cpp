//
//  NotifyManager.cpp
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/9.
//
//

#include "NotifyManager.h"
#include "StringHelper.h"

NotifyManager* NotifyManager::_instance = NULL;

NotifyManager* NotifyManager::getInstance()
{
    if(!_instance)
    {
        _instance = new NotifyManager();
        _instance->init();
    }
    
    return _instance;
}

NotifyManager::NotifyManager()
: _notify(NULL)
{
    
}

NotifyManager::~NotifyManager()
{
    
}

bool NotifyManager::init()
{
    _notify = CCNotificationCenter::sharedNotificationCenter();
    
    return true;
}

void NotifyManager::addObserver(cocos2d::CCObject *target, SEL_CallFuncO selector, NotifyMsg msg, cocos2d::CCObject *sender)
{
    _notify->addObserver(target, selector, getStrMsg(msg).c_str(), sender);
}

void NotifyManager::removeObserver(cocos2d::CCObject *target, NotifyMsg msg)
{
    _notify->removeObserver(target, getStrMsg(msg).c_str());
}

void NotifyManager::removeAllObservers(cocos2d::CCObject *target)
{
    _notify->removeAllObservers(target);
}

void NotifyManager::postNotification(NotifyMsg msg)
{
    _notify->postNotification(getStrMsg(msg).c_str());
}

void NotifyManager::postNotification(NotifyMsg msg, cocos2d::CCObject *sender)
{
    _notify->postNotification(getStrMsg(msg).c_str(), sender);
}

string NotifyManager::getStrMsg(NotifyMsg msg)
{
    return StringHelper::intToString((int)msg);
}