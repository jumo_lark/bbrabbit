//
//  ButtonConfig.h
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/9.
//
//

#ifndef bbrabbit_ButtonConfig_h
#define bbrabbit_ButtonConfig_h

const int KButtonIndex_None = -1;

enum ButtonId
{
    ButtonId_None = -1,
    ButtonId_Base = 0,
    
    // GameScene
    ButtonId_Game_Pause,
    ButtonId_Game_Acc,
    ButtonId_Game_Menu,
    
    ButtonId_Game_Continue,
    ButtonId_Game_Replay,
    ButtonId_Game_NextLevel,
    ButtonId_Game_ReturnMap,
    
    // GameUI
    ButtonId_GameUI_Constructor,
    ButtonId_GameUI_Levelup,
    ButtonId_GameUI_Destroy,
};

#endif
