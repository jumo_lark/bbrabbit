//
//  GameScene.h
//  rabbit
//
//  Created by 李崧榕 on 15/2/5.
//
//

#ifndef __rabbit__GameScene__
#define __rabbit__GameScene__

#include "cocos2d.h"
USING_NS_CC;

class PathHelper;
class TileMapStateHelper;
class EntityManager;
class GameUI;
class TowerData;
class Tower;
class DestroyableEntity;

enum GameState
{
    GameState_None,
    GameState_Play,
    GameState_Pause,
    GameState_Win,
    GameState_Over
};

enum GameSpeed
{
    GameSpeed_Normal,
    GameSpeed_Acc,
};

class GameScene : public CCLayer
{
public:
    
    static CCScene* scene();
    static GameScene* create(CCScene* scene);
    
    // util interface
    CCPoint convertToTilePoint(CCPoint worldPosition);
    CCPoint convertToWorldPosition(CCPoint tilePoint);
    CCPoint convertToWorldPositionInTileCenter(CCPoint tilePoint);
    
    void addTower(TowerData* data, CCPoint tilePoint);
    void removeTower(Tower* tower);
    
    void addGold(int gold, CCPoint pos);
    void costGold(int gold);
    
    void updateGameState(GameState state);
    void updateGameSpeed(GameSpeed speed);
    
    DestroyableEntity* getSelectedTarget();
    
    // ovrride
    virtual void onEnter();
    virtual void onExit();
    virtual bool ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent);
    virtual void ccTouchMoved(CCTouch* pTouch, CCEvent* pEvent);
    virtual void ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent);
    
protected:
    
    GameScene();
    virtual ~GameScene();
    virtual bool init(CCScene* scene);
    
    // create
    void createSceneContent();
    void createBackground();
    void createTileMap();
    void createAStartPathHelper();
    void createTileMapStateHelper();
    void createEntityManager();
    void createUI();
    
    void initSceneProperty();
    
    void addObserver();
    void onButtonClick(CCObject* obj);
    void onPopupOpen(CCObject* obj);
    void onPopupClose(CCObject* obj);
    void onPopupWin();
    void onPopupFailed();
    
private:
    
    CC_SYNTHESIZE_READONLY(CCScene*, _scene, Scene);
    CC_SYNTHESIZE_READONLY(CCSize, _screenSize, ScreenSize);
    CC_SYNTHESIZE_READONLY(CCSize, _mapSize, TileMapSize);
    CC_SYNTHESIZE_READONLY(CCSize, _tileSize, TileSize);
    CC_SYNTHESIZE_READONLY(CCSize, _mapRealSize, MapRealSize);
    CC_SYNTHESIZE_READONLY(CCPoint, _mapDiff, MapDiff);
    
    // scene content
    CC_SYNTHESIZE_READONLY(CCSprite*, _bg, Background);
    CC_SYNTHESIZE_READONLY(CCTMXTiledMap*, _tileMap, TileMap);
    CC_SYNTHESIZE_READONLY(PathHelper*, _pathHelper, PathHelper);
    CC_SYNTHESIZE_READONLY(TileMapStateHelper*, _stateHelper, TileMapStateHelper);
    CC_SYNTHESIZE_READONLY(EntityManager*, _entityManager, EntityManager);
    CC_SYNTHESIZE_READONLY(GameUI*, _ui, UI);
    
    // scene property
    CC_SYNTHESIZE_READONLY(CCPoint, _entry, Entry);
    CC_SYNTHESIZE_READONLY(CCPoint, _exit, Exit);
    CC_SYNTHESIZE_READONLY(CCArray*, _path, Path);
    CC_SYNTHESIZE_READONLY(CCPoint, _eggHpPoint, EggHpPoint);
    
    // state
    CC_SYNTHESIZE_READONLY(int, _gold, Gold);
    CC_SYNTHESIZE_READONLY(GameState, _state, GameState);
    GameState _preState;
    CC_SYNTHESIZE_READONLY(GameSpeed, _speed, GameSpeed);
    CC_SYNTHESIZE_READONLY(bool, _inPopup, InPopup);
};

enum GameSceneTouchPriority
{
    GameSceneTouchPriority_Base,
    GameSceneTouchPriority_GameScene,
    GameSceneTouchPriority_UI,
};

#endif /* defined(__rabbit__GameScene__) */
