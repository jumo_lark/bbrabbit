//
//  PopupLayer.h
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/11.
//
//

#ifndef __bbrabbit__PopupLayer__
#define __bbrabbit__PopupLayer__

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;
USING_NS_CC_EXT;
using namespace std;

class ButtonManager;

enum PopupId
{
    PopupId_Default = 0,
    PopupId_Win,
    PopupId_Failed,
    PopupId_Menu,
};

class PopupLayer : public CCLayer
{
public:
    
    static PopupLayer* create(CCScene* scene);
    
    virtual void onEnter();
    virtual void onExit();
    virtual bool ccTouchBegan(CCTouch* pTouch, CCEvent* pEvent);
    virtual void ccTouchMoved(CCTouch* pTouch, CCEvent* pEvent);
    virtual void ccTouchEnded(CCTouch* pTouch, CCEvent* pEvent);
    
protected:
    
    PopupLayer();
    virtual ~PopupLayer();
    virtual bool init(CCScene* scene);
    
    void createButtonManager();
    virtual void createBackground();
    
private:
    
    CC_SYNTHESIZE(PopupId, _id, Id);
    CC_SYNTHESIZE_READONLY(CCScene*, _scene, Scene);
    CC_SYNTHESIZE_READONLY(CCSize, _screenSize, ScreenSize);
    CC_SYNTHESIZE_READONLY(ButtonManager*, _buttonManager, ButtonManager);
    
    CCNode* _bg;
    CCSize _size;
    
    // observer
    virtual void addObserver();
    virtual void onButtonClick(CCObject* obj);
};

#endif /* defined(__bbrabbit__PopupLayer__) */
