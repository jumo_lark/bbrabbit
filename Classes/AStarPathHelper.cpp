//
//  AStarPathHelper.cpp
//  rabbit
//
//  Created by 李崧榕 on 15/2/5.
//
//

#include "AStarPathHelper.h"
#include "StringHelper.h"

PathHelper* PathHelper::create(cocos2d::CCTMXTiledMap *tileMap)
{
    PathHelper* helper = new PathHelper();
    if(helper && helper->init(tileMap))
    {
        helper->autorelease();
        return helper;
    }
    CC_SAFE_DELETE(helper);
    return NULL;
}

PathHelper::PathHelper()
: _mapDic(NULL)
, _tileMap(NULL)
{
    
}

PathHelper::~PathHelper()
{
    CC_SAFE_RELEASE_NULL(_mapDic);
}

bool PathHelper::init(cocos2d::CCTMXTiledMap *tileMap)
{
    if(CCNode::init())
    {
        _mapDic = CCDictionary::create();
        _mapDic->retain();
        
        this->initPathUnits(tileMap);
        
        return true;
    }
    return false;
}

void PathHelper::initPathUnits(cocos2d::CCTMXTiledMap *tileMap)
{
    _size = tileMap->getMapSize();
    
    CCTMXLayer* pathLayer = tileMap->layerNamed("Path");
    
    PathUnit* unit = NULL;
    for(int x = 0; x < _size.width; ++x)
    {
        for(int y = 0; y < _size.height; ++y)
        {
            unit = PathUnit::create();
            unit->setLocation(ccp(x, y));
            this->addPathUnit(x, y, unit);
            
            unit->setType(PathUnitType_UnPass);
            
            int gid = pathLayer->tileGIDAt(ccp(x, y));
            CCDictionary* propertyDic = tileMap->propertiesForGID(gid);
            if(propertyDic)
            {
                CCString* walkable = (CCString*)propertyDic->objectForKey("walkable");
                if(walkable->intValue() != 0) // walkable->intValue() == 1
                {
                    unit->setType(PathUnitType_Pass);
                    
                    string str_direction = ((CCString*)propertyDic->objectForKey("direction"))->getCString();
                    
                    for(int i = PathDirection_Up; i <= PathDirection_Right; ++i)
                    {
                        if(StringHelper::hasPrefix(str_direction, StringHelper::intToString(i)))
                        {
                            unit->setDirection((PathDirection)i, true);
                        }
                    }
                }
            }
        }
    }
}

void PathHelper::addPathUnit(int locX, int locY, PathUnit *unit)
{
    string str_key = this->getPathKey(locX, locY);
    _mapDic->setObject(unit, str_key);
}

PathUnit* PathHelper::getPathUnit(int locX, int locY)
{
    string str_key = this->getPathKey(locX, locY);
    return (PathUnit*)_mapDic->objectForKey(str_key);
}

string PathHelper::getPathKey(int locX, int locY)
{
    return StringHelper::intToString(locX) + "_" + StringHelper::intToString(locY);
}

void PathHelper::getAStarSearchPath(cocos2d::CCArray *path, cocos2d::CCPoint start, cocos2d::CCPoint end)
{
    CCArray* units_open = CCArray::create();
    CCArray* units_close = CCArray::create();
    
    PathUnit* unit = this->getPathUnit(start.x, start.y);
    unit->setFrontUnit(NULL);
    unit->setG(0);
    unit->setH(end);
    units_open->addObject(unit);
    
    PathUnit* endUnit = this->getPathUnit(end.x, end.y);
    
    bool findPath = false;
    
    while(units_open->count() > 0)
    {
        unit = (PathUnit*)units_open->objectAtIndex(0);
        units_close->addObject(unit);
        units_open->removeObject(unit);
        
        CCArray* aroundsUnits = CCArray::create();
        this->getAroundsUnit(aroundsUnits, unit);
        
        for(int i = 0; i < aroundsUnits->count(); ++i)
        {
            PathUnit* unit_i = (PathUnit*)aroundsUnits->objectAtIndex(i);
            
            if(units_close->indexOfObject(unit_i) != CC_INVALID_INDEX)
            {
                // continue
            }
            else if(units_open->indexOfObject(unit_i) != CC_INVALID_INDEX)
            {
                int i_g = unit->getG() + 1;
                int i_h = unit_i->getH();
                int i_f = i_g + i_h;
                
                if(i_f < unit_i->getF())
                {
                    unit_i->setFrontUnit(unit);
                    unit_i->setG(unit->getG() + 1);
                    units_open->removeObject(unit_i);
                    this->insertOrderUnit(units_open, unit_i);
                }
            }
            else
            {
                unit_i->setFrontUnit(unit);
                unit_i->setG(unit->getG() + 1);
                unit_i->setH(end);
                this->insertOrderUnit(units_open, unit_i);
                
//                CCLOG("unit_i->Loaction = %f, %f", unit_i->getLocation().x, unit_i->getLocation().y);
            }
        }
        
        if(units_open->indexOfObject(endUnit) != CC_INVALID_INDEX)
        {
//            CCLOG("endunit f = %d, g = %d, h = %d, location = (%f, %f)", endUnit->getF(), endUnit->getG(), endUnit->getH(), endUnit->getLocation().x, endUnit->getLocation().y);
//            
//            CCLOG("func:PathHelper::AStarSearch log : \"find path\"");
            findPath = true;
            break;
        }
    }
    
    if(!findPath) // 找到 close中距离end最近的一点作为end
    {
        PathUnit* nearUnit = (PathUnit*)units_close->objectAtIndex(0);
        
        for(int i = 0; i < units_close->count(); ++i)
        {
            PathUnit* unit_i = (PathUnit*)units_close->objectAtIndex(i);
            
            if(unit_i->getH() < nearUnit->getH())
            {
                nearUnit = unit_i;
            }
        }
        endUnit = nearUnit;
    }
    
    // 导出路径
    path->removeAllObjects();
    unit = endUnit;
    
    while(unit->getFrontUnit())
    {
//        path->addObject(unit);
        path->insertObject(unit, 0);
        unit = unit->getFrontUnit();
    }
}

void PathHelper::getAroundsUnit(cocos2d::CCArray *aroundUnits, PathUnit *unit)
{
    CCPoint loc = unit->getLocation();
    PathUnit* aroundUnit = NULL;
    
    if(loc.x > 0)
    {
        aroundUnit = this->getPathUnit(loc.x - 1, loc.y);
        if(aroundUnit->getType() == PathUnitType_Pass && unit->getDirection()[PathDirection_Left])
            aroundUnits->addObject(aroundUnit);
    }
    
    if(loc.x < _size.width - 1)
    {
        aroundUnit = this->getPathUnit(loc.x + 1, loc.y);
        if(aroundUnit->getType() == PathUnitType_Pass && unit->getDirection()[PathDirection_Right])
            aroundUnits->addObject(aroundUnit);
    }
    
    if(loc.y > 0)
    {
        aroundUnit = this->getPathUnit(loc.x, loc.y - 1);
        if(aroundUnit->getType() == PathUnitType_Pass && unit->getDirection()[PathDirection_Up])
            aroundUnits->addObject(aroundUnit);
    }
    
    if(loc.y < _size.height - 1)
    {
        aroundUnit = this->getPathUnit(loc.x, loc.y + 1);
        if(aroundUnit->getType() == PathUnitType_Pass && unit->getDirection()[PathDirection_Down])
            aroundUnits->addObject(aroundUnit);
    }
}

void PathHelper::insertOrderUnit(cocos2d::CCArray *units, PathUnit *unit)
{
    for(int i = 0; i < units->count(); ++i)
    {
        PathUnit* unit_i = (PathUnit*)units->objectAtIndex(i);
        if(unit->getF() < unit_i->getF())
        {
            units->insertObject(unit, units->indexOfObject(unit_i));
            return;
        }
    }
    
    units->addObject(unit);
}

CCArray* PathHelper::getPath(cocos2d::CCPoint point_start, cocos2d::CCPoint point_end)
{
    CCArray* path = CCArray::create();
    this->getAStarSearchPath(path, point_start, point_end);
    return path;
}