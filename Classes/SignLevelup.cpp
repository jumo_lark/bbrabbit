//
//  SignLevelup.cpp
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/12.
//
//

#include "SignLevelup.h"
#include "Tower.h"
#include "NotifyManager.h"

SignLevelup* SignLevelup::create(Tower* tower)
{
    SignLevelup* sign = new SignLevelup();
    if(sign && sign->init(tower))
    {
        sign->autorelease();
        return sign;
    }
    CC_SAFE_DELETE(sign);
    return NULL;
}

SignLevelup::SignLevelup()
{
    
}

SignLevelup::~SignLevelup()
{
    
}

bool SignLevelup::init(Tower* tower)
{
    if(SignNode::init())
    {
        _tower = tower;
        
        _sign = CCSprite::createWithSpriteFrameName("gameui_img_sign_level");
        this->addChild(_sign);
        
        this->updateState();
        
        return true;
    }
    return false;
}

void SignLevelup::addObserver()
{
    Notify->addObserver(this, callfuncO_selector(SignLevelup::updateState), NotifyMsg_update_gold, NULL);
}

void SignLevelup::updateState()
{
    this->setVisible(_tower->enableLevelup());
}