//
//  Bullet.h
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/10.
//
//

#ifndef __bbrabbit__Bullet__
#define __bbrabbit__Bullet__

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;
USING_NS_CC_EXT;

#include "MoveComponent.h"
class EntityManager;
class DestroyableEntity;

class Bullet : public CCNode, public MoveDelegate
{
public:
    
    static Bullet* create(EntityManager* entityManager, DestroyableEntity* target, float attackPower);
    
    void startShoot();
    
    virtual void onEnter();
    virtual void onExit();
    
    virtual void onArrived();
    
protected:
    
    Bullet();
    virtual ~Bullet();
    virtual bool init(EntityManager* entityManager, DestroyableEntity* target, float attackPower);
    
    void initProperty();
    void addMoveComponent();
    
    virtual void update(float dt);
    void updateTargetPosition();
    
    void addObserver();
    
    void onPause();
    void onResume();
    void onTargetKilled(CCObject* obj);
    
private:
    
    CC_SYNTHESIZE_READONLY(EntityManager*, _entityManager, EntityManager);
    CC_SYNTHESIZE_READONLY(DestroyableEntity*, _target, Target);
    CC_SYNTHESIZE_READONLY(float, _attackPower, AttackPower);
    CC_SYNTHESIZE_READONLY(float, _speed, Speed);
    
    CC_SYNTHESIZE_READONLY(CCPoint, _targetPosition, TargetPosition);
    
    CC_SYNTHESIZE_READONLY(MoveComponent*, _moveComponent, MoveComponent);
    
    CC_SYNTHESIZE_READONLY(bool, _inPause, InPause);
};

#endif /* defined(__bbrabbit__Bullet__) */
