//
//  TileMapStateHelper.cpp
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/9.
//
//

#include "TileMapStateHelper.h"

TileMapStateHelper* TileMapStateHelper::create(cocos2d::CCTMXTiledMap *tileMap)
{
    TileMapStateHelper* helper = new TileMapStateHelper();
    if(helper && helper->init(tileMap))
    {
        helper->autorelease();
        return helper;
    }
    CC_SAFE_DELETE(helper);
    return NULL;
}

TileMapStateHelper::TileMapStateHelper()
{
    
}

TileMapStateHelper::~TileMapStateHelper()
{
    
}

bool TileMapStateHelper::init(cocos2d::CCTMXTiledMap *tileMap)
{
    if(CCNode::init())
    {
        _tileMap = tileMap;
        _size = _tileMap->getMapSize();
        
        this->initMapState();
        
        return true;
    }
    return false;
}

void TileMapStateHelper::initMapState()
{
    CCTMXLayer* pathLayer = _tileMap->layerNamed("Path");
    
    for(int x = 0; x < _size.width; ++x)
    {
        vector<TileMapState> vec_states;
        for(int y = 0; y < _size.height; ++y)
        {
            vec_states.push_back(TileMapState_Empty);
            
            int gid = pathLayer->tileGIDAt(ccp(x, y));
            CCDictionary* propertyDic = _tileMap->propertiesForGID(gid);
            if(propertyDic)
            {
                CCString* walkable = (CCString*)propertyDic->objectForKey("walkable");
                if(walkable->intValue() != 0)
                    vec_states[y - 1] = TileMapState_Path;
            }
        }
        _map_state.push_back(vec_states);
    }
}

TileMapState TileMapStateHelper::getTileMapState(cocos2d::CCPoint tilePoint)
{
    if(isInMap(tilePoint))
        return _map_state[tilePoint.x][tilePoint.y - 1];
    else
        return TileMapState_NotInMap;
}

void TileMapStateHelper::setTileMapState(cocos2d::CCPoint tilePoint, TileMapState state)
{
    if(isInMap(tilePoint))
        _map_state[tilePoint.x][tilePoint.y - 1] = state;
}

bool TileMapStateHelper::isInMap(cocos2d::CCPoint tilePoint)
{
    return (tilePoint.x >= 0 &&
            tilePoint.x < _size.width &&
            tilePoint.y >= 1 &&
            tilePoint.y < _size.height + 1);
}