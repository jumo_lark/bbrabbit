//
//  Util.cpp
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/9.
//
//

#include "Util.h"
#include "support/zip_support/ZipUtils.h"

ScreenProfile Util::_screenProfile = ScreenProfile_1280_720;

void Util::registScreenProfile(cocos2d::CCSize screenSize)
{
    float k_1280_720 = 1280.0f / 720.f;  // 1.78
    float k_800_480 = 800.0f / 480.0f;   // 1.66666
    float k_960_640 = 960.0f / 640.0f;   // 1.5
    float k_1024_768 = 1024.0f / 768.0f; // 1.3333
    
    float k_screen = screenSize.width / screenSize.height;
    
    if(k_screen >= (k_1280_720 + k_800_480) * 0.5)
    {
        _screenProfile = ScreenProfile_1280_720;
    }
    else if(k_screen >= (k_800_480 + k_960_640) * 0.5)
    {
        _screenProfile = ScreenProfile_800_480;
    }
    else if(k_screen >= (k_960_640 + k_1024_768) * 0.5)
    {
        _screenProfile = ScreenProfile_960_640;
    }
    else
    {
        _screenProfile = ScreenProfile_1024_768;
    }
}

ScreenProfile Util::getScreenProfile()
{
    return _screenProfile;
}

void Util::setPvrEncryptKeys()
{
    ZipUtils::ccSetPvrEncryptionKeyPart(0, 0x3d683683);
    ZipUtils::ccSetPvrEncryptionKeyPart(1, 0x54dee260);
    ZipUtils::ccSetPvrEncryptionKeyPart(2, 0x55ee4cf1);
    ZipUtils::ccSetPvrEncryptionKeyPart(3, 0xd2411205);
}

CCArray* Util::getRandomObjectArray(cocos2d::CCArray *objects, int randomCount)
{
    if(randomCount >= objects->count())
    {
        return getCopyArray(objects);
    }
    
    CCArray* randomArray = getCopyArray(objects);
    int index;
    
    for(int i = 0; i < randomCount; ++i)
    {
        index = randomInRange(0, randomArray->count() - 1);
        randomArray->removeObjectAtIndex(index);
    }
    
    return randomArray;
}

int Util::randomInRange(int min, int max)
{
    int range = max - min;
    return (int)(CCRANDOM_0_1() * (range + 0.999)) + min;
}

CCArray* Util::getCopyArray(cocos2d::CCArray *objects)
{
    CCArray* sameArray = CCArray::create();
    CCObject* obj;
    
    for(int i = 0; i < objects->count(); ++i)
    {
        obj = objects->objectAtIndex(i);
        sameArray->addObject(obj);
    }
    
    return sameArray;
}

bool Util::isInHitRect(const cocos2d::CCPoint &point1, float radius1, const cocos2d::CCPoint &point2, float radius2)
{
    return ccpDistance(point1, point2) <= (radius1 + radius2);
}

bool Util::isInHitRect(cocos2d::CCRect hitRect1, cocos2d::CCRect hitRect2)
{
    CCPoint pLeftDown_1 = ccp(hitRect1.getMinX(), hitRect1.getMinY());
    CCPoint pLeftUp_1 = ccp(hitRect1.getMinX(), hitRect1.getMaxY());
    CCPoint pRightDown_1 = ccp(hitRect1.getMaxX(), hitRect1.getMinY());
    CCPoint pRightUp_1 = ccp(hitRect1.getMaxX(), hitRect1.getMaxY());
    
    CCPoint pLeftDown_2 = ccp(hitRect2.getMinX(), hitRect2.getMinY());
    CCPoint pLeftUp_2 = ccp(hitRect2.getMinX(), hitRect2.getMaxY());
    CCPoint pRightDown_2 = ccp(hitRect2.getMaxX(), hitRect2.getMinY());
    CCPoint pRightUp_2 = ccp(hitRect2.getMaxX(), hitRect2.getMaxY());
    
    return (hitRect2.containsPoint(pLeftDown_1) || hitRect2.containsPoint(pLeftUp_1) || hitRect2.containsPoint(pRightDown_1) || hitRect2.containsPoint(pRightUp_1) || hitRect1.containsPoint(pLeftDown_2) || hitRect1.containsPoint(pLeftUp_2) || hitRect1.containsPoint(pRightDown_2) || hitRect1.containsPoint(pRightUp_2));
}

bool Util::isInHitRect(const cocos2d::CCPoint &point1, float radius1, cocos2d::CCRect hitRect)
{
    float x = point1.x;
    float y = point1.y;
    float minX = hitRect.getMinX();
    float minY = hitRect.getMinY();
    float maxX = hitRect.getMaxX();
    float maxY = hitRect.getMaxY();
    
    if(x < minX)
    {
        if(y <= maxY && y >= minY)
        {
            return (minX - x <= radius1);
        }
        else if(y > maxY)
        {
            CCPoint leftUp = ccp(hitRect.getMinX(), hitRect.getMaxY());
            
            return (ccpDistance(leftUp, point1) <= radius1);
        }
        else// if(y < minY)
        {
            CCPoint leftDown = ccp(hitRect.getMinX(), hitRect.getMinY());
            
            return (ccpDistance(leftDown, point1) <= radius1);
        }
    }
    else if(x >= minX && x <= maxX)
    {
        return (y < maxY + radius1 && y > minY - radius1);
    }
    else
    {
        if(y <= maxY && y >= minY)
        {
            return (x - maxX <= radius1);
        }
        else if(y > maxY)
        {
            CCPoint rightUp = ccp(hitRect.getMaxX(), hitRect.getMaxY());
            
            return (ccpDistance(rightUp, point1) <= radius1);
        }
        else// if(y < minY)
        {
            CCPoint rightDown = ccp(hitRect.getMaxX(), hitRect.getMinY());
            
            return (ccpDistance(rightDown, point1) <= radius1);
        }
    }
}

float Util::getTargetRotation(cocos2d::CCPoint targetPos, cocos2d::CCPoint centerPos)
{
    float targetRotation;
    
    float angle = ccpAngle(ccpSub(targetPos, centerPos), CCPointZero);
    float degrees = CC_RADIANS_TO_DEGREES(angle);
    
    if(targetPos.y >= centerPos.y)
    {
        if(targetPos.x - centerPos.x < 0)
        {
            targetRotation = 90 - degrees + 360;
        }
        else
        {
            targetRotation = 90 - degrees;
        }
    }
    else
    {
        targetRotation = 90 + degrees;
    }
    
    return targetRotation;
}

CCActionInterval* Util::getPopupAction()
{
    return getPopupAction(1);
}

CCActionInterval* Util::getPopupAction(float scaleFactor)
{
    float timeFactor = CCDirector::sharedDirector()->getScheduler()->getTimeScale();
    
//    CCActionInterval* action_1 = CCSpawn::create(CCHide::create(), CCScaleTo::create(0.05, 0), NULL);
    CCActionInstant* action_2 = CCShow::create();
    CCActionInterval* action_3 = CCEaseBackOut::create(CCScaleTo::create(0.15 * timeFactor, 1.05 * scaleFactor));
    CCActionInterval* action_4 = CCScaleTo::create(0.045 * timeFactor, 0.9 * scaleFactor);
    CCActionInterval* action_5 = CCScaleTo::create(0.025 * timeFactor, 1.0 * scaleFactor);
    CCSequence* actions = CCSequence::create(action_2, action_3, action_4, action_5, NULL);
    
    return actions;
}
