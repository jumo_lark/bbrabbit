//
//  PopupFailed.cpp
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/11.
//
//

#include "PopupFailed.h"
#include "Button.h"
#include "FormSprite.h"

PopupFailed* PopupFailed::create(cocos2d::CCScene *scene)
{
    PopupFailed* popup = new PopupFailed();
    if(popup && popup->init(scene))
    {
        popup->autorelease();
        return popup;
    }
    CC_SAFE_DELETE(popup);
    return NULL;
}

PopupFailed::PopupFailed()
{
    
}

PopupFailed::~PopupFailed()
{
    
}

bool PopupFailed::init(cocos2d::CCScene *scene)
{
    if(PopupLayer::init(scene))
    {
        _id = PopupId_Failed;
        
        this->createTitleBg();
        this->createImg();
        this->createButtonReturnMap();
        this->createButtonReplay();
        
        return true;
    }
    return false;
}

void PopupFailed::createBackground()
{
    _size = ccp(381, 356);
    
    _bg = FormSprite::createWithCorner(CCSprite::createWithSpriteFrameName("img_popup_bg_corner"), CCSprite::createWithSpriteFrameName("img_popup_bg_content"), _size.width, _size.height);
    _bg->setPosition(ccp(_screenSize.width * 0.5, _screenSize.height * 0.45));
    this->addChild(_bg);
}

void PopupFailed::createTitleBg()
{
    CCSprite* sprite_title_left = CCSprite::createWithSpriteFrameName("img_popup_title_blue");
    sprite_title_left->setAnchorPoint(ccp(0, 1));
    sprite_title_left->setPosition(ccp(-_size.width * 0.5, _size.height * 0.625));
    _bg->addChild(sprite_title_left);
    
    CCSprite* sprite_title_right = CCSprite::createWithSpriteFrameName("img_popup_title_blue");
    sprite_title_right->setFlipX(true);
    sprite_title_right->setAnchorPoint(ccp(1, 1));
    sprite_title_right->setPosition(ccp(_size.width * 0.5, _size.height * 0.625));
    _bg->addChild(sprite_title_right);
    
    CCSprite* sprite_light = CCSprite::createWithSpriteFrameName("img_popup_light_point");
    sprite_light->setPosition(ccp(sprite_title_left->getContentSize().width * 0.105, sprite_title_left->getContentSize().height * 0.82));
    sprite_title_left->addChild(sprite_light);
    
    CCLabelBMFont* font_title = CCLabelBMFont::create("过关失败", "Fonts/font_popup.fnt");
//    font_title->setScale(1.45);
    font_title->setScale(1.2);
    font_title->setPosition(ccp(0, _size.height * 0.54));
    font_title->setColor(ccYELLOW);
    _bg->addChild(font_title);
}

void PopupFailed::createImg()
{
    CCSprite* img = CCSprite::createWithSpriteFrameName("popup_failed_img");
    img->setPosition(ccp(0, _size.height * 0.08));
    _bg->addChild(img);
}

void PopupFailed::createButtonReturnMap()
{
    CCSprite* content = CCSprite::createWithSpriteFrameName("button_close");
    Button* button = Button::create(ButtonId_Game_ReturnMap, content);
    button->setPosition(ccp(_size.width * 0.405, _size.height * 0.54));
    _bg->addChild(button);
    _buttonManager->addButton(button);
}

void PopupFailed::createButtonReplay()
{
    CCSprite* content = CCSprite::createWithSpriteFrameName("button_orange_long");
    CCLabelBMFont* font_info = CCLabelBMFont::create("再试一次", "Fonts/font_popup.fnt");
//    font_info->setScale(1.25);
    font_info->setPosition(ccp(0, content->boundingBox().size.height * 0.05));
    Button* button = Button::create(ButtonId_Game_Replay, content, NULL, font_info);
    button->setPosition(ccp(0, -_size.height * 0.365));
    _bg->addChild(button);
    _buttonManager->addButton(button);
}

void PopupFailed::onButtonClick(cocos2d::CCObject *obj)
{
    Button* button = (Button*)obj;
    ButtonId bid = button->getId();
    
    if(bid == ButtonId_Game_ReturnMap)
    {
        // Nothing Todo
    }
    else if(bid == ButtonId_Game_Replay)
    {
        // Nothing Todo
    }
}