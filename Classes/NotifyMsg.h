//
//  NotifyMsg.h
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/9.
//
//

#ifndef __bbrabbit__NotifyMsg__
#define __bbrabbit__NotifyMsg__

#include <stdio.h>

enum NotifyMsg
{
    // button
    NotifyMsg_button_touch,
    NotifyMsg_button_click,
    
    // killed
    NotifyMsg_killed_destroyable_entity,
    NotifyMsg_killed_enemy,
    NotifyMsg_killed_egg,
    
    // game
    NotifyMsg_game_pause,
    NotifyMsg_game_resume,
    NotifyMsg_game_over,
    NotifyMsg_game_win,
    
    // update
    NotifyMsg_update_wave,
    NotifyMsg_update_gold,
    
    // poup
    NotifyMsg_popup_open,
    NotifyMsg_popup_close,
    
    
    // background 后台
    NotifyMsg_enter_background,
    NotifyMsg_enter_foreground,
};

#endif /* defined(__bbrabbit__NotifyMsg__) */
