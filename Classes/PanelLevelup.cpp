//
//  PanelLevelup.cpp
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/10.
//
//

#include "PanelLevelup.h"
#include "GameScene.h"
#include "Button.h"
#include "NotifyManager.h"
#include "TowerData.h"
#include "Tower.h"
#include "StaticDataManager.h"
#include "StringHelper.h"

enum KLevelupTag
{
    KLevelupTag_Base,
    KLevelupTag_Price,
};

const ccColor3B KColor_Enable = ccc3(255, 255, 255);
const ccColor3B KColor_Unable = ccc3(100, 100, 100);

PanelLevelup* PanelLevelup::create(GameScene *game)
{
    PanelLevelup* panel = new PanelLevelup();
    if(panel && panel->init(game))
    {
        panel->autorelease();
        return panel;
    }
    CC_SAFE_DELETE(panel);
    return NULL;
}

PanelLevelup::PanelLevelup()
: _tower(NULL)
, _buttonManager(NULL)
, _button_destroy(NULL)
, _button_levelup(NULL)
{
    
}

PanelLevelup::~PanelLevelup()
{
    
}

bool PanelLevelup::init(GameScene *game)
{
    if(CCNode::init())
    {
        _game = game;
        _tileSize = _game->getTileSize();
        
        this->createButtonManager();
        this->createBackground();
        this->createButtonLevelup();
        this->createButtonDestroy();
        
        this->hide();
        
        return true;
    }
    return false;
}

void PanelLevelup::createButtonManager()
{
    _buttonManager = ButtonManager::create();
    this->addChild(_buttonManager);
}

void PanelLevelup::createBackground()
{
    _bg = CCSprite::createWithSpriteFrameName("img_panel_levelup_bg");
//    _bg->setPosition(ccp(0, sprite_center->getContentSize().height * 0.75 + _bg->getContentSize().height * 0.5));
    this->addChild(_bg);
    _size = _bg->getContentSize();
}

void PanelLevelup::createButtonLevelup()
{
    CCSprite* content = CCSprite::createWithSpriteFrameName("button_levelup");
    
    _font_price_levelup = CCLabelBMFont::create("0", "Fonts/font_num.fnt");
    _font_price_levelup->setScale(0.45);
    _font_price_levelup->setPosition(ccp(content->getContentSize().width * 0.58, content->getContentSize().height * 0.225));
    content->addChild(_font_price_levelup);
    
    _button_levelup = Button::create(ButtonId_GameUI_Levelup, content);
    _button_levelup->setPosition(ccp(0, _tileSize.height));
    this->addChild(_button_levelup);
    _buttonManager->addButton(_button_levelup);
}

void PanelLevelup::createButtonDestroy()
{
    CCSprite* content = CCSprite::createWithSpriteFrameName("button_destroy");
    
    _font_price_destroy = CCLabelBMFont::create("0", "Fonts/font_num.fnt");
    _font_price_destroy->setScale(0.45);
    _font_price_destroy->setPosition(ccp(content->getContentSize().width * 0.6, content->getContentSize().height * 0.22));
    content->addChild(_font_price_destroy);
    
    _button_destroy = Button::create(ButtonId_GameUI_Destroy, content);
    _button_destroy->setPosition(ccp(0, -_tileSize.height));
    this->addChild(_button_destroy);
    _buttonManager->addButton(_button_destroy);
}

void PanelLevelup::onEnter()
{
    CCNode::onEnter();
    
    this->addObserver();
}

void PanelLevelup::onExit()
{
    CCNode::onExit();
    
    Notify->removeAllObservers(this);
}

void PanelLevelup::addObserver()
{
    Notify->addObserver(this, callfuncO_selector(PanelLevelup::onButtonTouch), NotifyMsg_button_touch, NULL);
    Notify->addObserver(this, callfuncO_selector(PanelLevelup::onUpdateGold), NotifyMsg_update_gold, NULL);
}

void PanelLevelup::onButtonTouch(cocos2d::CCObject *obj)
{
    Button* button = (Button*)obj;
    ButtonId bid = button->getId();
    if(bid == ButtonId_GameUI_Levelup)
    {
        button->resume();
        
        if(_tower->enableLevelup())
        {
            int cost = _tower->getLevelupCost();
            _tower->levelup();
            _game->costGold(cost);
            this->hide();
        }
        else
        {
            
        }
    }
    else if(bid == ButtonId_GameUI_Destroy)
    {
        button->resume();
        
        _game->removeTower(_tower);
        this->hide();
    }
}

void PanelLevelup::onUpdateGold(cocos2d::CCObject *obj)
{
    if(_tower)
        this->refresh(_tower);
}

bool PanelLevelup::touchBegan(cocos2d::CCTouch *pTouch)
{
    bool isTrigger = false;
    
    if(_inShow)
    {
        isTrigger = true;
        
        if(_buttonManager->addEventTouchBegan(pTouch))
        {
            
        }
        else
        {
            this->hide();
        }
    }
    
    return isTrigger;
}

void PanelLevelup::show(Tower* tower)
{
    float length_picture = 240 * 0.5;
    float factor_scale = tower->getAttackRange() / length_picture;
    _bg->setScale(factor_scale);
    
    _tower = tower;
    this->setVisible(true);
    this->setPosition(_game->convertToWorldPositionInTileCenter(_tower->getTilePoint()));
    _inShow = true;
    
    this->refresh(_tower);
}

void PanelLevelup::hide()
{
    _tower = NULL;
    _inShow = false;
    this->setVisible(false);
}

void PanelLevelup::refresh(Tower *tower)
{
    if(tower->getLevel() == 3)
    {
        _font_price_levelup->setString("Max");
    }
    else
    {
        int price = StaticData->getTowerData(tower->getData()->getId(), tower->getLevel() + 1)->getCost();
        _font_price_levelup->setString(StringHelper::intToString(price).c_str());
    }
    
    _font_price_destroy->setString(StringHelper::intToString(_tower->getDestroyPrice()).c_str());
    
    if(tower->enableLevelup())
    {
        _button_levelup->getSpriteNormal()->setColor(KColor_Enable);
    }
    else
    {
        _button_levelup->getSpriteNormal()->setColor(KColor_Unable);
    }
    
    if(tower->getTilePoint().y == _game->getTileMapSize().height - 1)
    {
        _button_destroy->setPosition(ccp(_tileSize.width, 0));
    }
    else
    {
        _button_destroy->setPosition(ccp(0, -_tileSize.height));
    }
}