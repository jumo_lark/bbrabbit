//
//  FormSprite.h
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/11.
//
//

#ifndef __bbrabbit__FormSprite__
#define __bbrabbit__FormSprite__

#include "cocos2d.h"
USING_NS_CC;

/*
 * class CornerState
 */

class CornerState : public CCObject
{
public:
    
    CREATE_FUNC(CornerState);
    
    static CornerState* createWithData(bool lt, bool rt, bool lb, bool rb);
    
    CornerState();
    
    virtual ~CornerState();
    
    bool init();
    
    bool initWithData(bool lt, bool rt, bool lb, bool rb);
    
    CC_SYNTHESIZE_PASS_BY_REF(bool, _leftTop, LeftTop);
    
    CC_SYNTHESIZE_PASS_BY_REF(bool, _rightTop, RightTop);
    
    CC_SYNTHESIZE_PASS_BY_REF(bool, _leftBottom, LeftBottom);
    
    CC_SYNTHESIZE_PASS_BY_REF(bool, _rightBottom, RightBottom);
    
private:
    
protected:
    
    
};

/*
 * class FormSprite
 */

enum FormSpriteLineType
{
    FormSpriteLineType_Horizon,
    FormSpriteLineType_Vertical,
};

class FormSprite : public CCNode
{
public:
    
    static FormSprite* createWithCorner(CCSprite* corner, CCSprite* content, float width, float height);
    
    static FormSprite* createWithCorner(CCSprite* corner, CCSprite* content, float width, float height, CornerState* state);
    
    static FormSprite* createWithLine(CCSprite* line, float length, FormSpriteLineType type);
    
    static FormSprite* createWithLine(CCSprite* border, CCSprite* line, float length, FormSpriteLineType type);
    
    FormSprite();
    
    virtual ~FormSprite();
    
    virtual bool initWithConrner(CCSprite* corner, CCSprite* content, float width, float height, CornerState* state);
    
    virtual bool initWithLine(CCSprite* line, float length, FormSpriteLineType type);
    
    virtual bool initWithLine(CCSprite* border, CCSprite* line, float length, FormSpriteLineType type);
    
private:
    
    void initSprite();
    
    void initLineSprite(float length);
    
    void initLineSpriteWithBorder();
    
protected:
    
    CCSprite* _corner;
    
    CCSprite* _content;
    
    CornerState* _cornerState;
    
    CCSprite* _line;
    
    FormSpriteLineType _lineType;
    
    CCSprite* _lineBorder;
    
    float _width;
    
    float _height;
    
    float _length;
    
    CCSpriteBatchNode* _sprite;
    
};

#endif /* defined(__bbrabbit__FormSprite__) */
