//
//  FileHelper.cpp
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/10.
//
//

#include "FileHelper.h"

Document FileHelper::getJsonFile(const string &fileName)
{
    unsigned long size;
    unsigned char* ch = CCFileUtils::sharedFileUtils()->getFileData(fileName.c_str(), "r", &size);
    string str_data = string((const char*)ch, size);
    
    Document doc;
    doc.Parse<0>(str_data.c_str());
    if(doc.HasParseError())
    {
        CCLOG("GetParseError: %s", doc.GetParseError());
    }
    
    return doc;
}