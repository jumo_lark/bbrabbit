//
//  ResourceHelper.cpp
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/11.
//
//

#include "ResourceHelper.h"
#include "StringHelper.h"

#define armatureManager   CCArmatureDataManager::sharedArmatureDataManager()
#define frameCache        CCSpriteFrameCache::sharedSpriteFrameCache()
#define textureCache      CCTextureCache::sharedTextureCache()

/*
 * Common
 */
void ResourceHelper::addCommonResource()
{
    frameCache->addSpriteFramesWithFile("Common/package_common.plist");
    frameCache->addSpriteFramesWithFile("Button/package_button.plist");
}

/*
 * Tower
 */
void ResourceHelper::addTowerResource(TowerId tid)
{
    string name_plist = "Tower/package_tower_" + TowerData::getTowerName(tid) + ".plist";
    frameCache->addSpriteFramesWithFile(name_plist.c_str());
    
    ResourceHelper::addBinaryArmatureInfos(ResourceHelper::getTowerArmatureNames(tid), false);
}

void ResourceHelper::removeTowerResource(TowerId tid)
{
    string name_plist = "Tower/package_tower_" + TowerData::getTowerName(tid) + ".plist";
    frameCache->removeSpriteFramesFromFile(name_plist.c_str());
    string name_texture = "Tower/package_tower_" + TowerData::getTowerName(tid) + ".pvr.ccz";
    textureCache->removeTextureForKey(name_texture.c_str());
    
    ResourceHelper::removeBinaryArmatureInfos(ResourceHelper::getTowerArmatureNames(tid));
}

vector<string> ResourceHelper::getTowerArmatureNames(TowerId tid)
{
    vector<string> names;
    
    if(tid == TowerId_None)
    {
        names.push_back("Tower/tower_start_light");
    }
    else if(tid == TowerId_Rabbit)
    {
        names.push_back("Tower/rabbit_bullet_1");
        names.push_back("Tower/rabbit_bullet_2");
        names.push_back("Tower/rabbit_bullet_3");
        names.push_back("Tower/rabbit_1");
        names.push_back("Tower/rabbit_2");
        names.push_back("Tower/rabbit_3");
    }
    
    return names;
}

/*
 * Popup
 */
void ResourceHelper::addPopupResource()
{
    frameCache->addSpriteFramesWithFile("Popup/package_popup.plist");
}

void ResourceHelper::removePopupResource()
{
    frameCache->removeSpriteFramesFromFile("Popup/package_popup.plist");
    textureCache->removeTextureForKey("Popup/package_popup.pvr.ccz");
    
    // Font
    textureCache->removeTextureForKey("Fonts/font_popup.png");
}

/*
 * GameScene
 */
void ResourceHelper::addGameSceneResource()
{
    frameCache->addSpriteFramesWithFile("GameUI/package_gameui.plist");
    frameCache->addSpriteFramesWithFile("PhoenixEgg/package_phoenix_egg.plist");
    frameCache->addSpriteFramesWithFile("Maps/Map_1/package_item_1.plist");
    frameCache->addSpriteFramesWithFile("Effect/package_effects.plist");
    
    vector<string> armatureNames = getGameSceneArmatureNames();
    for(int i = 0; i < armatureNames.size(); ++i)
    {
        armatureManager->addArmatureFileInfo((armatureNames[i] + ".ExportJson").c_str());
    }
    
    armatureManager->addArmatureFileInfo("Enemy/boss_1.csb");
    ResourceHelper::addBinaryArmatureInfo("Effect/death.csb", false);
    
    ResourceHelper::addTowerResource(TowerId_None);
    ResourceHelper::addTowerResource(TowerId_Rabbit);
}

void ResourceHelper::removeGameSceneResource()
{
    frameCache->removeSpriteFramesFromFile("GameUI/package_gameui.plist");
    textureCache->removeTextureForKey("GameUI/package_gameui.pvr.ccz");
    
    frameCache->removeSpriteFramesFromFile("PhoenixEgg/package_phoenix_egg.plist");
    textureCache->removeTextureForKey("PhoenixEgg/package_phoenix_egg.pvr.ccz");
    
    frameCache->removeSpriteFramesFromFile("Effect/package_effects.plist");
    textureCache->removeTextureForKey("Effect/package_effects.pvr.ccz");
    
    frameCache->removeSpriteFramesFromFile("Maps/Map_1/package_item_1.plist");
    textureCache->removeTextureForKey("Maps/Map_1/package_item_1.pvr.ccz");
    
    vector<string> armatureNames = getGameSceneArmatureNames();
    for(int i = 0; i < armatureNames.size(); ++i)
    {
        armatureManager->removeArmatureFileInfo((armatureNames[i] + ".ExportJson").c_str());
    }
    
    armatureManager->removeArmatureFileInfo("Enemy/boss_1.csb");
    ResourceHelper::removeBinaryArmatureInfo("Effect/death.csb");
    
    ResourceHelper::removeTowerResource(TowerId_None);
    ResourceHelper::removeTowerResource(TowerId_Rabbit);
}

vector<string> ResourceHelper::getGameSceneArmatureNames()
{
    vector<string> armatureNames;
    
    armatureNames.push_back("Effect/cat_show_effect");
    armatureNames.push_back("Effect/goldeffect");
    armatureNames.push_back("Effect/shield_hit_effect");
    armatureNames.push_back("Effect/upgrade_heroInfo_showHero_rotate");
    
    return armatureNames;
}

// public
void ResourceHelper::addBinaryArmatureInfos(vector<string> names, bool autoLoadResource)
{
    for(int i = 0; i < names.size(); ++i)
    {
        ResourceHelper::addBinaryArmatureInfo(names[i] + ".csb", autoLoadResource);
    }
}

void ResourceHelper::removeBinaryArmatureInfos(vector<string> names)
{
    for(int i = 0; i < names.size(); ++i)
    {
        ResourceHelper::removeBinaryArmatureInfo(names[i] + ".csb");
    }
}

void ResourceHelper::addBinaryArmatureInfo(string name, bool autoLoadResource)
{
    armatureManager->addArmatureFileInfo(name.c_str(), autoLoadResource);
}

void ResourceHelper::removeBinaryArmatureInfo(string name)
{
    armatureManager->removeArmatureFileInfo(name.c_str());
}