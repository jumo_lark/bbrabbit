//
//  PathMoveComponent.cpp
//  rabbit
//
//  Created by 李崧榕 on 15/2/6.
//
//

#include "PathMoveComponent.h"
#include "GameScene.h"
#include "AStarPathHelper.h"

PathMoveComponent* PathMoveComponent::create(GameScene* game, cocos2d::CCNode *target)
{
    PathMoveComponent* component = new PathMoveComponent();
    if(component && component->init(game, target))
    {
        component->autorelease();
        return component;
    }
    CC_SAFE_DELETE(component);
    return NULL;
}

PathMoveComponent::PathMoveComponent()
: _game(NULL)
, _target(NULL)
, _delegate(NULL)
, _moveComponent(NULL)
, _speed(100.0f)
, _path(NULL)
, _curPathUnitIndex(0)
{
    
}

PathMoveComponent::~PathMoveComponent()
{
    CC_SAFE_RELEASE_NULL(_path);
}

bool PathMoveComponent::init(GameScene* game, cocos2d::CCNode *target)
{
    if(CCNode::init())
    {
        _game = game;
        _target = target;
        
        _moveComponent = MoveComponent::create(_target);
        _moveComponent->setDelegate(this);
        this->addChild(_moveComponent);
        
        return true;
    }
    return false;
}

void PathMoveComponent::startMove(cocos2d::CCArray *path)
{
    path->retain();
    CC_SAFE_RELEASE_NULL(_path);
    _path = path;
    _curPathUnitIndex = 0;
    this->movePathUnit();
}

void PathMoveComponent::stopMove()
{
    _moveComponent->stopMove();
}

void PathMoveComponent::pauseMove()
{
    _moveComponent->pauseMove();
}

void PathMoveComponent::resumeMove()
{
    _moveComponent->resumeMove();
}

void PathMoveComponent::movePathUnit()
{
    if(_curPathUnitIndex < _path->count())
    {
        PathUnit* unit = (PathUnit*)_path->objectAtIndex(_curPathUnitIndex);
        CCPoint position = _game->convertToWorldPositionInTileCenter(unit->getLocation()) + ccp(0, -_game->getTileSize().height * 0.3/*0.5*/); // 接近底部
        _moveComponent->startMove(position);
        
        if(_delegate)
        {
            if(position.x > _target->getPosition().x)
                _delegate->onChangeDirection(PathDirection_Right);
            else if(position.x < _target->getPosition().x)
                _delegate->onChangeDirection(PathDirection_Left);
            else if(position.y > _target->getPosition().y)
                _delegate->onChangeDirection(PathDirection_Up);
            else if(position.y < _target->getPosition().y)
                _delegate->onChangeDirection(PathDirection_Down);
        }
    }
    else
    {
        if(_delegate)
        {
            _delegate->onArrived();
        }
    }
}

void PathMoveComponent::setDelegate(PathMoveDelegate *delegate)
{
    _delegate = delegate;
}

void PathMoveComponent::removeDelegate()
{
    _delegate = NULL;
}

void PathMoveComponent::onArrived()
{
    ++ _curPathUnitIndex;
    this->movePathUnit();
}

int PathMoveComponent::getLastDistance()
{
    return _path->count() - _curPathUnitIndex;
}

void PathMoveComponent::setSpeed(float speed)
{
    _speed = speed;
    _moveComponent->setSpeed(_speed);
}