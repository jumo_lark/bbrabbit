//
//  Button.cpp
//  rabbit
//
//  Created by 李崧榕 on 15/2/5.
//
//

#include "Button.h"
#include "NotifyManager.h"
#include "AudioController.h"

const float KScale_ButtonTouch = 0.9f;

Button* Button::create(ButtonId id, cocos2d::CCSprite *spriteNormal, cocos2d::CCSprite *spriteClick, cocos2d::CCNode *nodeInfo)
{
    Button* button = new Button();
    if(button && button->init(id, spriteNormal, spriteClick, nodeInfo))
    {
        button->autorelease();
        return button;
    }
    CC_SAFE_DELETE(button);
    return NULL;
}

Button* Button::create(ButtonId id, cocos2d::CCSprite *spriteNormal)
{
    return create(id, spriteNormal, NULL, NULL);
}

Button* Button::create(ButtonId id, cocos2d::CCSprite *spriteNormal, cocos2d::CCSprite *spriteClick)
{
    return create(id, spriteNormal, spriteClick, NULL);
}

Button::Button()
: _id(ButtonId_None)
, _sprite_normal(NULL)
, _sprite_click(NULL)
, _node_info(NULL)
, _scale_sprite_normal(ccp(1.0f, 1.0f))
, _scale_node_info(ccp(1.0f, 1.0f))
, _extraData(NULL)
, _touchAble(true)
, _clickAble(true)
{
    
}

Button::~Button()
{
    CC_SAFE_RELEASE_NULL(_extraData);
}

bool Button::init(ButtonId id, cocos2d::CCSprite *spriteNormal, cocos2d::CCSprite *spriteClick, cocos2d::CCNode *nodeInfo)
{
    if(CCNode::init())
    {
        _id = id;
        _sprite_normal = spriteNormal;
        _sprite_click = spriteClick;
        _node_info = nodeInfo;
        
        if(_sprite_normal)
        {
            this->addChild(_sprite_normal);
            _scale_sprite_normal = ccp(_sprite_normal->getScaleX(), _sprite_normal->getScaleY());
        }
        
        if(_sprite_click)
        {
            this->addChild(_sprite_click);
            _sprite_click->setVisible(false);
        }
        
        if(_node_info)
        {
            this->addChild(_node_info);
            _scale_node_info = ccp(_node_info->getScaleX(), _node_info->getScaleY());
        }
        
        return true;
    }
    return false;
}

void Button::touch()
{
    if(_touchAble)
    {
        Audio->playSfx("audio_button_touch");
        
        if(_sprite_click)
        {
            _sprite_normal->setVisible(false);
            _sprite_click->setVisible(true);
        }
        else
        {
            _sprite_normal->setScaleX(_scale_sprite_normal.x * KScale_ButtonTouch);
            _sprite_normal->setScaleY(_scale_sprite_normal.y * KScale_ButtonTouch);
            
            if(_node_info)
            {
                _node_info->setScaleX(_scale_node_info.x * KScale_ButtonTouch);
                _node_info->setScaleY(_scale_node_info.y * KScale_ButtonTouch);
            }
        }
        
        Notify->postNotification(NotifyMsg_button_touch, this);
    }
}

void Button::resume()
{
    if(_touchAble)
    {
        if(_sprite_click)
        {
            _sprite_normal->setVisible(true);
            _sprite_click->setVisible(false);
        }
        else
        {
            _sprite_normal->setScaleX(_scale_sprite_normal.x);
            _sprite_normal->setScaleY(_scale_sprite_normal.y);
            
            if(_node_info)
            {
                _node_info->setScaleX(_scale_node_info.x);
                _node_info->setScaleY(_scale_node_info.y);
            }
        }
    }
}

void Button::click()
{
    if(_clickAble)
    {
        Notify->postNotification(NotifyMsg_button_click, this);
    }
}

void Button::setExtraData(cocos2d::CCObject *extraData)
{
    CC_SAFE_RETAIN(extraData);
    CC_SAFE_RELEASE_NULL(_extraData);
    _extraData = extraData;
}

CCRect Button::getRect()
{
    CCRect rect = _sprite_normal->boundingBox();
    CCPoint worldZero = _sprite_normal->convertToWorldSpaceAR(CCPointZero);
    return CCRectMake(rect.origin.x + worldZero.x, rect.origin.y + worldZero.y, rect.size.width, rect.size.height);
}

/*
 * class ButtonUtil
 */

int ButtonUtil::getHitButtonIndex(cocos2d::CCArray *buttons, cocos2d::CCPoint pos)
{
    int index = KButtonIndex_None;
    
    Button* button = NULL;
    for(int i = 0; i < buttons->count(); ++i)
    {
        button = dynamic_cast<Button*>(buttons->objectAtIndex(i));
        
        if(button &&
           button->getId() != ButtonId_None &&
           button->getClickAble() &&
           button->getRect().containsPoint(pos))
        {
            index = i;
            break;
        }
    }
    
    return index;
}

Button* ButtonUtil::getHitButton(cocos2d::CCArray *buttons, cocos2d::CCPoint pos)
{
    Button* button = NULL;
    
    int index = getHitButtonIndex(buttons, pos);
    if(index != KButtonIndex_None)
        button = (Button*)buttons->objectAtIndex(index);
    
    return button;
}

int ButtonUtil::getHitButtonId(cocos2d::CCArray *buttons, cocos2d::CCPoint pos)
{
    int buttonId = ButtonId_None;
    
    Button* button = getHitButton(buttons, pos);
    if(button)
        buttonId = button->getId();
    
    return buttonId;
}

/*
 * class ButtonManager
 */

ButtonManager* ButtonManager::create()
{
    ButtonManager* manager = new ButtonManager();
    if(manager && manager->init())
    {
        manager->autorelease();
        return manager;
    }
    CC_SAFE_DELETE(manager);
    return NULL;
}

ButtonManager::ButtonManager()
: _buttons(NULL)
, _pressedButton(NULL)
{
    
}

ButtonManager::~ButtonManager()
{
    CC_SAFE_RELEASE_NULL(_buttons);
}

bool ButtonManager::init()
{
    if(CCNode::init())
    {
        _buttons = CCArray::create();
        _buttons->retain();
        
        return true;
    }
    return false;
}

void ButtonManager::addButton(Button *button)
{
    _buttons->addObject(button);
}

void ButtonManager::removeButton(Button *button)
{
    if(button == _pressedButton)
        _pressedButton = NULL;
    
    _buttons->removeObject(button);
}

void ButtonManager::removeButton(int buttonId)
{
    Button* button = NULL;
    for(int i = _buttons->count() - 1; i >= 0; --i)
    {
        button = (Button*)_buttons->objectAtIndex(i);
        if(button->getId() == buttonId)
            _buttons->removeObject(button);
    }
}

bool ButtonManager::addEventTouchBegan(cocos2d::CCTouch *pTouch)
{
    bool isTrigger = false;
    
    CCPoint pos = pTouch->getLocation();
    _pressedButton = ButtonUtil::getHitButton(_buttons, pos);
    if(_pressedButton)
    {
        _pressedButton->touch();
        isTrigger = true;
    }
    
    return isTrigger;
}

bool ButtonManager::addEventTouchEnd(cocos2d::CCTouch *pTouch)
{
    bool isTrigger = false;
    
    if(_pressedButton)
    {
        _pressedButton->resume();
        
        CCPoint pos = pTouch->getLocation();
        if(ButtonUtil::getHitButton(_buttons, pos) == _pressedButton)
        {
            _pressedButton->click();
            isTrigger = true;
        }
    }
    _pressedButton = NULL;
    
    return isTrigger;
}