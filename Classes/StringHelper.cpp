//
//  StringHelper.cpp
//  rabbit
//
//  Created by 李崧榕 on 15/2/5.
//
//

#include "StringHelper.h"

float StringHelper::randomFloatInRange(float minInclusive, float maxInclusive)
{
    return minInclusive + (CCRANDOM_0_1() * (maxInclusive - minInclusive));
}

int StringHelper::randomIntInRange(int minInclusive, int maxInclusive)
{
    float randomSeed = CCRANDOM_0_1();
    return minInclusive + ((int) floorf(randomSeed * (maxInclusive - minInclusive)));
}

string StringHelper::intToString(int num)
{
    stringstream ss;
    ss << num;
    string s = ss.str();
    return s;
}

string StringHelper::floatToString(float num)
{
    stringstream ss;
    ss << num;
    string s = ss.str();
    return s;
}

int StringHelper::stringToInt(const string& str)
{
    int num = 0;
    stringstream ss(str);
    ss >> num;
    return num;
}

float StringHelper::stringToFloat(const string &str)
{
    float num = 0;
    stringstream ss(str);
    ss >> num;
    return num;
}

bool StringHelper::hasPrefix(const string &source, const string &prefix)
{
    return source.find(prefix) != string::npos;
}

vector<string> StringHelper::componentsSeparatedByString(const string &str, const string &delim)
{
    vector<string> tokens;
    string::size_type lastPos = 0;
    string::size_type pos = str.find(delim, lastPos);
    while(string::npos != pos)
    {
        tokens.push_back(str.substr(lastPos, pos - lastPos));
        lastPos = pos + delim.size();
        pos = str.find(delim, lastPos);
    }
    
    tokens.push_back(str.substr(lastPos, str.size() - lastPos));
    return tokens;
}
