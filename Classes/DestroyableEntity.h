//
//  DestroyableEntity.h
//  rabbit
//
//  Created by 李崧榕 on 15/2/5.
//
//

#ifndef __rabbit__DestroyableEntity__
#define __rabbit__DestroyableEntity__

#include "cocos2d.h"
USING_NS_CC;

class DestroyableEntity : public CCNode
{
public:
    
    virtual void recover(int hp);
    virtual void damage(int hp);
    virtual void killed();
    
    virtual CCRect getRect() = 0;
    
protected:
    
    DestroyableEntity();
    virtual ~DestroyableEntity();
    virtual bool init();
    
private:
    
    CC_SYNTHESIZE_READONLY(int, _curHp, CurHp);
    CC_SYNTHESIZE_READONLY(int, _maxHp, MaxHp);
    CC_SYNTHESIZE_READONLY(bool, _isKilled, IsKilld);
};

#endif /* defined(__rabbit__DestroyableEntity__) */
