//
//  GameScene.cpp
//  rabbit
//
//  Created by 李崧榕 on 15/2/5.
//
//

#include "GameScene.h"
#include "AStarPathHelper.h"
#include "TileMapStateHelper.h"
#include "EntityManager.h"
#include "StringHelper.h"
#include "EnemyData.h"
#include "Enemy.h"
#include "GameUI.h"
#include "NotifyManager.h"
#include "TowerData.h"
#include "Tower.h"
#include "StaticDataManager.h"
#include "PhoenixEgg.h"
#include "ResourceHelper.h"
#include "Button.h"
#include "Item.h"
#include "PopupWin.h"
#include "PopupFailed.h"
#include "AudioController.h"

enum GameSceneZorder
{
    GameSceneZorder_Background,
    GameSceneZorder_TileMap,
    GameSceneZorder_Entity,
    
    GameSceneZorder_UI,
};

CCScene* GameScene::scene()
{
    CCScene* scene = CCScene::create();
    GameScene* game = GameScene::create(scene);
    scene->addChild(game);
    return scene;
}

GameScene* GameScene::create(CCScene* scene)
{
    GameScene* game = new GameScene();
    if(game && game->init(scene))
    {
        game->autorelease();
        return game;
    }
    CC_SAFE_DELETE(game);
    return NULL;
}

GameScene::GameScene()
: _bg(NULL)
, _tileMap(NULL)
, _pathHelper(NULL)
, _stateHelper(NULL)
, _entityManager(NULL)
, _ui(NULL)
, _state(GameState_Play)
, _preState(GameState_None)
, _speed(GameSpeed_Normal)
, _path(NULL)
, _eggHpPoint(CCPointZero)
, _gold(500)
, _inPopup(false)
{
    
}

GameScene::~GameScene()
{
    CC_SAFE_RELEASE_NULL(_path);
    
//    ResourceHelper::removeGameSceneResource();
}

bool GameScene::init(CCScene* scene)
{
    if(CCLayer::init())
    {
        _scene = scene;
        _screenSize = CCDirector::sharedDirector()->getWinSize();
        
        this->createSceneContent();
        this->initSceneProperty();
        this->createUI();
        
//        CCArmatureDataManager::sharedArmatureDataManager()->addArmatureFileInfo("Enemy/enemy_1.ExportJson");
//        CCArmatureDataManager::sharedArmatureDataManager()->addArmatureFileInfo("PhoenixEgg/king_3.ExportJson");
//        CCArmatureDataManager::sharedArmatureDataManager()->addArmatureFileInfo("Tower/captain_primary.ExportJson");
//        CCArmatureDataManager::sharedArmatureDataManager()->addArmatureFileInfo("Tower/captain_senior.csb");
//        CCArmatureDataManager::sharedArmatureDataManager()->addArmatureFileInfo("", "", "Tower/captain_senior.csb");
//        CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("Tower/captain_senior0.plist");
        
        // add egg
        _entityManager->addEgg(_exit, _eggHpPoint);
        
        Audio->playBgMusic("bgmusic_gamescene");
        
        return true;
    }
    return false;
}

// create
void GameScene::createSceneContent()
{
    this->createBackground();
    this->createTileMap();
    this->createAStartPathHelper();
    this->createTileMapStateHelper();
    this->createEntityManager();
}

void GameScene::createBackground()
{
    _bg = CCSprite::createWithSpriteFrameName("map_bg_1");
    _bg->setScale(1.25);
    _bg->setPosition(_screenSize * 0.5);
    this->addChild(_bg, GameSceneZorder_Background);
}

void GameScene::createTileMap()
{
    _tileMap = CCTMXTiledMap::create("Maps/map_path_1-1.tmx");
    
    _mapSize = _tileMap->getMapSize();
    _tileSize = _tileMap->getTileSize();
    _mapRealSize = ccp(_mapSize.width * _tileSize.width, _mapSize.height * _tileSize.height);
    _mapDiff = (_screenSize - _mapRealSize) * 0.5;
    
    _tileMap->setPosition(_mapDiff);
    this->addChild(_tileMap, GameSceneZorder_TileMap);
}

void GameScene::createAStartPathHelper()
{
    _pathHelper = PathHelper::create(_tileMap);
    this->addChild(_pathHelper);
}

void GameScene::createTileMapStateHelper()
{
    _stateHelper = TileMapStateHelper::create(_tileMap);
    this->addChild(_stateHelper);
}

void GameScene::createEntityManager()
{
    _entityManager = EntityManager::create(this);
    this->addChild(_entityManager, GameSceneZorder_Entity);
}

void GameScene::createUI()
{
    _ui = GameUI::create(_scene, this);
    this->addChild(_ui, GameSceneZorder_UI);
}

void GameScene::initSceneProperty()
{
    CCTMXObjectGroup* mapObjectGroup = _tileMap->objectGroupNamed("Objects");
    CCArray* objects = mapObjectGroup->getObjects();
    
    for(int i = 0; i < objects->count(); ++i)
    {
        CCDictionary* objectDic = (CCDictionary*)objects->objectAtIndex(i);
        CCString* cstr_name = (CCString*)objectDic->objectForKey("name");
        
        float x = ((CCString*)objectDic->objectForKey("x"))->floatValue() / _tileSize.width;
        float y = (_mapRealSize.height - ((CCString*)objectDic->objectForKey("y"))->floatValue()) / _tileSize.height - 1;
        
        if(StringHelper::hasPrefix(string(cstr_name->getCString()), "Waypoint"))
        {
            if(strcmp(cstr_name->getCString(), "Waypoint_exit") == 0)
            {
                _exit = ccp(x, y);
            }
            else // entry
            {
                _entry = ccp(x, y);
            }
        }
        else if(strcmp(cstr_name->getCString(), "HpBar") == 0)
        {
            _eggHpPoint = ccp(x, y);
        }
    }
    
    _path = _pathHelper->getPath(_entry, _exit);
    _path->retain();
    
    CCSprite* img_entry = CCSprite::createWithSpriteFrameName("gameui_img_entry");
    img_entry->setPosition(this->convertToWorldPositionInTileCenter(_entry) + ccp(0, -_tileSize.height * 0.225));
    this->addChild(img_entry, GameSceneZorder_TileMap);
}

void GameScene::addObserver()
{
    Notify->addObserver(this, callfuncO_selector(GameScene::onButtonClick), NotifyMsg_button_click, NULL);
    Notify->addObserver(this, callfuncO_selector(GameScene::onPopupOpen), NotifyMsg_popup_open, NULL);
    Notify->addObserver(this, callfuncO_selector(GameScene::onPopupClose), NotifyMsg_popup_close, NULL);
}

void GameScene::onButtonClick(cocos2d::CCObject *obj)
{
    Button* button = (Button*)obj;
    ButtonId bid = button->getId();
    
    if(bid == ButtonId_Game_Replay)
    {
        //
        CCDirector::sharedDirector()->replaceScene(GameScene::scene());
    }
    else if(bid == ButtonId_Game_NextLevel)
    {
        //
    }
    else if(bid == ButtonId_Game_ReturnMap)
    {
        //
    }
}

void GameScene::onPopupOpen(cocos2d::CCObject *obj)
{
    _preState = _state;
    this->updateGameState(GameState_Pause);
    _inPopup = true;
}

void GameScene::onPopupClose(cocos2d::CCObject *obj)
{
    this->updateGameState(_preState);
    _preState = GameState_None;
    _inPopup = false;
}

void GameScene::onPopupWin()
{
    int eggHp = _entityManager->getEgg()->getCurHp();
    int starCount = eggHp < 3 ? 1 : eggHp < 10 ? 2 : 3;
    _scene->addChild(PopupWin::create(_scene, starCount));
}

void GameScene::onPopupFailed()
{
    _scene->addChild(PopupFailed::create(_scene));
}

// util interface
CCPoint GameScene::convertToTilePoint(cocos2d::CCPoint worldPosition)
{
    CCPoint tilePosition = worldPosition - _mapDiff;
    
    CCPoint tilePoint = ccp((int)(tilePosition.x / _tileSize.width), (int)((_mapRealSize.height - tilePosition.y) / _tileSize.height));
    
    if(tilePosition.x < 0)
        tilePoint.x = -1;
    
    if(tilePosition.y < 0)
        tilePoint.y = -1;
    
    return tilePoint;
}

CCPoint GameScene::convertToWorldPosition(cocos2d::CCPoint tilePoint)
{
    CCPoint tilePosition = ccp(tilePoint.x * _tileSize.width, _mapRealSize.height - tilePoint.y * _tileSize.height);
    
    return tilePosition + _mapDiff; // 返回的坐标为Tile的左上角
}

CCPoint GameScene::convertToWorldPositionInTileCenter(cocos2d::CCPoint tilePoint)
{
    return convertToWorldPosition(tilePoint) + ccp(_tileSize.width * 0.5, -_tileSize.height * 0.5);
}

void GameScene::addTower(TowerData *data, cocos2d::CCPoint tilePoint)
{
    _entityManager->addTower(data, tilePoint);
}

void GameScene::removeTower(Tower *tower)
{
    _entityManager->removeTower(tower);
}

void GameScene::addGold(int gold, cocos2d::CCPoint pos)
{
    _gold += gold;
    Notify->postNotification(NotifyMsg_update_gold, CCInteger::create(_gold));
    _entityManager->addGoldEffect(gold, pos);
}

void GameScene::costGold(int gold)
{
    CCAssert(_gold >= gold, "game gold should >= cost gold!");
    _gold -= gold;
    Notify->postNotification(NotifyMsg_update_gold, CCInteger::create(_gold));
}

void GameScene::updateGameState(GameState state)
{
    _state = state;
    
    if(_state == GameState_Pause)
    {
        Notify->postNotification(NotifyMsg_game_pause);
    }
    else if(_state == GameState_Play)
    {
        Notify->postNotification(NotifyMsg_game_resume);
    }
    else if(_state == GameState_Win)
    {
        this->updateGameSpeed(GameSpeed_Normal);
        Notify->postNotification(NotifyMsg_game_pause);
        Notify->postNotification(NotifyMsg_game_win);
        this->scheduleOnce(schedule_selector(GameScene::onPopupWin), 1);
    }
    else if(_state == GameState_Over)
    {
        this->updateGameSpeed(GameSpeed_Normal);
        Notify->postNotification(NotifyMsg_game_pause);
        Notify->postNotification(NotifyMsg_game_over);
        this->scheduleOnce(schedule_selector(GameScene::onPopupFailed), 1);
    }
}

void GameScene::updateGameSpeed(GameSpeed speed)
{
    _speed = speed;
    
    if(_speed == GameSpeed_Normal)
    {
        CCDirector::sharedDirector()->getScheduler()->setTimeScale(1);
    }
    else if(_speed == GameSpeed_Acc)
    {
        CCDirector::sharedDirector()->getScheduler()->setTimeScale(2);
    }
}

DestroyableEntity* GameScene::getSelectedTarget()
{
    return _ui->getAttackSignTarget();
}

// override
void GameScene::onEnter()
{
    CCLayer::onEnter();
    CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this, -GameSceneTouchPriority_GameScene, true);
    
    this->addObserver();
    
    _entityManager->scheduleOnce(schedule_selector(EntityManager::readyAddWave), 1.5);
}

void GameScene::onExit()
{
    CCLayer::onExit();
    CCDirector::sharedDirector()->getTouchDispatcher()->removeDelegate(this);
    
    Notify->removeAllObservers(this);
}

bool GameScene::ccTouchBegan(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent)
{
    if(_state == GameState_Over || _state == GameState_Win)
        return true;
    
    CCPoint pos = pTouch->getLocation();
    CCPoint tilePoint = this->convertToTilePoint(pos);
    
    bool isTrigger = false;
    
    if(!isTrigger)
    {
        Enemy* enemy = _entityManager->getEnemy(pos);
        if(enemy)
        {
            _ui->setAttackSignTarget(enemy);
            isTrigger = true;
        }
    }
    
    if(!isTrigger)
    {
        TileMapState state = _stateHelper->getTileMapState(tilePoint);
        if(state != TileMapState_NotInMap)
        {
            if(state == TileMapState_Empty)
            {
                _ui->showPanelConstructor(tilePoint);
                isTrigger = true;
            }
            else if(state == TileMapState_Item)
            {
                Item* item = _entityManager->getItem(tilePoint);
                if(item)
                {
                    _ui->setAttackSignTarget(item);
                    isTrigger = true;
                }
            }
            else if(state == TileMapState_Tower)
            {
                Tower* tower = _entityManager->getTower(tilePoint);
                if(tower)
                {
                    tower->click();
                    _ui->showPanelLevelup(tower);
                    isTrigger = true;
                }
            }
        }
    }
    
    return true;
}

void GameScene::ccTouchMoved(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent)
{
    
}

void GameScene::ccTouchEnded(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent)
{
    
}