//
//  BulletRadish.cpp
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/13.
//
//

#include "BulletRadish.h"
#include "StringHelper.h"
#include "Util.h"

BulletRadish* BulletRadish::create(EntityManager *entityManager, DestroyableEntity *target, float attackPower, int level)
{
    BulletRadish* bullet = new BulletRadish();
    if(bullet && bullet->init(entityManager, target, attackPower, level))
    {
        bullet->autorelease();
        return bullet;
    }
    CC_SAFE_DELETE(bullet);
    return NULL;
}

BulletRadish::BulletRadish()
{
    
}

BulletRadish::~BulletRadish()
{
    
}

bool BulletRadish::init(EntityManager *entityManager, DestroyableEntity *target, float attackPower, int level)
{
    if(Bullet::init(entityManager, target, attackPower))
    {
        _level = level;
        
        string str_name = "rabbit_bullet_" + StringHelper::intToString(_level);
        _anim = CCArmature::create(str_name.c_str());
        this->addChild(_anim);
        _anim->getAnimation()->playByIndex(0);
        
        return true;
    }
    return false;
}

void BulletRadish::update(float dt)
{
    Bullet::update(dt);
    
    if(!_inPause)
    {
        //set rotation
        this->setRotation(Util::getTargetRotation(_targetPosition, this->getPosition()));
    }
}