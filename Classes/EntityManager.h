//
//  EntityManager.h
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/6.
//
//

#ifndef __bbrabbit__EntityManager__
#define __bbrabbit__EntityManager__

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;
USING_NS_CC_EXT;

#include "TileMapStateHelper.h"
#include "TowerData.h"

class GameScene;
class Enemy;
class EnemyData;
class Tower;
class Wave;
class WaveData;
class PhoenixEgg;
class Bullet;
class Item;
class ItemData;

const float KTime_WaveReady = 3.0f;

class EntityManager : public CCNode
{
public:
    
    static EntityManager* create(GameScene* game);
    
    virtual void onEnter();
    virtual void onExit();
    
    Enemy* addEnemy(EnemyData* data);
    void removeEnemy(Enemy* enemy, bool beKilled);
    Enemy* getEnemy(CCPoint worldPosition);
    
    Item* addItem(ItemData* data, CCPoint tilePoint, CCSize tileSize);
    void removeItem(Item* item, bool beKilled);
    Item* getItem(CCPoint tilePoint);
    
    Tower* addTower(TowerData* data, CCPoint tilePoint);
    void removeTower(Tower* tower);
    Tower* getTower(CCPoint tilePoint);
    
    Bullet* addBullet(Tower* tower);
    void removeBullet(Bullet* bullet);
    
    void addEgg(CCPoint tilePoint, CCPoint hpPoint);
    void removeEgg();
    
    void readyAddWave();
    void addWave();
    void removeWave(Wave* wave);
    int getMaxWave();
    
    void addGoldEffect(int gold, const cocos2d::CCPoint &pos);
    void addDeathEffect(const CCPoint &pos);
    void addTowerStartEffect(const CCPoint &pos);
    void addTowerLevelupEffect(const CCPoint &pos, int level);
    void addTowerHitEffect(TowerId towerId, const CCPoint &pos);
    
protected:
    
    EntityManager();
    virtual ~EntityManager();
    virtual bool init(GameScene* game);
    
    void initItems();
    void initWaves();
    
    void addObserver();
    void onPause();
    void onResume();
    
    void updateGameMapState(Item* item, TileMapState state);
    void endWave();
    
    void addAutoRemoveAnimation(CCArmature* anim, CCPoint pos, int zorder);
    void onRemoveAnimation(CCArmature *, MovementEventType, const char *);
    
    string getTowerHitEffectName(TowerId towerId);
    
private:
    
    CC_SYNTHESIZE_READONLY(GameScene*, _game, Game);
    
    CC_SYNTHESIZE_READONLY(CCArray*, _enemyArr, EnemyArr);
    CC_SYNTHESIZE_READONLY(CCArray*, _itemArr, ItemArr);
    CC_SYNTHESIZE_READONLY(CCArray*, _towerArr, TowerArr);
    CC_SYNTHESIZE_READONLY(PhoenixEgg*, _egg, Egg);
    
    CCArray* _waveDataArr;
    int _curWaveDataIndex;
    Wave* _curWave;
};

#endif /* defined(__bbrabbit__EntityManager__) */
