//
//  LabelImgNum.h
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/9.
//
//

#ifndef __bbrabbit__LabelImgNum__
#define __bbrabbit__LabelImgNum__

#include "cocos2d.h"
USING_NS_CC;

enum LabelImgNumType
{
    LabelImgNumType_GameGold,
};

class LabelImgNum : public CCNode
{
public:
    
    static LabelImgNum* create(LabelImgNumType type, CCSprite* img, int num);
    
    void setNum(int num);
    
    virtual void onEnter();
    virtual void onExit();
    
protected:
    
    LabelImgNum();
    virtual ~LabelImgNum();
    virtual bool init(LabelImgNumType type, CCSprite* img, int num);
    
    void createImg();
    void createLabelNum();
    
    void addObserver();
    void onGoldUpdate(CCObject* obj);
    
private:
    
    CC_SYNTHESIZE_READONLY(LabelImgNumType, _type, Type);
    CC_SYNTHESIZE_READONLY(int, _num, Num);
    
    CCSprite* _sprite_img;
    CCLabelBMFont* _label_num;
};

#endif /* defined(__bbrabbit__LabelImgNum__) */
