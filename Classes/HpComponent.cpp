//
//  HpComponent.cpp
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/12.
//
//

#include "HpComponent.h"

HpComponent* HpComponent::create(float maxHp)
{
    HpComponent* hp = new HpComponent();
    
    if(hp != NULL && hp->init(maxHp))
    {
        hp->autorelease();
        return hp;
    }
    
    CC_SAFE_DELETE(hp);
    return NULL;
}

HpComponent::HpComponent()
: _max(0)
, _curHp(0)
{
    
}

HpComponent::~HpComponent()
{
    
}

bool HpComponent::init(float max)
{
    if(CCNode::init())
    {
        _max = max;
        _curHp = max;
        this->createHp();
        return true;
    }
    
    return false;
}

void HpComponent::createHp()
{
    CCSprite* hpOuter = CCSprite::createWithSpriteFrameName("gameui_img_hp_outer");
    this->addChild(hpOuter);
    
    CCSprite* hpInner = CCSprite::createWithSpriteFrameName("gameui_img_hp_inner");
    _hp = CCProgressTimer::create(hpInner);
    _hp->setType(kCCProgressTimerTypeBar);
    _hp->setMidpoint(ccp(0, 0.5));
    _hp->setBarChangeRate(ccp(1, 0.5));
    _hp->setPercentage(100);
    
    this->addChild(_hp);
}

void HpComponent::refresh(float curHp)
{
    _curHp = curHp;
    if(_curHp < 0)
    {
        _curHp = 0;
    }
    float percentage = ((float)_curHp * 100) / (float)_max;
    _hp->setPercentage(percentage);
}

void HpComponent::getProcessed(float process)
{
    float percentage = ((float)process * 100) / (float)_max;
    _hp->setPercentage(percentage);
}

void HpComponent::resetMaxHp(float max)
{
    _max = max;
    this->refresh(_curHp);
}

void HpComponent::resetCurHp(float cur)
{
    _curHp = cur;
    this->refresh(_curHp);
}
