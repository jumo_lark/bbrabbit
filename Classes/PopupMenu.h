//
//  PopupMenu.h
//  bbrabbit
//
//  Created by 李崧榕 on 15/2/11.
//
//

#ifndef __bbrabbit__PopupMenu__
#define __bbrabbit__PopupMenu__

#include "PopupLayer.h"

class PopupMenu : public PopupLayer
{
public:
    
    static PopupMenu* create(CCScene* scene);
    
protected:
    
    PopupMenu();
    virtual ~PopupMenu();
    virtual bool init(CCScene* scene);
    
    virtual void createBackground();
    void createTitleBg();
    void createButtonContinue();
    void createButtonReplay();
    void createButtonReturnMap();
    
private:
    
    // observer
    virtual void onButtonClick(CCObject* obj);
};

#endif /* defined(__bbrabbit__PopupMenu__) */
