<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>3</int>
        <key>texturePackerVersion</key>
        <string>3.2.1</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string>-hd</string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>0.5</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>premultiplyAlpha</key>
        <true/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>cocos2d</string>
        <key>textureFileName</key>
        <filename>../../Resources/Common/map_common_elements{v}.pvr.ccz</filename>
        <key>flipPVR</key>
        <false/>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">FloydSteinbergAlpha</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>2</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">pvr2ccz</enum>
        <key>borderPadding</key>
        <uint>2</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>1024</int>
            <key>height</key>
            <int>1024</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>reduceBorderArtifacts</key>
        <false/>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">NPOT</enum>
            <key>forceSquared</key>
            <false/>
            <key>forceWordAligned</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../Resources/Common/map_common_elements{v}.plist</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA4444</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string>3d68368354dee26055ee4cf1d2411205</string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <true/>
        <key>cleanTransparentPixels</key>
        <false/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>innerPadding</key>
            <uint>0</uint>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>heuristicMask</key>
            <false/>
        </struct>
        <key>fileList</key>
        <array>
            <filename>batman_icon.png</filename>
            <filename>button_close_bg.png</filename>
            <filename>button_close.png</filename>
            <filename>captain_icon.png</filename>
            <filename>catwoman_icon.png</filename>
            <filename>challenge_icon.png</filename>
            <filename>common_achieve_finish_icon.png</filename>
            <filename>common_achieve_img_light.png</filename>
            <filename>common_popup_item_clear_bg.png</filename>
            <filename>hulk_icon.png</filename>
            <filename>ironman_icon.png</filename>
            <filename>main_popup_text2_bg.png</filename>
            <filename>messagePopup.png</filename>
            <filename>no.png</filename>
            <filename>spiderman_icon.png</filename>
            <filename>superman_icon.png</filename>
            <filename>thor_icon.png</filename>
            <filename>wolverine_icon.png</filename>
            <filename>yes.png</filename>
            <filename>common_popup_achieve_finished_bg.png</filename>
            <filename>button_add.png</filename>
            <filename>button_service.png</filename>
            <filename>statue.png</filename>
            <filename>house1.png</filename>
            <filename>wood.png</filename>
            <filename>long_button_bg.png</filename>
            <filename>long_button.png</filename>
            <filename>button_charge_spree.png</filename>
            <filename>img_charge_card.png</filename>
            <filename>button_guanwang.png</filename>
            <filename>button_weekendspree.png</filename>
            <filename>img_weekend_frenzy.png</filename>
            <filename>button_get.png</filename>
            <filename>button_unget.png</filename>
            <filename>button_notice.png</filename>
            <filename>img_loading.png</filename>
            <filename>no_gray.png</filename>
            <filename>img_christmas_sign.png</filename>
            <filename>superrobot_icon.png</filename>
            <filename>long_button_gray.png</filename>
            <filename>img_sign_gamepower_empty.png</filename>
            <filename>img_sign_gamepower.png</filename>
            <filename>img_dropstar.png</filename>
            <filename>img_energy.png</filename>
            <filename>main_popup_text1_bg.png</filename>
            <filename>long_button_red.png</filename>
            <filename>guidance_img_doctor.png</filename>

        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
    </struct>
</data>
